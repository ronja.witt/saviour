import paho.mqtt.client as mqtt
from datetime import datetime
import json
import random
import numpy as np
import time
import uuid

TOPIC = "Sensoren"
MQTT_BROKER = "127.0.0.1"
MQTT_PORT = 1883
QOS = 1


""" Constants altering the course of the exponential decay time series. Feel free to change them :) """
T0 = 75 # Inital tem perature in degrees
tau = 180 # decay factor in s 

def exponential_decay(t, A0, tau, noise_factor=0.5):
    """
    Virtual sensor simulating a measurement of an expoentially decaying quantity, such as the temperature in a coffee mug :)
    t := time step, int
    A0 := Initial amplitude at t=0
    tau := decay constant 
    noise_factor := noise amplitude control (0 means no noise!) 

    """
    value = (A0-21) * np.exp(-t / tau) + 21 
    value += np.random.normal(0, noise_factor * np.sqrt(value))
    return value

def create_message():
    """
    value := sensor value, list
    unit  := unit of the value, str
    nonce := arbitrary dictionary of metadata, dict 
    """

    response = {'Time': datetime.now().strftime('%H:%M:%S'), 'Date': datetime.now().strftime('%Y-%m-%d'), 'Acc_X_Ext': random.random() * 100,'Acc_Y_Ext': random.random() * 100,'Acc_Z_Ext': random.random() * 100,'Acc_X_Bed': random.random() * 100,'Acc_Y_Bed': random.random() * 100,'Acc_Z_Bed':random.random() * 100,'FilFeed': random.random() * 100, 'FilPositio': random.random() * 100, 'tempExt1': random.random() * 100, 'pressure':random.random() * 100,  'humidity': random.random() * 100, 'FilDiaA': random.random() * 100, 'FilDiaB':random.random() * 100, 'CrossSec': random.random() * 100, 'DoorState_closed':random.random() * 100}
    # response = {'Time': datetime.now().strftime('%H:%M:%S'), 'Date': datetime.now().strftime('%d.%m.%Y'), 'Acc_X_Ext': random.random() * 100,'Acc_Y_Ext': random.random() * 100,'Acc_Z_Ext': random.random() * 100,'Acc_X_Bed': random.random() * 100,'Acc_Y_Bed': random.random() * 100,'Acc_Z_Bed':random.random() * 100,'FilFeed': random.random() * 100, 'FilPositio': random.random() * 100, 'tempExt1': random.random() * 100, 'pressure':random.random() * 100,  'humidity': random.random() * 100, 'FilDiaA': random.random() * 100, 'FilDiaB':random.random() * 100, 'CrossSec': random.random() * 100, 'DoorState_closed':random.random() * 100}
    return json.dumps(response)

if __name__ == "__main__":
    client = mqtt.Client()
    client.connect(MQTT_BROKER, MQTT_PORT)
    print("Connected to MQTT Broker: " + MQTT_BROKER)

    while True:
        try:
            client.publish(TOPIC, create_message(), 0)       
            time.sleep(0.1)

        except KeyboardInterrupt:
            break
        
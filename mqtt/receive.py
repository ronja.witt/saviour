import os, sys
sys.path.append(os.getcwd()+"")
import paho.mqtt.client as mqtt
import json
import mysql.connector as connector




TOPIC = "Sensoren" # Sensoren
BROKER_ADDRESS = "127.0.0.1"
PORT = 1883

def on_message(client, userdata, message):
    msg = str(message.payload.decode("utf-8"))
    print("message received: ", msg)
    print("message topic: ", message.topic)

    print("_________________________________this worked__________________________________")
    data = dict(
        topic=message.topic,
        payload=message.payload.decode()
    )

    # emit a mqtt_message event to the socket containing the message data
    response = json.loads(data["payload"])

    # insert mqtt-data into the database
    convertedDict = json.loads(data["payload"])

    print(convertedDict)

    # search_query = """
    # SELECT id, ended_time FROM process_step ORDER BY id DESC LIMIT 1
    # """

    # # insert_query = """
    # # insert into sensor_readings_current_process (value,timestamp, sensor_id, process_step_id)
    # # values(%s,%s,(SELECT id FROM sensor WHERE name = %s),%s)
    # # """

    # insert_query = """
    # insert into sensor_readings_current_process (value,timestamp, sensor_id, process_step_id)
    # values(%s,%s,%s,%s)
    # """

    # mydb = connector.connect(
    #     host=db_credentials.host,
    #     user=db_credentials.user,
    #     password=db_credentials.password,
    #     database=db_credentials.database_name
    # )
    # cursor = mydb.cursor()
    # cursor.execute(search_query)
    # search = cursor.fetchall()
    # # continue inserting data to database, if the last process step has not stopped running yet
    # if search[0][1] == None:
    #     for key in convertedDict.keys():
    #         if key != "time":
    #             print("value:")
    #             print(convertedDict[key])
    #             print("timestamp:")
    #             print(convertedDict["time"])
    #             print("sensor_id:")
    #             print(key)
    #             print("process_step_id:")
    #             print(search[0][0])
    #             cursor.execute(
    #                 insert_query, (convertedDict[key], convertedDict["time"], key, search[0][0]))
    #             mydb.commit()
    # cursor.close()

def on_connect(client, userdata, flags, rc):
    print("Connected to MQTT Broker: " + BROKER_ADDRESS)
    client.subscribe(TOPIC)

if __name__ == "__main__":
    client = mqtt.Client()
    client.on_connect = on_connect
    client.on_message = on_message

    client.connect(BROKER_ADDRESS, PORT)
    client.loop_forever()







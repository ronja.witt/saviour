from sktime.transformations.panel.tsfresh import TSFreshFeatureExtractor
import pandas as pd
import numpy as np
import pickle

# -----------------------------------------------------------------------------------------------------
# prepare multivariant-panel-data for TSFreshFeatureExtractor (Dataframe of Dataframes)
# -----------------------------------------------------------------------------------------------------

def prepare_multivariant_panel_data(list_of_sensordataframes):
    elements = []
    for df_sensordata in list_of_sensordataframes:
        for name_columns in df_sensordata.columns:
            if name_columns != "time" and name_columns != "Time: ":
                elements.append(df_sensordata[[name_columns]])
    return elements


# -----------------------------------------------------------------------------------------------------
# funktions for feature-extraction
# -----------------------------------------------------------------------------------------------------


def convert_data_to_nested_data(elements):
    """
    converts a list of dataframes into a dataframe of dataframes

    input: 
            list of experiment-dataframes
    output: 
            dataframe of dataframes (for the feature-extraction)
    """

    d = {}
    for element in elements:
        d[element.iloc[0]["id"]] = element.loc[:, element.columns!='id'] # take id of experiment as key of the dict
        # print(d)

    X_elements = pd.concat(d) # turns dict of dataframes into dataframe of dataframes
    # print(X_elements)
    return X_elements


    
def extract_features(elements):
    """
    extracts features with sktime and saves them into a dataframe

    For the dataframes belonging to acceleration data, a previously selected set of features is extracted, 
    while for the other dataframes the default features of sktime are extracted. 
    This speeds up the extraction process considerably due to the large amount of data in acceleration dataframes. 

    input: 
            list of experiment-dataframes
    output: 
            extracted feature-dataframe
    """

    kind_to_fc_parameters = {
        "mean": None, 
        "maximum": None, 
        "minimum": None, 
        "standard_deviation": None, 
        "abs_energy": None, 
        "variance": None, 
        "spkt_welch_density": [{"coeff": 2}, {"coeff": 5}, {"coeff": 8}], 
        "fft_coefficient": [{"coeff": 10, "attr": "real"}], 
        "fft_aggregated": [{"aggtype": "centroid"}, {"aggtype": "variance"}, {"aggtype": "skew"}], 
        "fourier_entropy": [{"bins": 6}, {"bins": 9}]}

    list_of_feature_df_single_sensor = []

    for element in elements:
        element.reset_index()
        element.iloc[:,0] = pd.to_numeric(element.iloc[:,0], errors = 'coerce')
        element.dropna(inplace = True)
        element.iloc[:,0] =  element.iloc[:,0].astype('float64')
        for name_columns in element.loc[:, element.columns!='id'].columns:
            elements_single = []
            element["id"] = 0
            elements_single.append(element[[name_columns, "id"]])

            # put df of same sensor in one dataframe
            if name_columns in ["Acc_Z_Bed", "Acc_Y_Bed", "Acc_X_Bed", "Acc_Z_Ext", "Acc_Y_Ext", "Acc_X_Ext",'  Acc_X_Ext [mm/s] ', '  Acc_Y_Ext [mm/s] ', '  Acc_Z_Ext [mm/s] ', '  Acc_X_Bed [mm/s] ', '  Acc_Y_Bed [mm/s] ', '  Acc_Z_Bed [mm/s] ']:
                t = TSFreshFeatureExtractor(default_fc_parameters=kind_to_fc_parameters, show_warnings=False)
                feature_df_single_sensor = t.fit_transform(convert_data_to_nested_data(elements_single))
                list_of_feature_df_single_sensor.append(feature_df_single_sensor)
            else:
                t = TSFreshFeatureExtractor(default_fc_parameters="minimal", show_warnings=False)
                feature_df_single_sensor = t.fit_transform(convert_data_to_nested_data(elements_single))
                list_of_feature_df_single_sensor.append(feature_df_single_sensor)
    list_of_all_features = combine_features(list_of_feature_df_single_sensor)
    print(list_of_all_features)

    return(list_of_all_features)



def combine_features(list_of_feature_df_single_sensor):
    """
    combine features from different sensors to one features_df
    TODO: Combine all features to one dataframe

    input: 
            List of feature_df_single_sensor
    output: 
            feature_df with all features from all sensors
    """

    feature_df = pd.DataFrame()
    for feature_df_single_sensor in list_of_feature_df_single_sensor:
        feature_df = pd.concat([feature_df, feature_df_single_sensor], axis=1)
    return feature_df


def step_4_feature_extraction(list_of_sensordataframes):
    elements = prepare_multivariant_panel_data(list_of_sensordataframes)
    list_of_all_features = extract_features(elements)
    return list_of_all_features

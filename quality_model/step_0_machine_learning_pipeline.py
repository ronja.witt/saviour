import os, sys
sys.path.append(os.getcwd()+"")
import pickle

import pandas as pd

import quality_model.step_3_data_preprocessing
import quality_model.step_4_feature_extraction
import quality_model.step_5_feature_selection

def get_df_of_selected_features(n_percentage, n_features = 100):
    # create a file
    picklefile = open('quality_model/features_dfs/' + str(n_percentage) + 'percent', 'rb')

    # pickle the dataframe
    df_of_features = pickle.load(picklefile)

    # close file
    picklefile.close()

    label_y_data = pd.DataFrame({"result" : [0, 1, 1, 0, 1, 0]})

    df_of_features = df_of_features.dropna(axis=1)

    df_of_selected_features = quality_model.step_5_feature_selection.select_features(df_of_features, label_y_data, n_features=n_features)
    return df_of_selected_features


def get_df_of_features(filenames, n_percentage):
    
    df_of_features = pd.DataFrame()
    for filename in filenames:
        if df_of_features.empty:
            pass
        else:
            if (filename in df_of_features['id'].values) != True:
                pass
            else:
                continue

        list_of_dataframes = quality_model.step_3_data_preprocessing.step_3_data_preprocessing(filename)
        print(list_of_dataframes)

        list_of_dataframes_percentage = list()
        for df in list_of_dataframes:
            # print(df)
            df = df.head(int(len(df)*(n_percentage/100)))
            # print(df)
            list_of_dataframes_percentage.append(df)
        
        print(list_of_dataframes_percentage)

        list_of_all_features_percentage = quality_model.step_4_feature_extraction.step_4_feature_extraction(list_of_dataframes_percentage)
        list_of_all_features_percentage["id"]=filename
        print(list_of_all_features_percentage)
        print(type(list_of_all_features_percentage))
        df_of_features = pd.concat([df_of_features, list_of_all_features_percentage], ignore_index = True)

    print(df_of_features)

    # create a file
    picklefile = open('quality_model/features_dfs/' + str(n_percentage) + 'percent', 'wb')

    # pickle the dataframe
    pickle.dump(df_of_features, picklefile)

    # close file
    picklefile.close()

def get_df_of_features_all_percentage(filenames):
    
    for filename in filenames:
        list_of_dataframes = quality_model.step_3_data_preprocessing.step_3_data_preprocessing(filename)
        print(list_of_dataframes)

        for percentage in [10, 20, 30, 40, 50, 60, 70, 80, 90, 100]:
            try:
                df_of_features = pickle.load(open('quality_model/features_dfs/' + str(percentage) + 'percent', "rb"))
            except:
                df_of_features = pd.DataFrame()
                pickle.dump(df_of_features, open('quality_model/features_dfs/' + str(percentage) + 'percent', "wb"))
                df_of_features = pickle.load(open('quality_model/features_dfs/' + str(percentage) + 'percent', "rb"))

            if df_of_features.empty:
                pass
            else:
                if (filename in df_of_features['id'].values) != True:
                    pass
                else:
                    continue


            list_of_dataframes_percentage = list()
            for df in list_of_dataframes:
                # print(df)
                df = df.head(int(len(df)*(percentage/100)))
                # print(df)
                list_of_dataframes_percentage.append(df)
            
            print(list_of_dataframes_percentage)

            list_of_all_features_percentage = quality_model.step_4_feature_extraction.step_4_feature_extraction(list_of_dataframes_percentage)
            list_of_all_features_percentage["id"]=filename
            print(list_of_all_features_percentage)
            print(type(list_of_all_features_percentage))
            df_of_features = pd.concat([df_of_features, list_of_all_features_percentage], ignore_index = True)

            print(df_of_features)

            # create a file
            picklefile = open('quality_model/features_dfs/' + str(percentage) + 'percent', 'wb')

            # pickle the dataframe
            pickle.dump(df_of_features, picklefile)

            # close file
            picklefile.close()

def get_df_of_features_all_percentage_DB(filename, list_of_dataframes):
    """
    this methods creates a dataframe of all percentiles of a sensordataset
    Input: list of dataframes
    """

    df_of_all_features_all_percentage = pd.DataFrame()
    for percentage in [10, 20, 30, 40, 50, 60, 70, 80, 90, 100]:

        list_of_dataframes_percentage = list()
        for df in list_of_dataframes:
            df = df.head(int(len(df)*(percentage/100)))
            list_of_dataframes_percentage.append(df)
        
        print(list_of_dataframes_percentage)

        list_of_all_features_percentage = quality_model.step_4_feature_extraction.step_4_feature_extraction(list_of_dataframes_percentage)
        list_of_all_features_percentage["id"]=filename
        list_of_all_features_percentage["percent"]=percentage
        print(list_of_all_features_percentage)

        df_of_all_features_all_percentage = pd.concat([df_of_all_features_all_percentage, list_of_all_features_percentage], ignore_index = True)
        print(df_of_all_features_all_percentage)
    return df_of_all_features_all_percentage




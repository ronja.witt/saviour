# this Python-script is used for data preprocessing of the extracted data (see "data_extraction.py")
# 1. open csv-file
# 2. Pre-processing Steps
# - handling datetime
# - handling Null values
# - Standardization
# - Handling Categorical Variables (One-Hot Encoding)
# - Multicollinearity

import pandas as pd
from datetime import date
import numpy as np
import pickle



def load_data_into_dataframes():
    # open csv-file and text-file
    df = pd.read_csv("U:/1_Projekte/1_Saviour/Code/SAViour/data/Versuchsdaten_2/Versuchsdaten_2022_04_21.csv", sep = ";")
    df_2 = pd.read_csv("U:/1_Projekte/1_Saviour/Code/SAViour/data/Versuchsdaten_2/Versuzchsdaten_2022_04_21_sensor_angepasst.txt", sep = ";")

    # change df_2 to a readable dataframe
    new_df_2 = df_2[['time', 'Acc_X_Ext', 'Acc_Y_Ext', 'Acc_Z_Ext', 'Acc_X_Bed', 'Acc_Y_Bed', 'Acc_Z_Bed', 'temperature_cham', 'humidity', \
        'pressure', 'FilPosition', 'FilFeed', 'FilDiaA', 'FilDiaB', 'CrossSec', 'DoorState']]
    # print(df_2)
    # print(new_df_2)
    
    # Add current date to time in order to convert it into datetime
    today = date.today()

    # convert time_value of df into datetime:
    df['time']= [str(today)+str(" ")+ str(x) for x in df['time']]
    df['time'] = pd.to_datetime(df['time'], format='%Y-%m-%d %H:%M:%S')

    # convert time_value of new_df_2 into datetime:
    new_df_2['time']= [str(today)+str(" ")+ str(x) for x in new_df_2['time']]
    new_df_2['time'] = pd.to_datetime(new_df_2.loc[:, 'time'], format='%Y-%m-%d %H:%M:%S:%f')
    return df, new_df_2


def one_hot_encoding_for_objecttype_variables(filtered_df_2):
    """
    makes a new dataframe for the object-type variables
            - if the datatype of a variable is "object" the variable is treated as a categorical one
            - in that case one-hot-encoding is applied
            - else if the variable is not categorical, df_categorical_return is empty

    input: 
            dataframe of a single sensor without NaN-values
    output: 
            new dataframe for categorical data
    """

    df_categorical_return = pd.DataFrame()
    df_categorical = filtered_df_2.select_dtypes(include=['object']).copy()
    if df_categorical.empty == False:
        # frequency distribution of categories within the feature
        # print(df_categorical.iloc[:, 0].value_counts())
        # how many categories does the feature have
        # print(df_categorical.iloc[:, 0].value_counts().count())
        # implement one_hot_encoding for every categorical dataframe
        df_categorical_onehot =  pd.get_dummies(filtered_df_2, columns=[df_categorical.columns[0]], prefix = [df_categorical.columns[0]])
        # append the list of categorical dataframes, which will be returned
        df_categorical_return = df_categorical_onehot
    return df_categorical_return





# -----------------------------------------------------------------------------------------------
# 2.3 handling Null values
# Problem: we can not know, how many values of a sensor there should be (maybe how much data per second)
# Therefore deleting time-series with to less data values will be difficult
# -----------------------------------------------------------------------------------------------


def handling_NaN_values_and_categorical_values(dataframe_input):
    """ 
    seperating the dataframe into several dataframes and deleting rows with NaN-values:
            - Each of the dataframes displays one sensor and the time
            - dropping the rows with NaN-values
            - returns a list of dataframes

    input: 
            dataframe_input is a dataframe containing the time and the data of all sensors
    output: 
            list of dataframes, where each dataframe consists of the time and the data of one sensor
    """

    list_of_dataframes = list()
    for i in dataframe_input.columns:
        if i != "time":
            filtered_df_2=dataframe_input[["time", i]].dropna(axis=0)

            # if categorical data: returns categorical dataframe with one-hot-encoding
            # if not: returns an empty dataframe
            df_categorical_return = one_hot_encoding_for_objecttype_variables(filtered_df_2)

            # for categorical data: append categorical dataframe
            if df_categorical_return.empty == False:
                list_of_dataframes.append(df_categorical_return)

            # else: append the filtered dataframe
            else:
                list_of_dataframes.append(filtered_df_2)

    # The list contains all sensordata in individual dataframes with one-hot-encoding
    return list_of_dataframes


def step_3_data_preprocessing(filename):
    # df, new_df_2 = load_data_into_dataframes()

    # read the pickle file
    picklefile = open('quality_model/dataframes/' + str(filename), 'rb')
    # unpickle the dataframe
    list_of_dataframes = pickle.load(picklefile)
    # close file
    picklefile.close()
    list_of_dataframes

    list_of_sensordataframes = list_of_dataframes #list_of_dataframes
    return list_of_sensordataframes


# -----------------------------------------------------------------------------------------------------
# save the list of sensor-dataframes in a picklefile for feature-extraction
# -----------------------------------------------------------------------------------------------------

# # take a dataframe of all sensor data and return a list of dataframes 
# # without NaN-values and one-hot-encoding for categorical data
# list_1 = handling_NaN_values_and_categorical_values(df)
# list_2 = handling_NaN_values_and_categorical_values(new_df_2)
# list_of_sensordataframes = list_1 + list_2

# list_of_sensordataframes = step_3_data_preprocessing()

# print(list_of_sensordataframes)

# # create a file
# picklefile = open('list_of_sensordataframes', 'wb')
# # pickle the dataframe
# pickle.dump(list_of_sensordataframes, picklefile)
# # close file
# picklefile.close()


# -----------------------------------------------------------------------------------------------
# 2.4 Standardization
# -----------------------------------------------------------------------------------------------

# only if necessary for the ML-Algorithms

# -----------------------------------------------------------------------------------------------
# 2.5 Multicollinearity
# -----------------------------------------------------------------------------------------------
from darts.models import RNNModel, NBEATSModel
from darts.datasets import AirPassengersDataset, MonthlyMilkDataset, ElectricityDataset
import matplotlib.pyplot as plt
from darts.dataprocessing.transformers import Scaler
from darts import TimeSeries
from darts.metrics import mape, smape, mae

import os, sys
sys.path.append(os.getcwd()+"")

import quality_model.step_3_data_preprocessing
list_of_dataframes = quality_model.step_3_data_preprocessing.step_3_data_preprocessing()
print(list_of_dataframes[14].reset_index(drop=True))

own_series = TimeSeries.from_dataframe(list_of_dataframes[14].reset_index(drop=True), time_col=None, value_cols='humidity', freq='3S')
print(own_series)

series_air = AirPassengersDataset().load()
series_milk = MonthlyMilkDataset().load()

own_series.plot(label="humidity")
# series_air.plot(label="Number of air passengers")
# series_milk.plot(label="Pounds of milk produced per cow")
plt.legend()
plt.show()


scaler_own, scaler_air, scaler_milk = Scaler(), Scaler(), Scaler()
series_own_scaled = scaler_own.fit_transform(own_series)
series_air_scaled = scaler_air.fit_transform(series_air)
series_milk_scaled = scaler_milk.fit_transform(series_milk)

series_own_scaled.plot(label="own")
# series_air_scaled.plot(label="air")
# series_milk_scaled.plot(label="milk")
plt.legend()
plt.show()

train_own, val_own = series_own_scaled[:-200], series_own_scaled[-200:]
train_air, val_air = series_air_scaled[:-36], series_air_scaled[-36:]
train_milk, val_milk = series_milk_scaled[:-36], series_milk_scaled[-36:]

# print(train_air, val_air)
print(type(series_air))

model_own = NBEATSModel(
    input_chunk_length=300, output_chunk_length=100, n_epochs=100, random_state=0
)

model_own.fit([train_own, train_own, train_own, train_own, train_own, train_own, train_own, train_own], verbose=True)

pred = model_own.predict(n=400, series=series_own_scaled[:-400])

series_own_scaled.plot(label="actual")
pred.plot(label="forecast")
plt.legend()
plt.show()
print("MAPE = {:.2f}%".format(mape(series_own_scaled, pred)))





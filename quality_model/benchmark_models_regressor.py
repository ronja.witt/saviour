import os, sys
sys.path.append(os.getcwd()+"")

import pandas as pd
import numpy as np
from sklearn.datasets import make_classification
from sklearn.model_selection import train_test_split
from sklearn.model_selection import GridSearchCV
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import precision_recall_curve, average_precision_score, roc_auc_score, auc, roc_curve
from sklearn.linear_model import LogisticRegression
from sklearn.svm import SVC, SVR
from sklearn.pipeline import Pipeline
import matplotlib.pyplot as plt
# from helper import plot_classifier
from sklearn.pipeline import Pipeline
from sklearn.ensemble import RandomForestClassifier, RandomForestRegressor
from sklearn.neighbors import KNeighborsClassifier, KNeighborsRegressor
from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.svm import SVC
from sklearn.naive_bayes import GaussianNB
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.gaussian_process import GaussianProcessClassifier
from sklearn.gaussian_process.kernels import RBF, DotProduct
import pickle
from joblib import dump
from sklearn.feature_selection import SelectKBest, chi2, f_classif, SelectFromModel, mutual_info_classif
from sklearn.svm import LinearSVC
from xgboost import XGBRegressor
from sklearn.feature_selection import SelectKBest, chi2, f_classif, SelectFromModel, mutual_info_classif, f_regression
from sklearn.feature_selection import VarianceThreshold


# The scorers can be either one of the predefined metric strings or a scorer
# callable, like the one returned by make_scorer


# Setting refit='AUC', refits an estimator on the whole dataset with the
# parameter setting that has the best cross-validated AUC score.
# That estimator is made available at ``clf.best_estimator_`` along with
# parameters like ``clf.best_score_``, ``clf.best_params_`` and
# ``clf.best_index_``

# using 10-fold CrossValidation and GridSearch the different classifiers are compared based on the metric
# output: the estimator and the hyperparameters that proofed most effective on the given data-set
def benchmark_models_reg(X_train, y_train, n_selected_features):
    # status for this function range is between 0 and 1
    global status_benchmark_reg
    status_benchmark_reg = 0

    # pipelines for each Classifier:
    scoring = {"R2": "r2", "MAX_ERROR": "max_error"}

    pipeline1 = Pipeline([
        ("variance", VarianceThreshold()),
        ("scaler", StandardScaler()),
        ("reg", SVR())
    ])

    pipeline2 = Pipeline([
        ("variance", VarianceThreshold()),
        ("scaler", StandardScaler()),
        ('reg', KNeighborsRegressor())
    ])

    pipeline3 = Pipeline([
        ("variance", VarianceThreshold()),
        ("scaler", StandardScaler()),
        ('reg', RandomForestRegressor()),
    ])

    pipeline4 = Pipeline((
        ("variance", VarianceThreshold()),
        ("scaler", StandardScaler()),
        ('reg', XGBRegressor()),
    ))

    pipeline5 = Pipeline((
        ("variance", VarianceThreshold()),
        ("scaler", StandardScaler()),
        ('reg', GaussianProcessRegressor()),
    ))

    # Parameters for the GridSearch for each Pipeline:
    parameters1 = {
        'reg__C': [0.01, 0.1, 1],
        'reg__kernel': ['rbf', 'poly'],
        'reg__gamma': [0.01, 0.1, 1.0], #"scale"
    }

    parameters2 = {
        'reg__n_neighbors': [3, 7, 10, 12, 14],

    }

    parameters3 = {
        'reg__n_estimators': [10, 20, 30],
        'reg__criterion': ['squared_error', 'absolute_error'],
        'reg__min_samples_leaf': [4, 5, 6],
    }

    parameters4 = {
    }

    parameters5 = {
        'reg__kernel': [RBF(l) for l in np.logspace(-1, 1, 2)]
    }, {
        'reg__kernel': [DotProduct(sigma_0) for sigma_0 in np.logspace(-1, 1, 2)]
    }
    
    pipelines = [pipeline1, pipeline2, pipeline3, pipeline4, pipeline5]
    parameters = [parameters1, parameters2, parameters3, parameters4, parameters5]
    estimators = {}
    
    # for each pipeline with the given Parameter-grids the GridSearchCV is run through
    # for each pipeline the "best_estimator_" is chosen and put in a dictionary together with its "best_score_"
    for i in range(len(pipelines)):
        gs = GridSearchCV(
            pipelines[i],
            parameters[i],
            scoring=scoring,
            refit="R2",
            cv=10
        )
        status_benchmark_reg = status_benchmark_reg + round((0.1 * 1/len(pipelines)), 2)
        print(f"Hi there, this is status: {status_benchmark_reg}")
        gs.fit(X_train, y_train)
        status_benchmark_reg = status_benchmark_reg + round((0.8 * 1/len(pipelines)), 2)
        print(f"Hi there, this is status: {status_benchmark_reg}")
        print("this is model number: " + str(i))
        estimators[gs.best_estimator_] = gs.best_score_
        status_benchmark_reg = status_benchmark_reg + round((0.1 * 1/len(pipelines)), 2)
        print(f"Hi there, this is status: {status_benchmark_reg}")
        print(gs.best_score_)
    
    # the estimator with the highest best_score_ is determined and returned as the best_estimator for the classification problem at hand
    best_estimator = (max(estimators, key=estimators.get))
    status_benchmark_reg = 1
    return best_estimator

def get_benchmark_models_reg(X_train, X_test, y_train, y_test, quality_parameter, percentile = 90, n_selected_features=8):

    print(X_test)

    best_estimator = benchmark_models_reg(X_train, y_train, n_selected_features)

    try:
        # create a file
        picklefile = open('quality_model/models/' + str(quality_parameter) + str(percentile), 'wb')
        # pickle the dataframe
        pickle.dump(best_estimator, picklefile)
        # close file
        picklefile.close()
    except:
        print("The Model could not be saved in the Pickle-file")
    return best_estimator, best_estimator.score(X_test, y_test)


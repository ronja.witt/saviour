from typing import Dict

import plotly.graph_objects as go

from .utils import get_features


def loco_bar_plot(result: Dict, title: str = 'LOCO bar plot', score_axis_title: str = 'feature importance', horizontal: bool = True) -> go.Figure:
    """Creates a bar plot for LOCO scores from given results, including confidence intervals.

    Args:
        result (Dict): A LOCO importance tool's result dictionary.
        title (str, optional): Title for the plot. Defaults to 'LOCO bar plot'.
        score_axis_title (str, optional): Title for the score axis (usually x-axis). Defaults to 'feature importance'.
        horizontal (bool, optional): Whether the bars extend horizontally rather than vertically. Defaults to True.

    Returns:
        go.Figure: The generated LOCO bar plot.
    """

    features = get_features(result)

    # get corresponding scores
    scores = [result[f] for f in features]

    # prepare confidence intervals
    cis = [result['misc']['ci'][f] for f in features]
    cis_minus = [scores[i] - ci[0] for i, ci in enumerate(cis)]
    cis_plus = [ci[1] - scores[i] for i, ci in enumerate(cis)]

    # TODO error bars in legend
    # create figure
    if horizontal:
        bars = go.Bar(x=scores, y=features, error_x_array=cis_plus, error_x_arrayminus=cis_minus, orientation='h')
    else:
        bars = go.Bar(x=features, y=scores, error_y_array=cis_plus, error_y_arrayminus=cis_minus, orientation='v')
    fig = go.Figure(bars)

    # adjust layout
    fig.update_layout(title=title)
    if horizontal:
        fig.update_layout(xaxis_title=score_axis_title)
    else:
        fig.update_layout(yaxis_title=score_axis_title)

    return fig

from typing import Dict, List, Tuple

import networkx as nx
import numpy as np
import plotly.graph_objects as go
from networkx.drawing.layout import spring_layout

from .utils import colors, get_features, minmax


def interaction_graph_plot(result: Dict, importance: Dict = None, title: str = 'Interaction graph plot', threshold: float = 0.0, layout: callable = spring_layout, plot_size: int = 750) -> go.Figure:
    """Creates a graph plot of the given interaction results, indicating stronger interactions by wider edges. Optionally, node sizes can be adjusted based on given importance results.

    Args:
        result (Dict): An interaction tool's result dictionary with positive real values for each feature pair.
        importance (Dict, optional): An importance tool's result dictionary with positive real values for each feature. Defaults to None.
        title (str, optional): Title for the plot. Defaults to 'Interaction graph plot'.
        threshold (float, optional): Threshold for interaction scores below which edges are not shown. Defaults to 0.0.
        layout (callable, optional): Method for specifying the graph layout, as provided by [networkx](https://networkx.org/documentation/stable/reference/drawing.html#module-networkx.drawing.layout). Defaults to spring_layout.
        plot_size (int, optional): Edge length of the square plot. Defaults to 750.

    Returns:
        go.Figure: The generated interaction graph plot.
    """

    feature_pairs = get_features(result)
    fig = _init_graph_figure(title, plot_size)

    # only keep edges of pairs with a score above threshold
    edges = {pair: result[pair]
             for pair in feature_pairs
             if result[pair] > threshold}
    nodes = _position_nodes(edges, layout=layout, weighted=True)

    # plot graph components
    _plot_edges(fig, nodes, edges)
    _plot_nodes(fig, nodes, importance)

    return fig


def causality_graph_plot(result: Dict, title: str = 'Causality graph plot', layout: callable = spring_layout, plot_size: int = 750) -> go.Figure:
    """Creates a graph plot of the given causality results, indicating causality relations with arrows.

    Args:
        result (Dict): A causality tool's result dictionary with values for each feature pair.
        title (str, optional): Title for the plot. Defaults to 'Causality graph plot'.
        layout (callable, optional): Method for specifying the graph layout, as provided by [networkx](https://networkx.org/documentation/stable/reference/drawing.html#module-networkx.drawing.layout). Defaults to spring_layout.
        plot_size (int, optional): Edge length of the square plot. Defaults to 750.

    Returns:
        go.Figure: The generated causality graph plot.
    """

    fig = _init_graph_figure(title, plot_size)

    nodes = result['nodes']
    edges = result['edges']
    pos_nodes = _position_nodes(nodes, edges, layout=layout)

    # plot graph components
    _plot_arrows(fig, pos_nodes, edges)
    _plot_nodes(fig, pos_nodes)

    return fig


def _init_graph_figure(title: str = None, plot_size: int = 750) -> go.Figure:
    """Creates a plain square figure for plotting graphs.

    Args:
        title (str, optional): Title for the plot. Defaults to None.
        plot_size (int, optional): Edge length of the square plot. Defaults to 750.

    Returns:
        go.Figure: The initialized graph figure.
    """

    fig = go.Figure()
    fig.update_xaxes(showgrid=False, zeroline=False, showticklabels=False)
    fig.update_yaxes(showgrid=False, zeroline=False, showticklabels=False, scaleanchor='x')
    fig.update_layout(title=title, plot_bgcolor='white', height=plot_size, width=plot_size)

    return fig


def _position_nodes(nodes: List[str], edges: Dict[Tuple[str, str], float], layout: callable = spring_layout) -> Dict[str, Tuple[float, float]]:
    """Determines the positions of graph nodes based on the given edges and desired layout. Optionally takes into account weighted edges.

    Args:
        nodes (List[str]): List of all nodes.
        edges (List[Tuple[str, str]]): List of edges as node pairs.
        layout (callable, optional): Method for specifying the graph layout, as provided by [networkx](https://networkx.org/documentation/stable/reference/drawing.html#module-networkx.drawing.layout). Defaults to spring_layout.

    Returns:
        Dict[str, Tuple[float, float]]: Dictionary of the positioned nodes with their (x, y) coordinates.
    """

    G = nx.Graph()
    G.add_nodes_from(nodes)
    G.add_edges_from(edges)
    pos_nodes = layout(G)

    return pos_nodes


def _plot_nodes(fig: go.Figure, nodes: Dict, importance: Dict = None, legend_text: str = 'importance values'):
    """Plots the given graph nodes. Optionally takes into account importance results for node sizes.

    Args:
        fig (go.Figure): The figure to plot in.
        nodes (Dict): Dictionary of nodes with their (x, y) coordinates.
        importance (Dict, optional): An importance tool's result dictionary with positive real values for each feature. Defaults to None.
        legend_text (str, optional): Text for node sizes in the legend. Defaults to 'importance values'.
    """

    features = list(nodes.keys())
    x = [pos[0] for pos in nodes.values()]
    y = [pos[1] for pos in nodes.values()]

    if importance is None:
        # plot nodes
        node_trace = go.Scatter(x=x, y=y, text=features, mode='markers+text', showlegend=False, hoverinfo='text',
                                textposition='top center', marker_color=colors(0), marker_size=10, name='features')
        fig.add_trace(node_trace)

    else:
        # plot node markers (low opacity)
        node_trace = go.Scatter(x=x, y=y, mode='markers', showlegend=False, hoverinfo='none',
                                marker_color=colors(0, 0.5), marker_size=10)
        fig.add_trace(node_trace)

        val_min, val_max = minmax([importance[f] for f in features])  # min and max importance values

        for i, node in enumerate(features):
            val = importance[node]
            w = np.sqrt(val / val_max) * 25
            hovertext = f'{val} <br> {node}'

            # plot individual importance indicators
            importance_trace = go.Scatter(x=[x[i]], y=[y[i]], text=node, mode='markers+text', hovertext=hovertext,
                                          textposition='top center', marker_color=colors(0), marker_size=w,
                                          hoverinfo='text', showlegend=(val in [val_min, val_max]), name=f'{val:.3e}',
                                          legendgroup='nodes', legendgrouptitle_text=legend_text)
            fig.add_trace(importance_trace)


def _plot_edges(fig: go.Figure, nodes: Dict[str, Tuple[float, float]], edges: Dict[Tuple[str, str], float], legend_text: str = 'interaction values'):
    """Plots the given graph edges, including invisible middle points for tooltips on hover.

    Args:
        fig (go.Figure): The figure to plot in.
        nodes (Dict[str, Tuple[float, float]]): Dictionary of nodes with their (x, y) coordinates.
        edges (Dict[Tuple[str, str], float]): Dictionary of edges with their nodes as keys and weights as values.
        legend_text (str, optional): Text for edge widths in the legend. Defaults to 'interaction values'.
    """

    val_min, val_max = minmax(edges.values())  # min and max interaction values

    for (n0, n1), val in edges.items():
        x0, y0 = nodes[n0]
        x1, y1 = nodes[n1]
        w = val / val_max * 10

        # plot individual edges
        edge_trace = go.Scatter(x=[x0, x1], y=[y0, y1], mode='lines', showlegend=(val in [val_min, val_max]),
                                hoverinfo='none', line_width=w, line_color=colors(2, 0.5), name=f'{val:.3e}',
                                legendgroup='edges', legendgrouptitle_text=legend_text)
        fig.add_trace(edge_trace)

    # add edge middle points for hovertext
    mid_x = [(nodes[n0][0] + nodes[n1][0]) / 2 for (n0, n1) in edges.keys()]
    mid_y = [(nodes[n0][1] + nodes[n1][1]) / 2 for (n0, n1) in edges.keys()]
    hovertext = [f'{val} <br> {pair}' for pair, val in edges.items()]

    middle_points = go.Scatter(x=mid_x, y=mid_y, mode='markers', hoverinfo='text', hovertext=hovertext,
                               showlegend=False, line_color=colors(2), opacity=0)
    fig.add_trace(middle_points)


def _plot_arrows(fig: go.Figure, nodes: Dict[str, Tuple[float, float]], edges: List[Tuple[str, str]], space: float = 0.025):
    """Plots arrows on the given graph edges.

    Args:
        fig (go.Figure): The figure to plot in.
        nodes (Dict[str, Tuple[float, float]]): Dictionary of nodes with their (x, y) coordinates.
        edges (List[Tuple[str, str]]): Dictionary of edges with their nodes as keys.
        space (float, optional): Space to keep clear between nodes and arrow heads and tails. Defaults to 0.025.
    """

    for (n0, n1) in edges:
        x0, y0 = nodes[n0]
        x1, y1 = nodes[n1]
        length = np.linalg.norm((x1-x0, y1-y0))
        start_x = x0 + space * (x1-x0) / length
        start_y = y0 + space * (y1-y0) / length
        end_x = x1 - space * (x1-x0) / length
        end_y = y1 - space * (y1-y0) / length

        fig.add_annotation(
            ax=start_x, ay=start_y, x=end_x, y=end_y,
            axref='x', ayref='y', xref='x', yref='y',
            arrowhead=4, arrowsize=1, arrowwidth=2,
            showarrow=True, opacity=0.5
        )

import warnings
from typing import Dict, Tuple

import numpy as np
import plotly.graph_objects as go
from plotly.subplots import make_subplots

from . import importance_bar_plot
from .utils import get_features, minmax


def shap_bar_plot(result: Dict, title: str = 'SHAP bar plot') -> go.Figure:
    """Creates a bar plot for mean SHAP values from given results.

    Args:
        result ([type]): A SHAP importance tool's result dictionary.
        title (str, optional): Title for the plot. Defaults to 'SHAP bar plot'.

    Returns:
        go.Figure: The generated SHAP bar plot.
    """

    fig = importance_bar_plot(result, title=title, score_axis_title='mean absolute SHAP value')

    return fig


def shap_beeswarm_plot(result: Dict, title: str = 'SHAP beeswarm plot', max_num_samples: int = 250) -> go.Figure:
    """Creates a beeswarm plot for individual SHAP values from given results, including corresponding feature values.
    Plot is adapted from [here](https://github.com/slundberg/shap).

    Args:
        result (Dict): A SHAP importance tool's result dictionary.
        title (str, optional): Title for the plot. Defaults to 'SHAP beeswarm plot'.
        max_num_samples (int, optional): Maximum number of samples to show. Defaults to 250.

    Returns:
        go.Figure: The generated beeswarm plot.
    """

    fig = go.Figure()

    # get features and sample count
    feature_values = result['misc']['values']
    features = list(feature_values.keys())
    n_samples = len(list(feature_values.values())[0])

    if n_samples > max_num_samples:  # only plot a random subset of samples
        i_samples = np.random.choice(n_samples, max_num_samples, replace=False)
        warnings.warn('Not showing all samples, only {max_num_samples} of them.'
                      .format(max_num_samples=max_num_samples), RuntimeWarning)
    else:
        i_samples = np.arange(n_samples)

    for i, (feature, shap_values) in enumerate(result['misc']['shap_values'].items()):
        # jitter along y-axis
        jitter = 0.5 * (np.random.random_sample(len(i_samples)) - 0.5)

        # get feature values and their extrema for the colorbar
        values = feature_values[feature]
        cmin, cmax = minmax(values)

        # point color styling and colorbar
        markers = dict(
            cmin=cmin, cmax=cmax,
            color=values[i_samples],
            opacity=0.5,
            colorscale='Bluered',
            colorbar=dict(title='feature value', titleside='right', tickvals=[cmin, cmax], ticktext=['min', 'max']),
            showscale=(not i)
        )

        # plot the points for one feature
        points = go.Scatter(x=shap_values[i_samples], y=jitter+i, mode='markers',
                            marker=markers, showlegend=False, name=feature)

        fig.add_trace(points)

    # add feature names as ticklabels
    fig.update_yaxes(
        ticktext=features,
        tickvals=list(range(len(features))),
        showgrid=False,
        zeroline=False
    )

    fig.update_layout(
        title=title,
        xaxis_title='SHAP value'
    )

    return fig


def shap_summary_plot(result: Dict, title: str = 'SHAP summary plot') -> go.Figure:
    """Creates a summary plot for SHAP values from given results, including both a bar plot and a beeswarm plot.

    Args:
        result (Dict): A SHAP importance tool's result dictionary.
        title (str, optional): Title for the plot. Defaults to 'SHAP summary plot'.

    Returns:
        go.Figure: The generated SHAP summary plot.
    """

    features = get_features(result)
    n_features = len(features)

    # create plots
    importance_plot = importance_bar_plot(result)
    beeswarm_plot = shap_beeswarm_plot(result)

    fig = make_subplots(rows=2)
    beeswarm_plot.data[0].marker.colorbar.update({
        'y': fig.layout.yaxis2.domain[0],
        'yanchor': 'bottom',
        'len': fig.layout.yaxis2.domain[1] - fig.layout.yaxis2.domain[0]
    })
    fig.add_traces(importance_plot.data, rows=1, cols=1)
    fig.add_traces(beeswarm_plot.data, rows=2, cols=1)

    # adjust importance plot
    fig.update_xaxes(title_text='mean absolute SHAP value', row=1, col=1)

    # adjust beeswarm plot
    fig.update_xaxes(title_text='SHAP value', row=2, col=1)
    fig.update_yaxes(
        ticktext=features,
        tickvals=list(range(n_features)),
        showgrid=False,
        zeroline=False,
        row=2, col=1
    )

    # adjust figure layout
    fig.update_layout(
        title=title,
        height=100+100*n_features,
        showlegend=False
    )

    return fig


def shap_interaction_plot(result: Dict, feature_pair: Tuple[str, str], title: str = 'SHAP interaction plot', max_num_samples: int = 250, show_colorbar: bool = True) -> go.Figure:
    """Creates an interaction plot for 2nd order effects between given features from given results.
    Plot is adapted from [here](https://github.com/slundberg/shap#shap-interaction-values).

    Args:
        result (Dict): A SHAP interaction tool's result dictionary.
        feature_pair (Tuple[str, str]): Feature pair to plot the interaction for.
        title (str, optional): Title for the plot. Defaults to 'SHAP interaction plot'.
        max_num_samples (int, optional): Maximum number of samples to show. Defaults to 250.
        show_colorbar (bool, optional): Whether to include the colorbar. Defaults to True.

    Returns:
        go.Figure: The generated SHAP interaction plot.
    """

    f_a, f_b = feature_pair

    if feature_pair not in result['misc']['shap_interaction_values'].keys():
        feature_pair = feature_pair[::-1]
    interaction_values = result['misc']['shap_interaction_values'][feature_pair]
    values_fa = result['misc']['values'][f_a]
    values_fb = result['misc']['values'][f_b]

    n_samples = len(interaction_values)
    cmin, cmax = minmax(values_fb)

    if n_samples > max_num_samples:  # only plot a random subset of samples
        i_samples = np.random.choice(n_samples, max_num_samples, replace=False)
        warnings.warn('Not showing all samples, only {max_num_samples} of them.'
                      .format(max_num_samples=max_num_samples), RuntimeWarning)
    else:
        i_samples = np.arange(n_samples)

    fig = go.Figure()

    # point color styling and colorbar
    markers = dict(
        cmin=cmin, cmax=cmax,
        color=values_fb[i_samples],
        opacity=0.5,
        colorscale='Bluered',
        colorbar=dict(title=f_b, titleside='right'),
        showscale=show_colorbar
    )

    points = go.Scatter(x=values_fa[i_samples], y=interaction_values[i_samples],
                        mode='markers', marker=markers, showlegend=False, name='SHAP interaction values')

    fig.add_trace(points)

    fig.update_layout(
        title=title,
        xaxis_title=f_a,
        yaxis_title='SHAP interaction value'
    )

    return fig


def shap_interaction_matrix_plot(result: Dict, title: str = 'SHAP interaction matrix plot') -> go.Figure:
    """Creates an interaction matrix plot for 2nd order effects between all feature pairs from given results.

    Args:
        result (Dict): A SHAP interaction tool's result dictionary.
        title (str, optional): Title for the plot. Defaults to 'SHAP interaction matrix plot'.

    Returns:
        go.Figure: The generated SHAP interaction matrix plot.
    """

    feature_pairs = get_features(result)
    features = get_features(result, no_pairs=True)
    n_features = len(features)

    fig = make_subplots(rows=n_features, cols=n_features, shared_xaxes=True)

    # make y-axes shared over all subplots
    for i in range(1, n_features**2):
        fig.layout['yaxis'+str(i+1)].matches = 'y'
        if (i) % n_features != 0:
            fig.layout['yaxis'+str(i+1)].showticklabels = False

    for i, f_a in enumerate(features):
        for j, f_b in enumerate(features):

            row, col = n_features-j, n_features-i
            cell = (row - 1) * n_features + col

            if i != j:
                # create interaction plot
                interaction_plot = shap_interaction_plot(result, feature_pair=(f_a, f_b),
                                                         show_colorbar=(i == 0 or (j == 0 and i == 1)))

                # adjust colorbar position
                y_axis_layout = fig.layout['yaxis'+(str(cell) if cell > 1 else '')]
                interaction_plot.data[0].marker.colorbar.update({
                    'y': y_axis_layout.domain[0],
                    'yanchor': 'bottom',
                    'len': y_axis_layout.domain[1] - y_axis_layout.domain[0]
                })

                fig.add_traces(interaction_plot.data, rows=row, cols=col)
            else:
                # create empty plots for diagonal
                fig.add_traces(go.Scatter(), rows=row, cols=col)

            # adjust axis labels
            if col == 1:
                fig.update_yaxes(title_text='SHAP interaction value', row=row, col=col)
            if row == n_features:
                fig.update_xaxes(title_text=f_a, row=row, col=col)

    fig.update_layout(
        title=title,
        height=100+200*(n_features),
        width=100+200*(n_features)
    )

    return fig

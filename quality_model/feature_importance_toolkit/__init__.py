class Analyzer:  # forward declaration
    pass

from .model import Model, PreloadedModel
from .analyzer import Analyzer, ImportanceAnalyzer, InteractionAnalyzer, CausalityAnalyzer
from . import types
from . import datasets
from . import plots
from . import tools

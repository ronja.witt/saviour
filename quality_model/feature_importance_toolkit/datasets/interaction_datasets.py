from typing import List, Tuple

import numpy as np
import pandas as pd

from ..types import DataType
from . import ArtificialDataset


class InteractionOneDataset(ArtificialDataset):
    """Interaction One Dataset containing random base features and artificial interactions for a regression task, generated sythetically.
    Consists of five observed random base features with one hidden interaction.
    """

    def __init__(self, name: str = 'Interaction One', file: str = './data/interaction_one_dataset.hdf5', n: int = 1000, test_size: float = 0.2, weights: List[float] = [1.0, 1.0, 1.0, 1.0], alpha: List[float] = [1.0], noise_variance: float = 0.1, include_hidden: bool = False, force: bool = False):
        """Initializes Interaction One dataset.

        Args:
            name (str, optional): Name of dataset instance. Defaults to 'Interaction One'.
            file (str, optional): Relative or absolute path to dataset file. If a dataset exists here already and is loaded, the following arguments are ignored. Defaults to './data/interaction_one_dataset.hdf5'.
            n (int, optional): Number of samples to create when generating new data. Defaults to 1000.
            test_size (float, optional): Proportion of all samples used for the testing data. Defaults to 0.2.
            weights (List[float], optional): Weighting coefficients for selected interaction and base features. Must have four entries. Defaults to [1.0, 1.0, 1.0, 1.0].
            alpha (List[float], optional): Weighting coefficients for interactions. Must have one entry. Defaults to [1.0].
            noise_variance (float, optional): Variance of Gaussian noise that is added to every base of interaction feature and to the target. Defaults to 0.1.
            include_hidden (bool, optional): Whether to include the otherwise hidden interaction features in the data when generating it.
            force (bool, optional): Whether to force the generation of a new dataset and overwrite a possibly existing one in `file`. Defaults to False.

        Raises:
            ValueError: Raised if `weights` does not have the expected number of entries.
            ValueError: Raised if `alpha` does not have the expected number of entries.
        """

        # set attributes
        if len(weights) == 4:
            self.weights = weights
        else:
            raise ValueError('Passed weights must have 4 entries. Got {n_entries} instead.'
                             .format(n_entries=len(weights)))

        if len(alpha) == 1:
            self.alpha = alpha
        else:
            raise ValueError('Passed alpha must have 1 entry. Got {n_entries} instead.'
                             .format(n_entries=len(alpha)))

        self.noise_variance = noise_variance
        self.include_hidden = include_hidden

        super().__init__(name=name, data_type=DataType.REGRESSION, file=file, n=n, test_size=test_size, force=force)

    def _generate(self) -> Tuple[pd.DataFrame, pd.Series]:
        """Generates artificial Interaction One dataset with five random base features and an artificial interaction for a regression task.
        Data is generated as follows:

        1. Five random base features `b0` to `b4` are sampled i.i.d. from `U(0,1) + N(0,s)` with variance `s` (sigma) for the Gaussian noise.

        2. An interaction is computed as `i0 = a0 * b0 * b1 + N(0,s)` with parameter `a` (alpha).

        3. A target is computed as `y = w * [i0 b2 b3 b4]ᵀ + N(0,s)` with weights `w = [w0 w1 w2 w3]`.

        Returns:
            Tuple[pd.DataFrame, pd.Series]: DataFrame of all base features and Series of labels for all samples.
        """

        # setup
        n_features = 5
        feature_names = ['base_{n}'.format(n=n) for n in range(n_features)]

        # create random base features
        base = np.random.random_sample((n_features, self.n))
        base += np.random.normal(0, self.noise_variance, base.shape)

        # set coefficients
        weights = np.array(self.weights)  # interaction0, base2, base3, base4
        alpha = self.alpha

        # compute interaction
        interaction0 = alpha[0] * base[0] * base[1]
        interaction0 += np.random.normal(0, self.noise_variance, interaction0.shape)

        # compute target labels
        inputs = np.array([interaction0, base[2], base[3], base[4]])
        target = weights.T @ inputs
        target += np.random.normal(0, self.noise_variance, target.shape)

        # construct data frame and series
        if self.include_hidden:
            interaction_names = ['interaction_0']
            x_all = np.concatenate([base, interaction0[np.newaxis]])
            x_all = pd.DataFrame(x_all.T, columns=feature_names+interaction_names)
        else:
            x_all = pd.DataFrame(base.T, columns=feature_names)
        y_all = pd.Series(target, name='target')

        return x_all, y_all


class InteractionTwoDataset(ArtificialDataset):
    """Interaction Two Dataset containing random base features and artificial interactions for a regression task, generated sythetically.
    Consists of five observed random base features with two hidden interactions.
    """

    def __init__(self, name: str = 'Interaction Two', file: str = './data/interaction_two_dataset.hdf5', n: int = 1000, test_size: float = 0.2, weights: List[float] = [1.0, 1.0, 1.0, 1.0], alpha: List[float] = [1.0, 1.0, 1.0, 1.0], noise_variance: float = 0.1, include_hidden: bool = False, force: bool = False):
        """Initializes Interaction Two dataset.

        Args:
            name (str, optional): Name of dataset instance. Defaults to 'Interaction Two'.
            file (str, optional): Relative or absolute path to dataset file. If a dataset exists here already and is loaded, the following arguments are ignored. Defaults to './data/interaction_two_dataset.hdf5'.
            n (int, optional): Number of samples to create when generating new data. Defaults to 1000.
            test_size (float, optional): Proportion of all samples used for the testing data. Defaults to 0.2.
            weights (List[float], optional): Weighting coefficients for selected interaction and base features. Must have four entries. Defaults to [1.0, 1.0, 1.0, 1.0].
            alpha (List[float], optional): Weighting coefficients for interactions. Must have 4 entries. Defaults to [1.0, 1.0, 1.0, 1.0].
            noise_variance (float, optional): Variance of Gaussian noise that is added to every base of interaction feature and to the target. Defaults to 0.1.
            include_hidden (bool, optional): Whether to include the otherwise hidden interaction features in the data when generating it.
            force (bool, optional): Whether to force the generation of a new dataset and overwrite a possibly existing one in `file`. Defaults to False.

        Raises:
            ValueError: Raised if `weights` does not have the expected number of entries.
            ValueError: Raised if `alpha` does not have the expected number of entries.
        """

        # set attributes
        if len(weights) == 4:
            self.weights = weights
        else:
            raise ValueError('Passed weights must have 4 entries. Got {n_entries} instead.'
                             .format(n_entries=len(weights)))

        if len(alpha) == 4:
            self.alpha = alpha
        else:
            raise ValueError('Passed alpha must have 4 entries. Got {n_entries} instead.'
                             .format(n_entries=len(alpha)))

        self.noise_variance = noise_variance
        self.include_hidden = include_hidden

        super().__init__(name=name, data_type=DataType.REGRESSION, file=file, n=n, test_size=test_size, force=force)

    def _generate(self) -> Tuple[pd.DataFrame, pd.Series]:
        """Generates artificial Interaction Two dataset with five random base features and two artificial interactions for a regression task.
        Data is generated as follows:

        1. Five random base features `b0` to `b4` are sampled i.i.d. from `U(0,1) + N(0,s)` with variance `s` (sigma) for the Gaussian noise.

        2. Two interactions are with parameters `a0` to `a3` (alpha) computed as

            a) `i0 = a0 * b0 + a1 * b0 * b1 + a2 * b1^2 + N(0,s)`.

            b) `i1 = a3 * b3 * b4 + N(0,s)`.

        3. A target is computed as `y = w * [i0 b2 i1 b4]ᵀ + N(0,s)` with weights `w = [w0 w1 w2 w3]`.

        Returns:
            Tuple[pd.DataFrame, pd.Series]: DataFrame of all base features and Series of labels for all samples.
        """

        # setup
        n_features = 5
        feature_names = ['base_{n}'.format(n=n) for n in range(n_features)]

        # create random base features
        base = np.random.random_sample((n_features, self.n))
        base += np.random.normal(0, self.noise_variance, base.shape)

        # set coefficients
        weights = np.array(self.weights)  # interaction0, base2, interaction1, base4
        alpha = self.alpha

        # compute interactions
        interaction0 = alpha[0] * base[0] + alpha[1] * base[0] * base[1] + alpha[2] * base[1]**2
        interaction1 = alpha[3] * base[3] * base[4]
        interaction0 += np.random.normal(0, self.noise_variance, interaction0.shape)
        interaction1 += np.random.normal(0, self.noise_variance, interaction1.shape)

        # compute target labels
        inputs = np.array([interaction0, base[2], interaction1, base[4]])
        target = weights.T @ inputs
        target += np.random.normal(0, self.noise_variance, target.shape)

        # construct data frame and series
        if self.include_hidden:
            interaction_names = ['interaction_{n}'.format(n=n) for n in range(2)]
            x_all = np.concatenate([base, interaction0[np.newaxis], interaction1[np.newaxis]])
            x_all = pd.DataFrame(x_all.T, columns=feature_names+interaction_names)
        else:
            x_all = pd.DataFrame(base.T, columns=feature_names)
        y_all = pd.Series(target, name='target')

        return x_all, y_all


class InteractionThreeDataset(ArtificialDataset):
    """Interaction Three Dataset containing random base features and artificial interactions for a regression task, generated sythetically.
    Consists of five observed random base features with two hidden interactions.
    """

    def __init__(self, name: str = 'Interaction Three', file: str = './data/interaction_three_dataset.hdf5', n: int = 1000, test_size: float = 0.2, weights: List[float] = [1.0, 1.0, 1.0, 1.0], alpha: List[float] = [1.0, 1.0, 1.0, 1.0], noise_variance: float = 0.1, include_hidden: bool = False, force: bool = False):
        """Initializes Interaction Three dataset.

        Args:
            name (str, optional): Name of dataset instance. Defaults to 'Interaction Three'.
            file (str, optional): Relative or absolute path to dataset file. If a dataset exists here already and is loaded, the following arguments are ignored. Defaults to './data/interaction_three_dataset.hdf5'.
            n (int, optional): Number of samples to create when generating new data. Defaults to 1000.
            test_size (float, optional): Proportion of all samples used for the testing data. Defaults to 0.2.
            weights (List[float], optional): Weighting coefficients for selected interaction and base features. Must have four entries. Defaults to [1.0, 1.0, 1.0, 1.0].
            alpha (List[float], optional): Weighting coefficients for interactions. Must have 4 entries. Defaults to [1.0, 1.0, 1.0, 1.0].
            noise_variance (float, optional): Variance of Gaussian noise that is added to every base of interaction feature and to the target. Defaults to 0.1.
            include_hidden (bool, optional): Whether to include the otherwise hidden interaction features in the data when generating it.
            force (bool, optional): Whether to force the generation of a new dataset and overwrite a possibly existing one in `file`. Defaults to False.

        Raises:
            ValueError: Raised if `weights` does not have the expected number of entries.
            ValueError: Raised if `alpha` does not have the expected number of entries.
        """

        # set attributes
        if len(weights) == 4:
            self.weights = weights
        else:
            raise ValueError('Passed weights must have 4 entries. Got {n_entries} instead.'
                             .format(n_entries=len(weights)))

        if len(alpha) == 4:
            self.alpha = alpha
        else:
            raise ValueError('Passed alpha must have 4 entries. Got {n_entries} instead.'
                             .format(n_entries=len(alpha)))

        self.noise_variance = noise_variance
        self.include_hidden = include_hidden

        super().__init__(name=name, data_type=DataType.REGRESSION, file=file, n=n, test_size=test_size, force=force)

    def _generate(self) -> Tuple[pd.DataFrame, pd.Series]:
        """Generates artificial Interaction Three dataset with five random base features and two artificial interactions for a regression task.
        Data is generated as follows:

        1. Five random base features `b0` to `b4` are sampled i.i.d. from `U(0,1) + N(0,s)` with variance `s` (sigma) for the Gaussian noise.

        2. Two interactions are with parameters `a0` to `a3` (alpha) computed as

            a) `i0 = b1 * (a0 * b0 + a1 * b1 * b2) + N(0,s)`.

            b) `i1 = a2 * b4 + a3 * b1 * b4 + N(0,s)`.

        3. A target is computed as `y = w * [i0 b2 i1 b4]ᵀ + N(0,s)` with weights `w = [w0 w1 w2 w3]`.

        Returns:
            Tuple[pd.DataFrame, pd.Series]: DataFrame of all base features and Series of labels for all samples.
        """

        # setup
        n_features = 5
        feature_names = ['base_{n}'.format(n=n) for n in range(n_features)]

        # create random base features
        base = np.random.random_sample((n_features, self.n))
        base += np.random.normal(0, self.noise_variance, base.shape)

        # set coefficients
        weights = np.array(self.weights)  # interaction0, base2, interaction1, base4
        alpha = self.alpha

        # compute interactions
        interaction0 = base[1] * (alpha[0] * base[0] + alpha[1] * base[1] * base[2])
        interaction1 = alpha[2] * base[4] + alpha[3] * base[1] * base[4]
        interaction0 += np.random.normal(0, self.noise_variance, interaction0.shape)
        interaction1 += np.random.normal(0, self.noise_variance, interaction1.shape)

        # compute target labels
        inputs = np.array([interaction0, base[2], interaction1, base[4]])
        target = weights.T @ inputs
        target += np.random.normal(0, self.noise_variance, target.shape)

        # construct data frame and series
        if self.include_hidden:
            interaction_names = ['interaction_{n}'.format(n=n) for n in range(2)]
            x_all = np.concatenate([base, interaction0[np.newaxis], interaction1[np.newaxis]])
            x_all = pd.DataFrame(x_all.T, columns=feature_names+interaction_names)
        else:
            x_all = pd.DataFrame(base.T, columns=feature_names)
        y_all = pd.Series(target, name='target')

        return x_all, y_all


class InteractionFourDataset(ArtificialDataset):
    """Interaction Four Dataset containing random base features and artificial interactions for a regression task, generated sythetically.
    Consists of five observed random base features with three hidden interactions.
    """

    def __init__(self, name: str = 'Interaction Four', file: str = './data/interaction_four_dataset.hdf5', n: int = 1000, test_size: float = 0.2, weights: List[float] = [1.0, 1.0, 1.0, 1.0], alpha: List[float] = [1.0, 1.0, 1.0, 1.0, 1.0], noise_variance: float = 0.1, include_hidden: bool = False, force: bool = False):
        """Initializes Interaction Four dataset.

        Args:
            name (str, optional): Name of dataset instance. Defaults to 'Interaction Four'.
            file (str, optional): Relative or absolute path to dataset file. If a dataset exists here already and is loaded, the following arguments are ignored. Defaults to './data/interaction_four_dataset.hdf5'.
            n (int, optional): Number of samples to create when generating new data. Defaults to 1000.
            test_size (float, optional): Proportion of all samples used for the testing data. Defaults to 0.2.
            weights (List[float], optional): Weighting coefficients for selected interaction and base features. Must have four entries. Defaults to [1.0, 1.0, 1.0, 1.0].
            alpha (List[float], optional): Weighting coefficients for interactions. Must have 5 entries. Defaults to [1.0, 1.0, 1.0, 1.0, 1.0].
            noise_variance (float, optional): Variance of Gaussian noise that is added to every base of interaction feature and to the target. Defaults to 0.1.
            include_hidden (bool, optional): Whether to include the otherwise hidden interaction features in the data when generating it.
            force (bool, optional): Whether to force the generation of a new dataset and overwrite a possibly existing one in `file`. Defaults to False.

        Raises:
            ValueError: Raised if `weights` does not have the expected number of entries.
            ValueError: Raised if `alpha` does not have the expected number of entries.
        """

        # set attributes
        if len(weights) == 4:
            self.weights = weights
        else:
            raise ValueError('Passed weights must have 4 entries. Got {n_entries} instead.'
                             .format(n_entries=len(weights)))

        if len(alpha) == 5:
            self.alpha = alpha
        else:
            raise ValueError('Passed alpha must have 5 entries. Got {n_entries} instead.'
                             .format(n_entries=len(alpha)))

        self.noise_variance = noise_variance
        self.include_hidden = include_hidden

        super().__init__(name=name, data_type=DataType.REGRESSION, file=file, n=n, test_size=test_size, force=force)

    def _generate(self) -> Tuple[pd.DataFrame, pd.Series]:
        """Generates artificial Interaction Four dataset with five random base features and three artificial interactions for a regression task.
        Data is generated as follows:

        1. Five random base features `b0` to `b4` are sampled i.i.d. from `U(0,1) + N(0,s)` with variance `s` (sigma) for the Gaussian noise.

        2. Three interactions are with parameters `a0` to `a4` (alpha) computed as

            a) `i0 = a0 * b0 + a1 * b0 * b1 + N(0,s)`.

            b) `i1 = b1 * (a2 * b2 * b4 + a3 * b4) + N(0,s)`.

            c) `i2 = a4 * b2 * i0 + N(0,s)`.

        3. A target is computed as `y = w * [i2 b1 i1 b4]ᵀ + N(0,s)` with weights `w = [w0 w1 w2 w3]`.

        Returns:
            Tuple[pd.DataFrame, pd.Series]: DataFrame of all base features and Series of labels for all samples.
        """

        # setup
        n_features = 5
        feature_names = ['base_{n}'.format(n=n) for n in range(n_features)]

        # create random base features
        base = np.random.random_sample((n_features, self.n))
        base += np.random.normal(0, self.noise_variance, base.shape)

        # set coefficients
        weights = np.array(self.weights)  # interaction2, base1, interaction1, base4
        alpha = self.alpha

        # compute interactions
        interaction0 = alpha[0] * base[0] + alpha[1] * base[0] * base[1]
        interaction1 = base[1] * (alpha[2] * base[2] * base[4] + alpha[3] * base[4])
        interaction2 = alpha[4] * base[2] * interaction0
        interaction0 += np.random.normal(0, self.noise_variance, interaction0.shape)
        interaction1 += np.random.normal(0, self.noise_variance, interaction1.shape)
        interaction2 += np.random.normal(0, self.noise_variance, interaction2.shape)

        # compute target labels
        inputs = np.array([interaction2, base[1], interaction1, base[4]])
        target = weights.T @ inputs
        target += np.random.normal(0, self.noise_variance, target.shape)

        # construct data frame and series
        if self.include_hidden:
            interaction_names = ['interaction_{n}'.format(n=n) for n in range(3)]
            x_all = np.concatenate([base, interaction0[np.newaxis], interaction1[np.newaxis], interaction2[np.newaxis]])
            x_all = pd.DataFrame(x_all.T, columns=feature_names+interaction_names)
        else:
            x_all = pd.DataFrame(base.T, columns=feature_names)
        y_all = pd.Series(target, name='target')

        return x_all, y_all

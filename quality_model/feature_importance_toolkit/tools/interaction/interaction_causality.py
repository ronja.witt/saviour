from copy import deepcopy
from typing import Dict, List, Tuple, Union

import numpy as np
import pandas as pd
from scipy.stats import norm

from ...analyzer import Analyzer
from ...datasets import Dataset, PreloadedDataset
from ...model import Model
from ...plots import causality_graph_plot
from ...types import ResultType
from .. import CausalityTool
from ..gs import GSCausality
from ..pc import PCCausality
from ..pdp import PDPInteraction
from ..utils import compute_tool, interaction_name_generator, interpolate_2D, pairs_from_features
from ..vip import VIPInteraction


class InteractionCausality(CausalityTool):
    """Tool for determining directed causal graphs (Bayesian networks) between inputs and output feature, including hidden interaction nodes.
    Hidden intermediate nodes are identified based on significant VIP interaction values between feature pairs. PDP interaction results are then evaluated for each sample to obtain data for each interaction node. The VIP-PDP loop is repeated until no further changes can be made. Using this augmented data combined with target values, the PC algorithm is applied to obtain a Bayesian network structure estimation.
    """

    def __init__(self, name: str = 'Interaction Causality', mode: str = 'gs', alpha_level: float = 0.1, max_num_iterations: int = 1, vip_grid_size: int = 50, vip_percentiles: Tuple[float, float] = (0.0, 1.0), pdp_grid_size: int = 50, pdp_percentiles: Tuple[float, float] = (0.0, 1.0), include_interaction_results: bool = False, pc_ci_test: str = 'pearsonr', pc_alpha_level: float = 0.01):
        """Initialize Interaction Causality Tool.

        Args:
            name (str, optional): Name of the tool. Defaults to 'Interaction Causality'.
            mode (str, optional): Which underlying tool to use for Bayesian network estimation. Either 'gs' (Grow-Shrink algorithm) or 'pc' (PC algorithm). Defaults to 'gs'.
            alpha_level (float, optional): Significance level to be exceeded by an interaction compared to the population of interaction values. Defaults to 0.05.
            max_num_iterations(int, optional): Maximum number of VIP-PDP-PC iterations to conduct before no more hidden iteraction variables are attempted to be discovered. Must be at least 1. Defaults to 2.
            vip_grid_size (int, optional): Maximum number of grid points for the VIP analysis. Defaults to 50.
            vip_percentiles (Tuple[float, float], optional): Lower and upper percentiles for the VIP-grid's extreme values. Defaults to (0.0, 1.0).
            pdp_grid_size (int, optional): Maximum number of grid points for the PDP analysis. Defaults to 50.
            pdp_percentiles (Tuple[float, float], optional): Lower and upper percentiles for the PDP-grid's extreme values. Defaults to (0.0, 1.0).
            include_interaction_results (bool, optional): Whether to include interaction results, meaning interacting variable pairs and the corresponding PDP results, in the result dictionary. Defaults to False.
            pc_ci_test (str, optional): Statistical test for the PC analysis. 'pearsonr' is required for continuous variables. Ignored if `mode` is not 'pc'. Defaults to 'pearsonr'.
            pc_alpha_level (float, optional): Statistical significante level for the PC analysis. Ignored if `mode` is not 'pc'. Defaults to 0.01.
        """

        # set mode
        exp_modes = ['gs', 'pc']
        if mode in exp_modes:
            self.mode = mode
        else:
            raise ValueError('Mode must be one of {exp_modes}. Got {mode} instead'
                             .format(exp_modes=exp_modes, mode=mode))

        super().__init__(name, result_type=ResultType.GRAPH)

        # set attributes
        self.interaction_threshold = None
        self.alpha_level = alpha_level
        self.max_num_iterations = max(1, max_num_iterations)
        self.vip_grid_size = vip_grid_size
        self.vip_percentiles = vip_percentiles
        self.pdp_grid_size = pdp_grid_size
        self.pdp_percentiles = pdp_percentiles
        self.include_interaction_results = include_interaction_results
        self.pc_ci_test = pc_ci_test
        self.pc_alpha_level = pc_alpha_level

    def run(self, model: Model, dataset: Dataset, features: List[str], analyzer: Analyzer = None) -> Dict[str, List[Union[str, Tuple[str, str]]]]:

        result = {}
        if self.include_interaction_results:
            result['misc'] = {
                'pdp': {},
                'interactions': {}
            }

        # prepare variables for data extension loop
        ext_model = model
        ext_dataset = dataset
        ext_features = features.copy()
        feature_pairs = pairs_from_features(features)
        self.interaction_names = interaction_name_generator(dataset.features)

        for i in range(self.max_num_iterations):
            if i > 0:
                # fit model on extended data
                ext_model = self._refit_model(ext_model, ext_dataset)

            # identify significant feature interactions with VIP
            interactions = self._find_interactions(ext_model, ext_dataset, feature_pairs, analyzer, result)
            if not interactions:
                break  # terminate loop if no significant interactions are found

            # extend dataset with PDP interaction values
            ext_dataset = self._extend_dataset(ext_model, ext_dataset, interactions, analyzer, result)

            ext_features += list(interactions.keys())
            # take feature pairs of previous interactions for following analysis
            feature_pairs = [(f_a, f_b) for (f_a, f_b) in pairs_from_features(ext_features)
                             if f_a in interactions.keys() and f_b in interactions.keys()]

        # compute bayesian network structure for extended dataset
        if self.mode == 'gs':
            bn_result = compute_tool(GSCausality, model, ext_dataset, ext_features, analyzer)
        elif self.mode == 'pc':
            bn_result = compute_tool(PCCausality, model, ext_dataset, ext_features, analyzer,
                                     ci_test=self.pc_ci_test, alpha_level=self.pc_alpha_level)

        result['nodes'] = ext_features + [dataset.target]
        result['edges'] = bn_result['edges']

        return result

    def _refit_model(self, model: Model, dataset: Dataset) -> Model:
        """Duplicates an existing model to retrain it on different data.

        Args:
            model (Model): Model to duplicate and refit.
            dataset (Dataset): Dataset to refit new model on.

        Returns:
            Model: The refitted model.
        """

        new_model = deepcopy(model)

        x_train, y_train = dataset.get_train()
        new_model.fit(x_train, y_train)

        return new_model

    def _find_interactions(self, model: Model, dataset: Dataset, feature_pairs: List[Tuple[str, str]], analyzer: Analyzer, result: Dict) -> Dict[str, Tuple[str, str]]:
        """Determines significant interactions by testing with p-values for large VIP interaction values compared to the population of all VIP interaction values.

        Args:
            model (Model): Model to use for VIP analysis.
            dataset (Dataset): Dataset to use for VIP analysis.
            feature_pairs (List[Tuple[str, str]]): List of feature pairs to consider.
            analyzer (Analyzer): Analyzer this tool is run from.
            result (Dict): Tool's result dictionary in which to include interacting variable pairs if `self.include_interaction_results` is True.

        Returns:
            Dict[str, Tuple[str, str]]: The dictionary of significant interactions with their feature pairs.
        """

        # compute VIP interaction
        vip_results = compute_tool(VIPInteraction, model, dataset, feature_pairs, analyzer,
                                   grid_size=self.vip_grid_size, percentiles=self.vip_percentiles)
        vip_values = [vip_results[pair] for pair in feature_pairs]

        # determine threshold for significant interactions
        if self.interaction_threshold is None:
            loc, scale = np.mean(vip_values), np.std(vip_values)
            self.interaction_threshold = norm.isf(self.alpha_level, loc=loc, scale=scale)

        # determine significant interactions
        interactions = {}
        for i, feature_pair in enumerate(feature_pairs):
            if vip_values[i] > self.interaction_threshold:
                name = next(self.interaction_names)
                interactions[name] = feature_pair

        if self.include_interaction_results:
            result['misc']['interactions'].update(interactions)

        return interactions

    def _extend_dataset(self, model: Model, dataset: Dataset, interactions: Dict[str, Tuple[str, str]], analyzer: Analyzer, result: Dict) -> Dataset:
        """Extends a given dataset with interaction samples obtained from PDP results for given feature pairs.

        Args:
            model (Model): Model to use for the PDP analysis.
            dataset (Dataset): Dataset to use for the PDP analysis and to extend with interaction features.
            interactions (Dict[str, Tuple[str, str]]): Dictionary of interactions to be added.
            analyzer (Analyzer): Analyzer this tool is run from.
            result (Dict): Tool's result dictionary in which to include PDP results if `self.include_interaction_results` is True.

        Returns:
            Dataset: The extended dataset including interaction data samples.
        """

        # compute PDP interaction for significant interactions
        pdp_results = compute_tool(PDPInteraction, model, dataset, list(interactions.values()), analyzer,
                                   grid_size=self.pdp_grid_size, percentiles=self.pdp_percentiles)

        if self.include_interaction_results:
            result['misc']['pdp'].update(pdp_results)

        # get data
        x_train, y_train = dataset.get_train()
        x_test, y_test = dataset.get_test()

        # add PDP-computed interactions to new extended data
        ext_x_train = x_train
        ext_x_test = x_test
        for name, (f_a, f_b) in interactions.items():
            # sample PDP results for given base feature samples
            pdp_scores = pdp_results[(f_a, f_b)]['scores']
            pdp_grid = [pdp_results[(f_a, f_b)][f_a], pdp_results[(f_a, f_b)][f_b]]
            pdp_values_train = interpolate_2D(x_train[[f_a, f_b]].to_numpy(), pdp_scores, pdp_grid)
            pdp_values_test = interpolate_2D(x_test[[f_a, f_b]].to_numpy(), pdp_scores, pdp_grid)

            ext_x_train = ext_x_train.merge(pd.Series(pdp_values_train, index=x_train.index, name=name),
                                            left_index=True, right_index=True)
            ext_x_test = ext_x_test.merge(pd.Series(pdp_values_test, index=x_test.index, name=name),
                                          left_index=True, right_index=True)

        # assemble extended dataset
        ext_dataset = PreloadedDataset(ext_x_train, y_train, ext_x_test, y_test,
                                       data_type=dataset.data_type, name=f'{dataset.name} (extended)', verbose=False)

        return ext_dataset

    def plot(self, result: Dict, title: str = 'Interaction', plot_config: Dict = None, **kwargs):

        fig = causality_graph_plot(result, title=title, **kwargs)
        fig.show(config=plot_config)

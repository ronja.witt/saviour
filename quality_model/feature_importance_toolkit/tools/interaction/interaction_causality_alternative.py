from typing import Dict, List, Tuple, Union

from ...analyzer import Analyzer
from ...datasets import Dataset
from ...model import Model
from ...plots import causality_graph_plot
from ..gs import GSCausality
from ..pc import PCCausality
from ..utils import compute_tool, features_from_pairs, interaction_name_generator, pairs_from_features
from . import InteractionCausality


class InteractionCausalityAlternative(InteractionCausality):
    """Tool for determining directed causal graphs (Bayesian networks) between inputs and output feature, including hidden interaction nodes.
    The GS or PC algorithm is initially applied to lay out causal directions between input features and target. Hidden intermediate nodes are, similar to the Interaction Causality Tool, identified based on significant VIP interaction values between feature pairs that share a target node. Interaction nodes are added to the graph, edges are redirected accordingly. PDP interaction results are then evaluated for each sample to obtain data for each interaction node. The VIP-PDP loop is repeated on the extended data, with a newly fitted model, until no further changes can be made.
    """

    def __init__(self, name: str = 'Interaction Causality Alternative', mode: str = 'gs', alpha_level: float = 0.1, max_num_iterations: int = 1, vip_grid_size: int = 50, vip_percentiles: Tuple[float, float] = (0.0, 1.0), pdp_grid_size: int = 50, pdp_percentiles: Tuple[float, float] = (0.0, 1.0), include_interaction_results: bool = True, pc_ci_test: str = 'pearsonr', pc_alpha_level: float = 0.01):
        """Initialize Interaction Causality Tool.

        Args:
            name (str, optional): Name of the tool. Defaults to 'Interaction Causality'.
            mode (str, optional): Which underlying tool to use for Bayesian network estimation. Either 'gs' (Grow-Shrink algorithm) or 'pc' (PC algorithm). Defaults to 'gs'.
            alpha_level (float, optional): Significance level to be exceeded by an interaction compared to the population of interaction values. Defaults to 0.05.
            max_num_iterations(int, optional): Maximum number of VIP-PDP iterations for data and graph extension to conduct before no more hidden iteraction variables are attempted to be discovered. Must be at least 1. Defaults to 2.
            vip_grid_size (int, optional): Maximum number of grid points for the VIP analysis. Defaults to 50.
            vip_percentiles (Tuple[float, float], optional): Lower and upper percentiles for the VIP-grid's extreme values. Defaults to (0.0, 1.0).
            pdp_grid_size (int, optional): Maximum number of grid points for the PDP analysis. Defaults to 50.
            pdp_percentiles (Tuple[float, float], optional): Lower and upper percentiles for the PDP-grid's extreme values. Defaults to (0.0, 1.0).
            include_interaction_results (bool, optional): Whether to include interaction results, meaning interacting variable pairs and the corresponding PDP results, in the result dictionary. Defaults to False.
            pc_ci_test (str, optional): Statistical test for the PC analysis. 'pearsonr' is required for continuous variables. Ignored if `mode` is not 'pc'. Defaults to 'pearsonr'.
            pc_alpha_level (float, optional): Statistical significante level for the PC analysis. Ignored if `mode` is not 'pc'. Defaults to 0.01.
        """

        super().__init__(name, mode, alpha_level, max_num_iterations, vip_grid_size,
                         vip_percentiles, pdp_grid_size, pdp_percentiles, include_interaction_results, pc_ci_test, pc_alpha_level)

    def run(self, model: Model, dataset: Dataset, features: List[str], analyzer: Analyzer = None) -> Dict[str, List[Union[str, Tuple[str, str]]]]:

        result = {}
        if self.include_interaction_results:
            result['misc'] = {
                'pdp': {},
                'interactions': {}
            }

        # compute bayesian network structure for unextended dataset
        if self.mode == 'gs':
            bn_result = compute_tool(GSCausality, model, dataset, features, analyzer)
        elif self.mode == 'pc':
            bn_result = compute_tool(PCCausality, model, dataset, features, analyzer,
                                     ci_test=self.pc_ci_test, alpha_level=self.pc_alpha_level)
        edges = bn_result['edges']

        # prepare variables for data extension loop
        ext_model = model
        ext_dataset = dataset
        ext_features = features.copy()
        feature_pairs = pairs_from_features(features)
        self.interaction_names = interaction_name_generator(dataset.features)

        for i in range(self.max_num_iterations):
            if i > 0:
                # fit model on extended data
                ext_model = self._refit_model(ext_model, ext_dataset)

            joining_pairs = self._find_joining_pairs(feature_pairs, dataset.target, edges)

            # identify significant feature interactions with VIP
            interactions = self._find_interactions(ext_model, ext_dataset, list(joining_pairs.keys()), analyzer, result)
            if not interactions:
                break  # terminate loop if no significant interactions are found

            # extend dataset with PDP interaction values
            ext_dataset = self._extend_dataset(ext_model, ext_dataset, interactions, analyzer, result)

            ext_features += list(interactions.keys())
            edges = self._adjust_interaction_edges(edges, interactions, joining_pairs)
            # take feature pairs with at least one previous interaction for following analysis
            feature_pairs = [(f_a, f_b) for (f_a, f_b) in pairs_from_features(ext_features)
                             if f_a in interactions.keys() or f_b in interactions.keys()]

        result['nodes'] = ext_features + [dataset.target]
        result['edges'] = edges

        return result

    def _find_joining_pairs(self, feature_pairs: List[Tuple[str, str]], target_feature: str, edges: List[Tuple[str, str]]) -> Dict[Tuple[str, str], List[str]]:
        """Finds pairs of feature nodes from a given list of directed edges that are pointing at the same target node.
        The additional condition holds, that this target must have at least three incoming edges.
        Example: for edges A→D, B→D, C→D, the result is { (A, B): [D], (A, C): [D], (B, C): [D] }.

        Args:
            feature_pairs (List[Tuple[str, str]]): Feature pairs to check for common target nodes.
            target_feature (str): Dataset target feature.
            edges (List[Tuple[str, str]]): List of directed edges of the considered graph.

        Returns:
            Dict[Tuple[str, str], List[str]]: Dictionary with identified feature pairs as keys and lists of features both features are pointing at as values.
        """

        features = features_from_pairs(feature_pairs)
        edges_incoming = {f: [] for f in features+[target_feature]}
        joining_pairs = {}

        for n1, n2 in edges:
            if n2 in features+[target_feature]:
                edges_incoming[n2].append(n1)

        for f_a, f_b in feature_pairs:
            for f, sources in edges_incoming.items():
                if (len(sources) > 2) and (f_a in sources and f_b in sources):
                    if (f_a, f_b) in joining_pairs.keys():
                        joining_pairs[(f_a, f_b)].append(f)
                    else:
                        joining_pairs[(f_a, f_b)] = [f]

        return joining_pairs

    def _adjust_interaction_edges(self, edges: List[Tuple[str, str]], interactions: Dict[str, Tuple[str, str]], joining_pairs: Dict[Tuple[str, str], List[str]]) -> List[Tuple[str, str]]:
        """Extends edges in a given graph to include additional interaction nodes between two feature nodes and a target node.
        The edges from both features to the target node are redirected towards a new interaction node and an additional edge from the interaction to the target node is introduced.
        Example: for an interaction between A and B, the edges A→C and B→C are replaced by A→I, B→I and I→C.

        Args:
            edges (List[Tuple[str, str]]): _description_
            interactions (Dict[str, Tuple[str, str]]): _description_
            joining_pairs (Dict[Tuple[str, str], List[str]]): _description_

        Returns:
            List[Tuple[str, str]]: _description_
        """

        for interaction, (f_a, f_b) in interactions.items():
            edges.append((f_a, interaction))
            edges.append((f_b, interaction))
            for target in joining_pairs[(f_a, f_b)]:
                edges.append((interaction, target))
                if (f_a, target) in edges:
                    edges.remove((f_a, target))
                if (f_b, target) in edges:
                    edges.remove((f_b, target))

        return edges

    def plot(self, result: Dict, title: str = 'Interaction Alternative', plot_config: Dict = None, **kwargs):

        fig = causality_graph_plot(result, title=title, **kwargs)
        fig.show(config=plot_config)

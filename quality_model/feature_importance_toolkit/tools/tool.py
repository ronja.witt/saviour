import numpy as np
from abc import ABC, abstractmethod
from numbers import Real
from typing import Dict, Union, Tuple, List

from ..analyzer import Analyzer
from ..model import Model
from ..datasets import Dataset
from ..types import ToolType, ResultType


class Tool(ABC):
    """Base Class for all tools.
    Each tool executes an analysis for a given model and dataset.
    (abstract base class, derive)

    Attributes:
        name (str): Name of the tool.
        tool_type (ToolType): Type of analysis the tool is used for (either importance, interaction or causality).
        result_type (ResultType): Type of result output by the tool, depending on output data dimensionality.
    """

    def __init__(self, name: str, tool_type: ToolType, result_type: ResultType):
        """Initializes abstract base of tool with given name, tool type and result type.

        Args:
            name (str): Name of the tool.
            tool_type (ToolType): Type of analysis the tool is used for (either importance, interaction or causality).
            result_type (ResultType): Type of result output by the tool, depending on output data dimensionality.
        """

        # set attributes
        self.name = name
        self.tool_type = tool_type
        self.result_type = result_type

    @abstractmethod
    def run(self, model: Model, dataset: Dataset, features: Union[List[str], List[Tuple[str, str]]], analyzer: Analyzer = None) -> Dict:
        """Runs the tool on a given model and dataset for the specified target features and returns the computed results.
        (abstract method, override)

        Expected result format is a dictionary with features as keys.
        The format of corresponding values is different for each result type.
        Expected feature result formats for these types are the following:

        1. `ResultType.DIM1`:
        `Real` (for one feature or feature pair only):
        Single scalar real value.

        2. `ResultType.DIM2`:
        `Dict[str, np.ndarray[Real]]` (for one feature only):
        Dictionary with two keys, the feature name and 'score', each with a 1D np.ndarray of real values as their value.
        Arrays have the same length.
        An optional third key 'misc' may have a dictionary with additional information as its value.

        3. `ResultType.DIM3`:
        `Dict[str, np.ndarray[Real]` (for one feature pair only):
        Dictionary with three keys, the two feature names and 'score'.
        The first two feature keys have a 1D np.ndarray of real values as their value.
        The 'score' key has a 2D np.ndarray of real values as its value.
        The number of values in scores is the product of feature array lengths.
        An optional fourth key 'misc' may have a dictionary with additional information as its value.

        4. `ResultType.GRAPH`:
        `Dict[str, Union[List[str], List[Tuple[str, str]]]]` (complete for all features):
        Dictionary with two keys, 'nodes' and 'edges'.
        The 'nodes' key has a list of all features considered in the graph.
        The 'edges' key has a list of feature pairs for all directed edges in the graph.
        An optional third key 'misc' may have a dictionary with additional information as its value.

        Args:
            model (Model): Model to use for the analysis.
            dataset (Dataset): Dataset to use for the analysis.
            features (Union[List[str], List[Tuple[str, str]]]): List of features to run the analysis tool for.
            analyzer: (Analyzer, optional): Analyzer the tool is run from. Defaults to None.

        Returns:
            Dict: Dictionary of the computed results, following the format specified above.
        """

        pass

    def plot(self, result: Dict, title: str = None, plot_config: Dict = None, **kwargs):
        """Creates and shows one or multiple plots of the given results for this tool.
        Plots created here are also included in the analysis report.
        (override)

        Keyworded arguments are passed to the respective plotting functions.

        Args:
            result (Dict): Results saved by the analyzer from running this tool.
            title (str): Title of the plot.
            plot_config (Dict, optional): Configuration dictionary for Plotly's `show()` method ([reference](https://plotly.com/python/configuration-options/)). Defaults to None.

        Raises:
            NotImplementedError: Raised if plotting is not implemented in derived tools.
        """

        raise NotImplementedError()

    def _check_result_format(self, result: Dict, features: List[Union[str, Tuple[str, str]]]):
        """Checks if the passed result adheres to the expected format for this tool's result type.
        The Analyzer checks the output of the `run`-function here.

        Args:
            result (Dict): Result to check the form of.
            feature (List[Union[str, Tuple[str, str]]]): Feature the passed result is the result for.

        Raises:
            AssertionError: Raised if any of the expected result format properties are not met.
            ValueError: Raised if the tool's result type is not expected/supported.
        """

        try:

            # expected format is a dictionary with features as keys
            # expected feature format is a str for importance tools or a Tuple[str, str] for interaction tools
            # optional additional key is 'misc' and its value a Dict

            assert isinstance(result, dict), \
                f'Result is of type {type(result)}. Expected a dict.'

            assert isinstance(features, list), \
                f'Features are of type {type(features)}. Expected a list.'

            if self.tool_type in [ToolType.IMPORTANCE, ToolType.CAUSALITY]:

                assert all(isinstance(f, str) for f in features), \
                    f'Not all passed features for importance tool are of type str.'

            elif self.tool_type == ToolType.INTERACTION:

                assert all(isinstance(f, tuple) for f in features), \
                    f'Not all passed features for interaction tool are of type tuple.'

                for feature in features:

                    assert len(feature) == 2, \
                        f'Length of feature tuple is {len(feature)}. Expected 2.'

                    feature_types = [type(f) for f in feature]
                    exp_feature_types = [str, str]
                    assert feature_types == exp_feature_types, \
                        f'Feature tuple contains types {tuple(feature_types)}. Expected {tuple(exp_feature_types)}.'

            n_keys = len(result.keys())
            if self.result_type == ResultType.GRAPH:
                exp_n_keys = 2
            else:
                exp_n_keys = len(features)
            if 'misc' in result.keys():
                exp_n_keys += 1
            assert n_keys == exp_n_keys, \
                f'Result has {n_keys} keys. Expected exactly {exp_n_keys} keys.'

            keys = set(result.keys())
            if 'misc' in keys:
                keys.remove('misc')
            if self.result_type == ResultType.GRAPH:
                exp_keys = set(['nodes', 'edges'])
            else:
                exp_keys = set(features)
            assert keys == exp_keys, \
                f'Result has keys {keys}. Expected keys {exp_keys}.'

            if self.result_type in [ResultType.DIM1, ResultType.DIM2, ResultType.DIM3]:

                # each key has as its value a real value or a dictionary
                # depending on the return type, different formats are possible

                for feature, subresult in result.items():

                    try:

                        if feature == 'misc':
                            assert isinstance(subresult, dict), \
                                f'Result is of type {type(subresult)}. Expected a dict.'

                        elif self.result_type is ResultType.DIM1:
                            # expected format is a single scalar real value

                            assert isinstance(subresult, Real), \
                                f'Result is of type {type(subresult)}. Expected a real value.'

                        elif self.result_type is ResultType.DIM2:
                            # expected format is a dictionary with two or three keys
                            # first key is the feature and its value a 1D np.ndarray[Real]
                            # second key is 'scores' and its value a 1D np.ndarray[Real]
                            # optional third key is 'misc' and its value a Dict
                            # all arrays of real values have the same length
                            # expected feature format is a str

                            assert isinstance(subresult, dict), \
                                f'Result is of type {type(subresult)}. Expected a dict.'

                            assert isinstance(feature, str), \
                                f'Feature is of type {type(feature)}. Expected a str.'

                            n_keys = len(subresult.keys())
                            assert n_keys == 2 or n_keys == 3, \
                                f'Result has {n_keys} {"key" if n_keys==1 else "keys"}. Expected 2 or 3 keys.'

                            keys = set(subresult.keys())
                            exp_keys = {feature, 'scores'}
                            assert all(k in keys for k in exp_keys), \
                                f'Result has keys {keys}. Expected keys {exp_keys}.'

                            lengths = []
                            for key, value in subresult.items():

                                if key == 'misc':

                                    assert isinstance(value, dict), \
                                        f'Result value for key {key} is of type {type(value)}. Expected a dict.'

                                else:

                                    assert isinstance(value, np.ndarray), \
                                        f'Result value for key {key} is of type {type(value)}. Expected a np.ndarray.'

                                    assert np.issubdtype(value.dtype, np.number), \
                                        f'Result array for {key} has data type {value.dtype}. Expected real values.'

                                    assert len(value.shape) == 1, \
                                        f'Result array for key {key} has dimension {len(value.shape)}. Expected 1.'

                                    lengths.append(len(value))

                            assert len(set(lengths)) <= 1, \
                                f'Lists of real values have varying lengths {lengths}. Expected equal lengths.'

                        elif self.result_type is ResultType.DIM3:
                            # expected format is a dictionary with three or four keys
                            # first key is the feature pair's first feature and its value a 1D np.ndarray[Real]
                            # second key is the feature pair's second feature and its value a 1D np.ndarray[Real]
                            # third key is 'scores' and its value a 2D np.ndarray[Real]:
                            # optional fourth key is 'misc' and its value a Dict
                            # product of feature array lengths is number of elements in scores
                            # expected feature format is a Tuple[str, str]

                            assert isinstance(subresult, dict), \
                                f'Result is of type {type(subresult)}. Expected a dict.'

                            assert isinstance(feature, tuple), \
                                f'Feature is of type {type(feature)}. Expected a tuple.'

                            n_keys = len(subresult.keys())
                            assert n_keys == 3 or n_keys == 4, \
                                f'Result has {n_keys} {"key" if n_keys==1 else "keys"}. Expected 3 or 4 keys.'

                            keys = set(subresult.keys())
                            exp_keys = {feature[0], feature[1], 'scores'}
                            assert all(k in keys for k in exp_keys), \
                                f'Result has keys {keys}. Expected keys {exp_keys}.'

                            lengths = []
                            size = 0
                            for key, value in subresult.items():

                                if key == 'misc':

                                    assert isinstance(value, dict), \
                                        f'Result value for key {key} is of type {type(value)}. Expected a dict.'

                                else:

                                    assert isinstance(value, np.ndarray), \
                                        f'Result value for key {key} is of type {type(value)}. Expected a np.ndarray.'

                                    assert np.issubdtype(value.dtype, np.number), \
                                        f'Result array for {key} has data type {value.dtype}. Expected real values.'

                                    if key == 'scores':

                                        assert len(value.shape) == 2, \
                                            f'Result array for key {key} has dimension {len(value.shape)}. Expected 2.'

                                        size = len(value[0]) * len(value)

                                    else:

                                        assert len(value.shape) == 1, \
                                            f'Result array for key {key} has dimension {len(value.shape)}. Expected 1.'

                                        lengths.append(len(value))

                            scores_key = 'scores'
                            exp_size = np.product(lengths)
                            assert size == exp_size, \
                                f'Number of elements for key {scores_key} is {size}. Expected it to be equal to {exp_size}, the product of feature list lengths {lengths}.'

                    except Exception as e:
                        # append feature in error message
                        e.args = (e.args[0] + f' Feature: {feature}.',)
                        raise e

            elif self.result_type is ResultType.GRAPH:
                # expected format is different for each key
                # first key is 'nodes' and its value a List[str]
                # second key is 'edges' and its value a List[Tuple[str, str]]
                # optional third key is 'misc' and its value a Dict
                # expected key format is a str

                for key, value in result.items():

                    try:

                        assert isinstance(key, str), \
                            f'Key is of type {type(key)}. Expected a str.'

                        if key == 'misc':

                            assert isinstance(value, dict), \
                                f'Result value for key {key} is of type {type(value)}. Expected a dict.'

                        if key == 'nodes':

                            assert isinstance(value, list), \
                                f'Result value for key {key} is of type {type(value)}. Expected a list.'

                            assert all(isinstance(n, str) for n in value), \
                                f'Not all passed features for interaction tool are of type str.'

                        if key == 'edges':

                            assert isinstance(value, list), \
                                f'Result value for key {key} is of type {type(value)}. Expected a list.'

                            assert all(isinstance(e, tuple) for e in value), \
                                f'Not all passed features for interaction tool are of type tuple.'

                            for edge in value:

                                assert len(edge) == 2, \
                                    f'Length of edge tuple is {len(edge)}. Expected 2.'

                                node_types = [type(n) for n in edge]
                                exp_node_types = [str, str]
                                assert node_types == exp_node_types, \
                                    f'Edge tuple contains node types {tuple(node_types)}. Expected {tuple(exp_node_types)}.'

                    except Exception as e:
                        # append key in error message
                        e.args = (e.args[0] + f' Key: {key}.',)
                        raise e

            else:
                raise ValueError('Encountered unexpected result type {result_type}.'
                                 .format(result_type=self.result_type))

        except Exception as e:
            # append tool in error message
            e.args = (e.args[0] + f' Tool: {self.name}.',)
            raise e


class ImportanceTool(Tool):
    """Tool for feature importance analysis.
    """

    def __init__(self, name: str, result_type: ResultType):

        super().__init__(name=name, tool_type=ToolType.IMPORTANCE, result_type=result_type)


class InteractionTool(Tool):
    """Tool for feature interaction analysis.
    """

    def __init__(self, name: str, result_type: ResultType):

        super().__init__(name=name, tool_type=ToolType.INTERACTION, result_type=result_type)


class CausalityTool(Tool):
    """Tool for feature causality analysis.
    """

    def __init__(self, name: str, result_type: ResultType):

        super().__init__(name=name, tool_type=ToolType.CAUSALITY, result_type=result_type)

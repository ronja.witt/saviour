from typing import Dict, List

import numpy as np
import shap

from ...analyzer import Analyzer
from ...datasets import Dataset
from ...model import Model
from ...plots import shap_summary_plot
from ...types import DataType, ResultType
from .. import ImportanceTool


class SHAPImportance(ImportanceTool):
    """Tool for determining Shapley Additive Explanations (SHAP) for single features.
    Using implementation from [shap](https://github.com/slundberg/shap).
    """

    def __init__(self, name='SHAP Importance', **kwargs):
        """Initializes SHAP Importance tool.

        Keyworded arguments are passed to the `Explainer` object from shap.

        Args:
            name (str, optional): Name of the tool. Defaults to 'SHAP Importance'.
        """

        super().__init__(name, result_type=ResultType.DIM1)

        # set attributes
        self.kwargs = kwargs

    def run(self, model: Model, dataset: Dataset, features: List[str], analyzer: Analyzer = None) -> Dict[str, float]:

        # get data
        x_train, _ = dataset.get_train()
        x_test, _ = dataset.get_test()
        x_features = x_train.columns.to_list()

        # compute scores
        explainer = shap.Explainer(model.model.predict, masker=x_train, **self.kwargs)
        shap_values = explainer(x_test).values
        if dataset.data_type == DataType.CLASSIFICATION:
            if len(shap_values.shape) == 3:
                shap_values = shap_values[1]  # TODO add support for more than two classes
        res = np.mean(np.abs(shap_values), axis=0)

        result = {
            f: res[x_features.index(f)] for f in features
        }
        result['misc'] = {
            'shap_values': {
                f: shap_values[:, x_features.index(f)] for f in features
            },
            'values': {
                f: x_test[f].to_numpy() for f in x_features
            }
        }

        return result

    def plot(self, result: Dict, title: str = 'Shapley Additive Explanations (SHAP)', plot_config: Dict = None, **kwargs):

        fig = shap_summary_plot(result, title=title, **kwargs)
        fig.show(config=plot_config)

from typing import Dict, List, Tuple

import numpy as np
import shap

from ...analyzer import Analyzer
from ...datasets import Dataset
from ...model import Model
from ...plots import interaction_summary_plot, shap_interaction_matrix_plot
from ...types import DataType, ResultType
from .. import InteractionTool


class SHAPInteraction(InteractionTool):
    """Tool for determining Shapley Additive Explanations (SHAP) for feature pairs.
    Using implementation from [shap](https://github.com/slundberg/shap).

    Note: This tool only computes the 2nd order interaction effects, disregarding the individual 1st order effects.
    Currently only supports tree models, as mentioned [here](https://github.com/slundberg/shap#shap-interaction-values).
    """

    def __init__(self, name='SHAP Interaction', **kwargs):
        """Initializes SHAP Interaction tool.

        Keyworded arguments are passed to the `Explainer` object from shap.

        Args:
            name (str, optional): Name of the tool. Defaults to 'SHAP Interaction'.
        """

        super().__init__(name, result_type=ResultType.DIM1)

        # set attributes
        self.kwargs = kwargs

    def run(self, model: Model, dataset: Dataset, features: List[Tuple[str, str]], analyzer: Analyzer = None) -> Dict[Tuple[str, str], float]:

        # get data
        x_train, _ = dataset.get_train()
        x_test, _ = dataset.get_test()
        x_features = x_train.columns.to_list()

        # compute scores
        explainer = shap.TreeExplainer(model.model, masker=x_train, **self.kwargs)
        shap_interaction_values = explainer.shap_interaction_values(x_test)
        if dataset.data_type == DataType.CLASSIFICATION:
            if len(shap_interaction_values.shape) == 4:
                shap_interaction_values = shap_interaction_values[1]  # TODO add support for more than two classes
        res = np.mean(np.abs(shap_interaction_values), axis=0)

        result = {
            f: res[x_features.index(f[0]), x_features.index(f[1])] for f in features
        }
        result['misc'] = {
            'shap_interaction_values': {
                f: shap_interaction_values[:, x_features.index(f[0]), x_features.index(f[1])] for f in features
            },
            'values': {
                f: x_test[f].to_numpy() for f in x_features
            }
        }

        return result

    def plot(self, result: Dict, title: str = 'Shapley Additive Explanations (SHAP)', plot_config: Dict = None, **kwargs):

        fig = interaction_summary_plot(result, title=title, **kwargs)
        fig.show(config=plot_config)

        fig = shap_interaction_matrix_plot(result, title=title, **kwargs)
        fig.show(config=plot_config)

from typing import Dict, List

from sklearn.inspection import permutation_importance

from ...analyzer import Analyzer
from ...datasets import Dataset
from ...model import Model
from ...plots import pfi_bar_plot
from ...types import ResultType
from .. import ImportanceTool


class PFIImportance(ImportanceTool):
    """Tool for determining the Permutation Feature Importance (PFI) for single features.
    Using implementation from [scikit-learn](https://scikit-learn.org/stable/modules/generated/sklearn.inspection.permutation_importance.html).
    """

    def __init__(self, name: str = 'PFI Importance', **kwargs):
        """Initializes PFI Importance tool.

        Keyworded arguments are passed to the `permutation_importance` function from sklearn.

        Args:
            name (str, optional): Name of the tool. Defaults to 'PFI Importance'.
        """

        super().__init__(name, result_type=ResultType.DIM1)

        # set attributes
        self.kwargs = kwargs

    def run(self, model: Model, dataset: Dataset, features: List[str], analyzer: Analyzer = None) -> Dict[str, float]:

        # get training data
        x_train, y_train = dataset.get_train()
        x_features = x_train.columns.to_list()

        # compute scores on training data
        res_train = permutation_importance(model.model, x_train, y_train, **self.kwargs)

        result = {
            f: res_train['importances_mean'][x_features.index(f)] for f in features
        }

        result['misc'] = {}

        # get testing data
        x_test, y_test = dataset.get_test()

        # compute scores on testing data
        res_test = permutation_importance(model.model, x_test, y_test, **self.kwargs)

        result['misc'] = {
            'test_importances': {
                f: res_test['importances_mean'][x_features.index(f)] for f in features
            },
            'train_stds': {
                f: res_train['importances_std'][x_features.index(f)] for f in features
            },
            'test_stds': {
                f: res_test['importances_std'][x_features.index(f)] for f in features
            }
        }

        return result

    def plot(self, result: Dict, title: str = 'Permutation Feature Importance (PFI)', plot_config: Dict = None, **kwargs):

        fig = pfi_bar_plot(result, title=title, **kwargs)
        # fig.show(config=plot_config)
        return fig
from typing import Dict, List, Tuple, Union

import networkx as nx
import pandas as pd
from cdt.causality.graph import GS

from ...analyzer import Analyzer
from ...datasets import Dataset
from ...model import Model
from ...plots import causality_graph_plot
from ...types import ResultType
from .. import CausalityTool


class GSCausality(CausalityTool):
    """Tool for determining Grow-Shrink (GS) algorithm results, meaning an estimated Bayesian network structure, between single features.
    Using implementation from [Causal Discovery Toolbox](https://fentechsolutions.github.io/CausalDiscoveryToolbox/html/causality.html#gs).
    """

    def __init__(self, name: str = 'GS Causality', whitelist: List[Tuple[str, str]] = None, blacklist: List[Tuple[str, str]] = None):
        """Initializes GS Causality tool.

        Args:
            name (str, optional): Name of the tool. Defaults to 'GS Causality'.
            whitelist(List[Tuple[str, str]], optional): Defaults to None.
            blacklist(List[Tuple[str, str]], optional): Defaults to None.
        """

        super().__init__(name, result_type=ResultType.GRAPH)

        # set attributes
        self.whitelist = whitelist
        self.blacklist = blacklist

    def run(self, model: Model, dataset: Dataset, features: List[str], analyzer: Analyzer = None) -> Dict[str, List[Union[str, Tuple[str, str]]]]:

        result = {
            'nodes': features.copy(),
            'edges': []
        }

        # get data
        x_train, y_train = dataset.get_train()

        xy_train = x_train[features].merge(pd.Series(y_train, name=str(dataset.target)),
                                           left_index=True, right_index=True)

        # estimate bayesian network
        est = _CustomGS()
        graph = est.create_graph_from_data_with_wlbl(xy_train, self.whitelist, self.blacklist)

        result['nodes'].append(dataset.target)
        result['edges'] = list(graph.edges)

        return result

    def plot(self, result: Dict, title: str = 'Grow-Shrink (GS)', plot_config: Dict = None, **kwargs):

        fig = causality_graph_plot(result, title=title, **kwargs)
        fig.show(config=plot_config)


class _CustomGS(GS):
    """Extends original GS implementation with a method to create a graph from data while taking a whitelist and blacklist into account.
    """

    def __init__(self):
        super().__init__()

    def create_graph_from_data_with_wlbl(self, data: pd.DataFrame, whitelist: List[Tuple[str, str]], blacklist: List[Tuple[str, str]]):
        """Run the algorithm on data while respecting given whitelist and blacklist.

        Args:
            data (pd.DataFrame): DataFrame containing the data.
            whitelist (List[Tuple[str, str]]): List of edges to be included in the graph.
            blacklist (List[Tuple[str, str]]): List of edges not to be included in the graph.
        """

        # build setup with arguments
        self.arguments['{SCORE}'] = self.score
        self.arguments['{VERBOSE}'] = str(self.verbose).upper()
        self.arguments['{BETA}'] = str(self.beta)
        self.arguments['{OPTIM}'] = str(self.optim).upper()
        self.arguments['{ALPHA}'] = str(self.alpha)

        # prepare data
        cols = list(data.columns)
        data2 = data.copy()
        data2.columns = [i for i in range(data.shape[1])]

        # set up whitelist and blacklist (first entry is ignored, for whatever reason)
        if whitelist is not None:
            whitelist = pd.DataFrame([(None, None)] + whitelist, columns=['from', 'to'])
            whitelist = whitelist.applymap(lambda node: 'X' + str(cols.index(node)) if node else None)
        if blacklist is not None:
            blacklist = pd.DataFrame([(None, None)] + blacklist, columns=['from', 'to'])
            blacklist = blacklist.applymap(lambda node: 'X' + str(cols.index(node)) if node else None)

        # construct graph
        results = self._run_bnlearn(data2, whitelist=whitelist, blacklist=blacklist, verbose=self.verbose)
        graph = nx.DiGraph()
        graph.add_nodes_from(['X' + str(i) for i in range(data.shape[1])])
        graph.add_edges_from(results)

        return nx.relabel_nodes(graph, {i: j for i, j in zip(['X' + str(i) for i in range(data.shape[1])], cols)})

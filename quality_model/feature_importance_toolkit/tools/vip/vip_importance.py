from typing import Dict, List, Tuple

from ...analyzer import Analyzer
from ...datasets import Dataset
from ...model import Model
from ...plots import importance_bar_plot
from ...tools import ImportanceTool
from ...types import ResultType
from ..pdp import PDPImportance
from ..utils import compute_tool
from .vip_utils import compute_vip


class VIPImportance(ImportanceTool):
    """Tool for determining Variable Importance Plot (VIP) results for single features.
    Implementation follows the original proposal of [Greenwell et al.](https://arxiv.org/pdf/1805.04755.pdf).
    """

    def __init__(self, name: str = 'VIP Importance', grid_size: int = 50, percentiles: Tuple[float, float] = (0.0, 1.0)):
        """Initializes VIP Importance tool.

        Args:
            name (str, optional): Name of the tool. Defaults to 'VIP Importance'.
            grid_size (int, optional): Maximum number of grid points for the PDP analysis. Defaults to 50.
            percentiles (Tuple[float, float], optional): Lower and upper percentiles for the PDP-grid's extreme values. Defaults to (0.0, 1.0).
        """

        super().__init__(name, result_type=ResultType.DIM1)

        # set attributes
        self.grid_size = grid_size
        self.percentiles = percentiles

    def run(self, model: Model, dataset: Dataset, features: List[str], analyzer: Analyzer = None) -> Dict[str, float]:

        result = {}

        pdp_result = compute_tool(PDPImportance, model, dataset, features, analyzer,
                                  grid_size=self.grid_size, percentiles=self.percentiles)

        # compute scores
        for feature in features:
            # TODO add support for categorical features
            score = compute_vip(pdp_result[feature]['scores'])
            result[feature] = score

        return result

    def plot(self, result: Dict, title: str = 'Variable Importance Plot (VIP)', plot_config: Dict = None, **kwargs):

        fig = importance_bar_plot(result, title=title, **kwargs)
        fig.show(config=plot_config)

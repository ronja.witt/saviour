from typing import Dict, List

import numpy as np
from scipy.stats import bootstrap, wilcoxon
from sklearn.base import clone

from ...analyzer import Analyzer
from ...datasets import Dataset
from ...model import Model
from ...plots import loco_bar_plot
from ...types import DataType, ResultType
from .. import ImportanceTool


class LOCOImportance(ImportanceTool):
    """Tool for determining Leave-one-Covariate-out (LOCO) results for single features.
    Implementation follows the original proposal of [Lei et al.](https://arxiv.org/pdf/1604.04173.pdf).
    Additional information is presented by [Tibshirani](http://www.stat.cmu.edu/~ryantibs/talks/loco-2018.pdf).
    """

    def __init__(self, name: str = 'LOCO Importance', confidence: float = 0.95, n_resamples: int = 9999, **kwargs):
        """Initializes LOCO Importance tool.

        Keyworded arguments are passed to the `wilcoxon` function from scipy.

        Args:
            name (str, optional): Name of the tool. Defaults to 'LOCO Importance'.
            confidence (float, optional): Confidence level for confidence intervals. Defaults to 0.95.
            n_resamples (int, optional): Number of resamples for bootstrapping of CIs. Defaults to 9999.
        """

        super().__init__(name, result_type=ResultType.DIM1)

        # set attributes
        self.confidence = confidence
        self.n_resamples = n_resamples
        self.kwargs = kwargs

    def run(self, model: Model, dataset: Dataset, features: List[str], analyzer: Analyzer = None) -> Dict[str, float]:

        result = {}
        result['misc'] = {}
        result['misc']['ci'] = {}
        result['misc']['confidence'] = self.confidence

        # compute scores
        for feature in features:
            res = self._compute_loco(model, dataset, feature)

            result[feature] = res['score']
            result['misc']['ci'][feature] = res['ci']

        return result

    def _compute_loco(self, model: Model, dataset: Dataset, feature: str) -> Dict:
        """Computes the LOCO score for a particular feature with the given model and dataset, including a confidence interval.

        Args:
            model (Model): Model to evaluate and retrain.
            dataset (Dataset): Dataset to use for evaluation and retraining.
            feature (str): Feature to compute the LOCO score for.

        Returns:
            Dict: Dictionary with LOCO score and confidence interval.
        """

        # get data
        x_train_orig, y_train = dataset.get_train()
        x_test_orig, y_test = dataset.get_test()

        # predict with original model
        y_pred_orig = model.predict(x_test_orig)

        # drop selected features from data
        x_train_new = x_train_orig.copy().drop(columns=feature)
        x_test_new = x_test_orig.copy().drop(columns=feature)

        # clone and retrain model on new data
        new_model = clone(model.model)
        new_model.fit(x_train_new, y_train)

        # predict with new model
        y_pred_new = new_model.predict(x_test_new)

        # determine change in errors
        if dataset.data_type == DataType.CLASSIFICATION:
            # mean binary error (0 or 1) for every sample
            errors_orig = (y_test != y_pred_orig).astype(int)
            errors_new = (y_test != y_pred_new).astype(int)
            statistic = np.mean
        elif dataset.data_type == DataType.REGRESSION:
            # median absolute error for every sample
            errors_orig = np.abs(y_test - y_pred_orig)
            errors_new = np.abs(y_test - y_pred_new)
            statistic = np.median
        excess_errors = errors_new - errors_orig

        # check statistical significance with Wilcoxon
        try:
            p_value = wilcoxon(excess_errors, **self.kwargs).pvalue
        except:
            p_value = 1.0

        if p_value < (1 - self.confidence):
            # median significantly different from zero
            score = statistic(excess_errors)
            ci = tuple(bootstrap((excess_errors,), statistic, method='basic', confidence_level=self.confidence,
                                 n_resamples=self.n_resamples).confidence_interval)
        else:
            score = 0
            ci = (0, 0)

        return {
            'score': score,
            'ci': ci
        }

    def plot(self, result: Dict, title: str = 'Leave-one-Covariate-out (LOCO)', plot_config: Dict = None, **kwargs):

        fig = loco_bar_plot(result, title=title, **kwargs)
        fig.show(config=plot_config)

from typing import Dict, List

from PyALE import ale

from ...analyzer import Analyzer
from ...datasets import Dataset
from ...model import Model
from ...plots import ale_grid_plot
from ...types import ResultType
from .. import ImportanceTool


class ALEImportance(ImportanceTool):
    """Tool for determining Accumulated Local Effects (ALE) for single features.
    Using implementation from [PyALE](https://github.com/DanaJomar/PyALE).
    """

    def __init__(self, name: str = 'ALE Importance', grid_size: int = 50, confidence: float = 0.95):
        """Initializes ALE Importance tool.

        Args:
            name (str, optional): Name of the tool. Defaults to 'ALE Importance'.
            grid_size (int, optional): Maximum number of grid points for the ALE analysis. Defaults to 50.
            confidence (float, optional): Confidence level for confidence intervals. Defaults to 0.95.
        """

        super().__init__(name, result_type=ResultType.DIM2)

        # set attributes
        self.grid_size = grid_size
        self.confidence = confidence

    def run(self, model: Model, dataset: Dataset, features: List[str], analyzer: Analyzer = None) -> Dict[str, Dict[str, List]]:

        result = {}

        # get data
        x_train, _ = dataset.get_train()

        for feature in features:
            # TODO add support for discrete features
            # compute scores
            res = ale(x_train, model.model, [feature], grid_size=self.grid_size, feature_type='continuous', plot=False)

            result[feature] = {
                feature: res.index.to_numpy(),  # grid points
                'scores': res['eff'].to_numpy(),
                'misc': {
                    'lower_ci': res['lowerCI_'+str(int(100*self.confidence))+'%'].to_numpy(),
                    'upper_ci': res['upperCI_'+str(int(100*self.confidence))+'%'].to_numpy(),
                    'confidence': self.confidence,
                    'samples': x_train[feature].to_numpy()
                }
            }

        return result

    def plot(self, result: Dict, title: str = 'Accumulated Local Effects (ALE)', plot_config: Dict = None, **kwargs):

        fig = ale_grid_plot(result, title=title, **kwargs)
        fig.show(config=plot_config)

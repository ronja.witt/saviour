from typing import Dict, List, Tuple

from sklearn.inspection import partial_dependence

from ...analyzer import Analyzer
from ...datasets import Dataset
from ...model import Model
from ...plots import pdp_grid_plot
from ...types import ResultType
from .. import ImportanceTool


class PDPImportance(ImportanceTool):
    """Tool for determining Partial Dependence Plot (PDP) results for single features.
    Using implementation from [scikit-learn](https://scikit-learn.org/stable/modules/generated/sklearn.inspection.partial_dependence.html).
    """

    def __init__(self, name: str = 'PDP Importance', grid_size: int = 50, percentiles: Tuple[float, float] = (0.0, 1.0), **kwargs):
        """Initializes PDP Importance tool.

        Keyworded arguments are passed to the `partial_depencence` function from sklearn.

        Args:
            name (str, optional): Name of the tool. Defaults to 'PDP Importance'.
            grid_size (int, optional): Maximum number of grid points for the PDP analysis. Defaults to 50.
            percentiles (Tuple[float, float], optional): Lower and upper percentiles for the grid's extreme values. Defaults to (0.0, 1.0).
        """

        super().__init__(name, result_type=ResultType.DIM2)

        # set attributes
        self.grid_size = grid_size
        self.percentiles = percentiles
        self.kwargs = kwargs

    def run(self, model: Model, dataset: Dataset, features: List[str], analyzer: Analyzer = None) -> Dict[str, Dict[str, List]]:

        result = {}

        # get data
        x_train, _ = dataset.get_train()

        for feature in features:
            # compute scores
            res = partial_dependence(model.model, x_train, [feature], grid_resolution=self.grid_size,
                                     percentiles=self.percentiles, kind='both', **self.kwargs)

            result[feature] = {
                feature: res['values'][0],  # grid points
                'scores': res['average'][0],
                'misc': {
                    'ice': res['individual'][0],
                    'samples': x_train[feature]
                }
            }

        return result

    def plot(self, result: Dict, title: str = 'Partial Dependence Plots (PDP)', plot_config: Dict = None, **kwargs):

        fig = pdp_grid_plot(result, title=title, **kwargs)
        # fig.show(config=plot_config)
        return fig

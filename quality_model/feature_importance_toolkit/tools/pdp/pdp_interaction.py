from functools import partial
from multiprocessing import Pool, cpu_count
from typing import Dict, List, Tuple

import numpy as np
import pandas as pd
from sklearn.inspection import partial_dependence

from ...analyzer import Analyzer
from ...datasets import Dataset
from ...model import Model
from ...plots import interaction_heatmap_matrix_plot
from ...types import ResultType
from .. import InteractionTool
from ..utils import center_1D, center_2D, compute_tool, features_from_pairs
from . import PDPImportance


class PDPInteraction(InteractionTool):
    """Tool for determining Partial Dependence Plot (PDP) results for feature pairs.
    Using implementation from [scikit-learn](https://scikit-learn.org/stable/modules/generated/sklearn.inspection.partial_dependence.html).

    Note: If `second_order_only` is True, this tool only computes the 2nd order interaction effects and 1st order effects are removed. PDPs are usually not doing this but for interaction analysis, including 1st order effects may make interactions less obvious and more difficult to interpret. The 2nd order effects are also centered around zero, as opposed to the predictions usually obtained with PDPs. The original PDP behaviour is kept by default by setting `second_order_only` to False.
    """

    def __init__(self, name: str = 'PDP Interaction', grid_size: int = 50, percentiles: Tuple[float, float] = (0.0, 1.0), second_order_only: bool = False, **kwargs):
        """Initialize PDP Interaction tool.

        Keyworded arguments are passed to the `partial_depencence` function from sklearn.

        Args:
            name (str, optional): Name of the tool. Defaults to 'PDP Interaction'.
            grid_size (int, optional): Maximum number of grid points for the PDP analysis. Defaults to 50.
            percentiles (Tuple[float, float], optional): Lower and upper percentiles for the grid's extreme values. Defaults to (0.0, 1.0).
            second_order_only (bool, optional): Whether to compute 2nd order effects only and remove 1st oder effects. Defaults to False.
        """

        super().__init__(name, result_type=ResultType.DIM3)

        # set attributes
        self.grid_size = grid_size
        self.percentiles = percentiles
        self.second_order_only = second_order_only
        self.kwargs = kwargs

    def run(self, model: Model, dataset: Dataset, features: List[Tuple[str, str]], analyzer: Analyzer = None) -> Dict[Tuple[str, str], Dict[str, List]]:

        result = {}

        # get data
        x_train, _ = dataset.get_train()

        if self.second_order_only:  # remove 1st order effects
            single_features = features_from_pairs(features)
            first_order_pdp = compute_tool(PDPImportance, model, dataset, single_features, analyzer,
                                           grid_size=self.grid_size, percentiles=self.percentiles)
            first_order_pdp_effects = {}
            for feature, value in first_order_pdp.items():
                first_order_pdp_effects[feature] = center_1D(value['scores'], value[feature])

            feat_pair_pdp = compute_tool(PDPInteraction, model, dataset, features, analyzer,
                                         grid_size=self.grid_size, percentiles=self.percentiles,
                                         second_order_only=False)

            for feature_pair in features:

                feat_a, feat_b = feature_pair
                feat_a_effects = first_order_pdp_effects[feat_a]
                feat_b_effects = first_order_pdp_effects[feat_b]

                feat_pair_scores = feat_pair_pdp[feature_pair]['scores']
                feat_pair_grid = [feat_pair_pdp[feature_pair][feature_pair[0]],
                                  feat_pair_pdp[feature_pair][feature_pair[1]]]
                feat_pair_effects = center_2D(feat_pair_scores, feat_pair_grid)

                res = feat_pair_effects - feat_a_effects.reshape(-1, 1) - feat_b_effects.reshape(1, -1)

                self.store_result(result, res, feature_pair, feat_pair_grid, x_train)

        else:  # keep 1st order effects
            # parallelized partial dependence computation
            with Pool() as pool:
                mapfunc = partial(partial_dependence, model.model, x_train,
                                  grid_resolution=self.grid_size, percentiles=self.percentiles,
                                  kind='average', **self.kwargs)
                feat_pair_pdps = pool.map(mapfunc, [list(f) for f in features])

            for i, feature_pair in enumerate(features):

                feat_pair_pdp = feat_pair_pdps[i]
                feat_pair_scores = feat_pair_pdp['average'][0]
                feat_pair_grid = feat_pair_pdp['values']

                res = feat_pair_scores

                self.store_result(result, res, feature_pair, feat_pair_grid, x_train)

        return result

    def store_result(self, result: Dict, pdp_scores: np.array, feature_pair: Tuple[str, str], grid: np.array, samples: pd.DataFrame):
        """Stores individual PDP results in the common result dictionary of this tool.

        Args:
            result (Dict): The common result dictionary.
            pdp_scores (np.array): PDP result scores of a feature pair to be stored.
            feature_pair (Tuple[str, str]): Feature pair to store results for.
            grid (np.array): PDP grid values for both features.
            samples (pd.DataFrame): Data frame with all training samples.
        """

        result[feature_pair] = {
            feature_pair[0]: grid[0],  # grid points of feature 0
            feature_pair[1]: grid[1],  # grid points of feature 1
            'scores': pdp_scores,
            'misc': {
                'samples': samples[list(feature_pair)].to_numpy()
            }
        }

    def plot(self, result: Dict, title: str = 'Partial Dependence Plots (PDP)', plot_config: Dict = None, **kwargs):

        cbar_title = 'interaction effect' if self.second_order_only else 'prediction'
        fig = interaction_heatmap_matrix_plot(result, title=title, colorbar_title=cbar_title, **kwargs)
        fig.show(config=plot_config)

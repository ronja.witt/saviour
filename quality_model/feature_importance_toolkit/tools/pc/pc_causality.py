from typing import Dict, List, Tuple, Union

import pandas as pd
from pgmpy.estimators import PC

from ...analyzer import Analyzer
from ...datasets import Dataset
from ...model import Model
from ...plots import causality_graph_plot
from ...types import ResultType
from .. import CausalityTool


class PCCausality(CausalityTool):
    """Tool for determining constraint-based PC algorithm results, meaning an estimated Bayesian network structure, between single features.
    Using implementation from [pgmpy](https://pgmpy.org/structure_estimator/pc.html).
    """

    def __init__(self, name: str = 'PC Causality', ci_test: str = 'pearsonr', alpha_level: float = 0.01, **kwargs):
        """Initializes PC Causality tool.

        Keyworded arguments are passed to the `PC.estimate` function from pgmpy.

        Args:
            name (str, optional): Name of the tool. Defaults to 'PC Causality'.
            ci_test (str, optional): Statistical test to use for testing conditional independence in the dataset. Either 'independence_match', 'chi_square' or 'pearsonr'. The latter is required for continuous variables. Defaults to 'pearsonr'.
            alpha_level (float, optional): Statistical significante level for the underlying p-value test. Defaults to 0.01.
        """

        super().__init__(name, result_type=ResultType.GRAPH)

        # set attributes
        self.ci_test = ci_test
        self.alpha_level = alpha_level
        self.kwargs = kwargs

    def run(self, model: Model, dataset: Dataset, features: List[str], analyzer: Analyzer = None) -> Dict[str, List[Union[str, Tuple[str, str]]]]:

        result = {
            'nodes': features.copy(),
            'edges': []
        }

        # get data
        x_train, y_train = dataset.get_train()

        xy_train = x_train[features].merge(pd.Series(y_train, name=str(dataset.target)),
                                           left_index=True, right_index=True)

        # estimate bayesian network
        est = PC(data=xy_train)
        graph = est.estimate(ci_test=self.ci_test, significance_level=self.alpha_level,
                             show_progress=False, **self.kwargs)

        result['nodes'].append(dataset.target)
        result['edges'] = list(graph.edges)

        return result

    def plot(self, result: Dict, title: str = 'PC', plot_config: Dict = None, **kwargs):

        fig = causality_graph_plot(result, title=title, **kwargs)
        fig.show(config=plot_config)

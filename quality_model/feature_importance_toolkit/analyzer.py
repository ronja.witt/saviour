import warnings
from copy import deepcopy
from typing import Any, Callable, Dict, List, Tuple, Type, Union

import pandas as pd
from sklearn.metrics import r2_score

from . import Analyzer  # forward declaration to be overwritten
from . import Model
from .datasets import Dataset, PreloadedDataset
from .plots import target_distributions_plot
from .tools import CausalityTool, ImportanceTool, InteractionTool, PDPImportance, Tool
from .tools.utils import compute_tool, interpolate_1D, interpolate_2D
from .types import DataType, ResultType, ToolType


class Analyzer():
    """Analyzer handling a model and a dataset and managing all tools to use for analysis.
    Provides functions to run analyses with all these tools and stores the computed results.

    Attributes:
        tool_type (ToolType): Type of tools used by this analyzer.
        model (Model): Model to run the analysis with.
        dataset (Dataset): Dataset to run the analysis for.
        tools: Dict[str, Tool]: Dictionary storing all tools for analysis.
        results: Dict[str, Dict]: Dictionary storing all results computed with the analyzer's tools.
        reuse_analyzers (List[Analyzer]): List of analyzers from whose tools results can be reused.
    """

    def __init__(self, tool_type: ToolType, model: Model, dataset: Dataset, tools: Union[List[Tool], Tool] = None, reuse_analyzers: Union[List[Analyzer], Analyzer] = None):
        """Initializes analyzer for given model and dataset with given tools.
        Sets up dictionaries for managing tools and results.

        Args:
            tool_type (ToolType): Type of tools to use for the analysis (feature importance, interaction or causality tools).
            model (Model): Model to run the analysis with.
            dataset (Dataset): Dataset to run the analysis for.
            tools (Union[List[Tool], Tool], optional): List of tools to execute the analysis with. Defaults to None.
            reuse_analyzers (Union[List[Analyzer], Analyzer], optional): List of analyzers from whose tools results can be reused. Defaults to None
        """

        # set attributes
        self.tool_type = tool_type
        self.model = model
        self.dataset = dataset

        # raise error for more than two classes # TODO add support for more than two classes
        if dataset.data_type == DataType.CLASSIFICATION:
            if dataset.n_classes > 2:
                raise ValueError('More than two classes are currently not supported for classification datasets.')
            warnings.warn('Analysis of classification data and models is only partially supported. Use with caution!')

        # initialize tool and result dictionaries
        self.tools = {}
        self.results = {}
        self._temp_results = {}

        if tools is not None:
            self.add_tools(tools)

        # make given analyzers' and own tools' results reusable
        self.reuse_analyzers = [self]
        if reuse_analyzers is not None:
            self.add_reuse_analyzers(reuse_analyzers)

        # default run settings
        self._run_reuse_tools = True
        self._run_force = False

    def add_tools(self, tools: Union[List[Tool], Tool], force: bool = False):
        """Adds the specified tools to the analyzer's list of tools.
        These tools are additionally run when executing the analysis.
        Preexisting tools are not overwritten, except when the `force` keyword is set to `True`.

        Args:
            tools (Union[List[Tool], Tool]): List of tools to add for the analysis.
            force (bool, optional): Option to overwrite existing tools if True. Defaults to False.
        """

        # check tools before adding
        tools = self._check_tools(tools, expected_type=Tool)

        # add tools to tools dictionary
        for tool in tools:
            if tool.tool_type == self.tool_type:
                if tool.name not in self.tools.keys() or force:
                    self.tools[tool.name] = tool
                    self.results[tool.name] = {}
                    self._temp_results[tool.name] = {}
                else:
                    warnings.warn('Tool {name} already exists in analyzer. Skipping.'
                                  .format(name=tool.name), RuntimeWarning)
            else:
                warnings.warn('Tool {name} has wrong tool type {tool_type}. Expected tool type {exp_type}. Skipping.'
                              .format(name=tool.name, tool_type=tool.tool_type.value, exp_type=self.tool_type.value),
                              RuntimeWarning)

    def remove_tools(self, tools: Union[List[str], str]):
        """Removes the specified tools from the analyzer's list of tools.
        Also removes any associated results already computed and stored by the analyzer.

        Args:
            tools (Union[List[str], str]): List of tools to remove from the analyzer, specified by their name.
        """

        # check tools before removing
        tools = self._check_tools(tools, expected_type=str)

        # remove tools from dictionaries
        for tool in tools:
            if tool in self.tools.keys():
                self.tools.pop(tool)
                self.results.pop(tool)
                self._temp_results.pop(tool)

    def add_reuse_analyzers(self, analyzers: Union[List[Analyzer], Analyzer]):
        """Adds the specified analyzers to the list of analyzers to reuse results from.

        Args:
            analyzers (Union[List[Analyzer], Analyzer]): List of analyzers to add for reuse.
        """

        if not isinstance(analyzers, list):
            analyzers = [analyzers]

        # add analyzers to reuse analyzer list
        for analyzer in analyzers:
            if isinstance(analyzer, Analyzer):
                if analyzer.dataset == self.dataset:
                    self.reuse_analyzers.append(analyzer)
                    if analyzer.model != self.model:
                        warnings.warn('Passed reuse analyzer uses different model {model} instead of {exp_model}.'
                                      .format(model=type(analyzer.model.model), exp_model=type(self.model.model)),
                                      RuntimeWarning)
                else:
                    warnings.warn('Passed reuse analyzer uses different dataset {data}. Expected {exp_data}. Skipping.'
                                  .format(data=analyzer.dataset.name, exp_data=self.dataset.name), RuntimeWarning)
            else:
                warnings.warn('Passed reuse analyzer has wrong type {obj_type}. Expected type {ana_type}. Skipping.'
                              .format(obj_type=type(analyzer), ana_type=Analyzer), RuntimeWarning)

    def fit_model(self, force: bool = False, *args, **kwargs):
        """Fits the underlying model to training data if it is not already fitted.
        Also evaluates the model.

        Positional and keyworded arguments are passed to the model's fit function.

        Args:
            force (bool, optional): Option to force the fitting of the model if True, even if it is already fitted. Defaults to False.
        """

        # check if model is fitted, otherwise fit
        if not self.model.is_fitted() or force:
            x_train, y_train = self.dataset.get_train()
            self.model.fit(x_train, y_train, *args, **kwargs)

            if 'model' in self.results.keys():
                self.results.pop('model')

        self.evaluate_model(force)

    def evaluate_model(self, force: bool = False) -> Dict:
        """Evaluates the underlying model if it has not been evaluated already and stores the results.

        Args:
            force (bool, optional): Option to force a reevaluation of the model. Defaults to False.

        Returns:
            Dict: Results of the model evaluation for training and testing data.
        """

        # check if model has been evaluated, otherwise evaluate
        if 'model' not in self.results.keys() or force:
            # store results
            self.results['model'] = self.model.evaluate(self.dataset)

        return self.results['model']

    def run_tool(self, tool: Union[Tool, str], features: Union[List[str], List[Tuple[str, str]]], fit: bool = True, reuse_tools: bool = True, force: bool = False, temp: bool = False) -> Dict:
        """Runs an analysis with the specified tool for the given features and stores the computed results.

        Args:
            tool (Union[Tool, str]): Tool to run the analysis with.
            features (Union[List[str], List[Tuple[str, str]]]): List of features to compute the analysis results for.
            fit (bool, optional): Whether to fit the model before running the analysis if it is not already fitted. Defaults to True.
            reuse_tools (bool, optional): Whether to reuse existing tool's results for computations of this tool. Kept at previous value if `temp` is True.
            force (bool, optional): Option to ignore existing results and force the analysis to run again and also fit the model again if True. Model fitting is excluded if `fit` is False. Kept at previous value if `temp` is True. Defaults to False.
            temp (bool, optional): Whether the tool is run for temporary intermediate results for other tools. If True, results are saved in `self._temp_results` instead of `self.results`. Defaults to False.

        Raises:
            KeyError: Raised if the specified tool is not in the analyzer's list of tools.
            TypeError: Raised if the specified tool is not of type str or Tool.

        Returns:
            Dict: All computed analysis results for the specified tool.
        """

        # check passed tool
        if not isinstance(tool, Tool):
            if isinstance(tool, str):
                # find tool by name
                if tool in self.tools.keys():
                    tool = self.tools[tool]
                else:
                    raise KeyError('Tool {tool} could not be found in tools.'
                                   .format(tool=tool))
            else:
                raise TypeError('Passed tool must be of type {type_1} or {type_2}. Got {object_type} instead.'
                                .format(type_1=str, type_2=Tool, object_type=type(tool)))

        # set flags to force new computation and to reuse tools in this run if desired
        if not temp:
            self._run_force = force
            self._run_reuse_tools = reuse_tools

        # choose correct results dict (temp or not)
        res_dict = self._temp_results if temp else self.results
        other_res_dict = self.results if temp else self._temp_results

        # check for missing and elsewhere existing results and filter features
        features = self._check_features(features)
        missing_features = [f for f in features if not self.result_exists(tool, f, self._run_force, temp)]
        existing_features = [f for f in missing_features if self.result_exists(tool, f, self._run_force, (not temp))]
        # copy results existing in self._temp_results to self.results or vice versa
        for f in existing_features:
            res_dict[tool.name][f] = other_res_dict[tool.name][f]
            missing_features.pop(missing_features.index(f))

        # fit model if needed
        if fit and not temp:
            self.fit_model(self._run_force)

        # run tool and check result
        result = tool.run(self.model, self.dataset, missing_features, self)
        if not temp:
            tool._check_result_format(result, missing_features)

        # empty results if needed and save new results
        if self._run_force and not temp:
            res_dict[tool.name] = {}
        res_dict[tool.name].update(result)

        return res_dict[tool.name]

    def run_analysis(self, features: Union[List[str], List[Tuple[str, str]]], fit: bool = True, reuse_tools: bool = True, raise_tool_errors: bool = False, force: bool = False) -> Dict[str, Dict]:
        """Executes the analysis for all specified features with every tool added to the analyzer.

        Args:
            features (Union[List[str], List[Tuple[str, str]]]): List of features to compute the analysis results for.
            fit (bool, optional): Whether to fit the model before running the analysis if it is not already fitted. Defaults to True.
            reuse_tools (bool, optional): Whether to reuse existing tool's results for computations of other tools.
            raise_tool_errors (bool, optional): Option to raise errors that occur when running tools if True. Else, only a warning will be issued. Defaults to False.
            force (bool, optional): Option to ignore existing results and force the analysis to run again and also fit the model again for every tool if True. Model fitting is excluded if `fit` is False. Defaults to False.

        Raises:
            TypeError: Raised if the specified tool type is not of type ToolType.

        Returns:
            Dict[str, Dict]: All computed analysis results from all tools. Includes results for features specified in prior executions of this function as well as model results.
        """

        # run every tool
        for tool in self.tools.values():
            try:
                self.run_tool(tool, features, fit, reuse_tools, force)
            except Exception as e:
                if raise_tool_errors:
                    raise e
                else:
                    warnings.warn(('An error occured while running tool {tool}:\n' + str(e) + '\nSkipping.')
                                  .format(tool=type(tool)), RuntimeWarning)

        return self.results

    def plot_results(self, include_model: bool = True, plot_config: Dict = None):
        """Plots the model performance and results for all tools.
        Each tools `plot` method is successively called here.

        Args:
            include_model (bool, optional): Whether to include a plot with model performance metrics. Defaults to True.
            plot_config (Dict, optional): Configuration dictionary for Plotly's `show()` method ([reference](https://plotly.com/python/configuration-options/)). Defaults to None.
        """

        fig_dict = {}
        # plot model performance metrics
        if include_model:
            if 'model' in self.results.keys():
                fig_dict["model"] = self.model.plot(self.results['model'])
            else:
                print('No results for model performance available.')

        # plot tool results
        for tool in self.tools.values():
            if not self.results[tool.name] == {}:
                fig_dict[tool.name] = tool.plot(self.results[tool.name], plot_config=plot_config)
            else:
                print(f'No results for tool {tool.name} available.')
        return fig_dict

    def result_exists(self, tool: Tool, feature: Union[str, Tuple[str, str]], force: bool = False, temp: bool = False) -> bool:
        """Checks whether a result has already been computed for the given tool and feature.

        Args:
            tool (Tool): Tool to check result existence for.
            feature (Union[str, Tuple[str, str]]): Feature to check result existence for.
            force (bool, optional): Option to ignore existing results if True. Defaults to False.
            temp (bool, optional): Whether to look in `self._temp_results` instead of `self.results`.

        Returns:
            bool: Whether a result has already been computed.
        """

        if force:
            exists = False
        elif temp == False and feature in self.results[tool.name].keys():
            exists = True
        elif temp == True and feature in self._temp_results[tool.name].keys():
            exists = True
        else:
            exists = False

        return exists

    def _find_reuse_tool(self, tool_class: Type[Tool], model: Model, dataset: Dataset, **requirements) -> Tuple[Analyzer, Tool]:
        """Looks for existing tools in `self.reuse_analyzers` that fulfill requirements and can be reused for intermediate result in other tools.

        Args:
            tool_class (Type[Tool]): Uninitialized class of tool to find an instance of for reusing.
            model(Model): Model to check compatibility with.
            dataset(Dataset): Dataset to check compatibility with.
            **requirements: Keyworded arguments with attributes to be checked in a found tool.

        Returns:
            Tuple[Analyzer, Tool]: Found analyzer and its tool for reusing.
        """

        if self._run_reuse_tools and not self._run_force:
            for analyzer in self.reuse_analyzers:
                if model == analyzer.model and dataset == analyzer.dataset:
                    for tool in analyzer.tools.values():
                        if type(tool) == tool_class:
                            fine = True
                            for key, value in requirements.items():
                                fine = fine and (getattr(tool, key) == value)
                                if not fine:
                                    break
                            if fine:
                                reuse_analyzer = analyzer
                                reuse_tool = tool

                                return reuse_analyzer, reuse_tool

        return None, None

    def _check_features(self, features: Any) -> Union[List[str], List[Tuple[str, str]]]:
        """Checks the format of the passed feature of feature list with respect to the analyzer's tool type.
        Adjusts the output format if needed and possible.
        The expected format is a str or List[str] for feature importance tools and a Tuple[str, str] or List[Tuple[str, str]] for feature interaction and causality tools.

        Args:
            features (Any): Feature or feature list to check the format of.

        Raises:
            TypeError: Raised if at least one feature is not a str or tuple.
            ValueError: Raised if a passed tuple does not have the form Tuple[str, str].
            TypeError: Raised if something other than a str is used for an importance tool.
            TypeError: Raised if something other than a tuple is used for an interaction or causality tool.

        Returns:
            Union[List[str], List[Tuple[str, str]]]: List of checked and possibly adjusted features or feature tuples.
        """

        # create list if needed
        if not isinstance(features, list):
            features = [features]

        for feature in features:
            # check for string or tuple types
            if not (isinstance(feature, str) or isinstance(feature, tuple)):
                raise TypeError('Passed features must be of type {type_1} or {type_2}. Got {object_type} instead.'
                                .format(type_1=str, type_2=tuple, object_type=type(feature)))
            # if tuple, check length and types
            if isinstance(feature, tuple) and not [type(f) for f in feature] == [str, str]:
                raise ValueError('Passed feature tuples must have the form {exp_form}. Got {form} instead.'
                                 .format(exp_form=(str, str), form=tuple([type(f) for f in feature])))

            # check if feature type (string or tuple) corresponds to tool type
            if self.tool_type in [ToolType.IMPORTANCE, ToolType.CAUSALITY] and not isinstance(feature, str):
                raise TypeError('Features for {tool_name} must be of type {expected_type}. Got {object_type} instead.'
                                .format(tool_name=self.tool_type.value, expected_type=str, object_type=type(feature)))
            elif self.tool_type is ToolType.INTERACTION and not isinstance(feature, tuple):
                raise TypeError('Features for {tool_name} must be of type {expected_type}. Got {object_type} instead.'
                                .format(tool_name=self.tool_type.value, expected_type=tuple, object_type=type(feature)))

        return features

    def _check_tools(self, tools: Any, expected_type: Union[Type[Tool], Type[str]]) -> Union[List[Tool], List[str]]:
        """Checks the type of the passed tool or tool list with respect to the expected type str or Tool.
        Adjusts the output format if needed and possible.

        Args:
            tools (Any): Tool or list of tools to check the type of.
            expected_type (Union[type[Tool], type[str]]): Expected type for the specified tools (either str or Tool).

        Raises:
            TypeError: Raised if at least one tool does not have the expected type.

        Returns:
            Union[List[Tool], List[str]]: List of checked and possibly adjusted tools with the expected type.
        """

        # create list if needed
        if not isinstance(tools, list):
            tools = [tools]

        for tool in tools:
            # check for correct type of tools
            if isinstance(tool, Tool) and expected_type == str:
                tool = tool.name
            if not isinstance(tool, expected_type):
                raise TypeError('Passed tools must be of type {expected_type}. Got {object_type} instead.'
                                .format(expected_type=expected_type, object_type=type(tool)))

        return tools


class ImportanceAnalyzer(Analyzer):
    """Analyzer for feature importance analysis.
    """

    def __init__(self, model: Model, dataset: Dataset, tools: Union[List[ImportanceTool], ImportanceTool] = None, **kwargs):

        super().__init__(tool_type=ToolType.IMPORTANCE, model=model, dataset=dataset, tools=tools, **kwargs)


class InteractionAnalyzer(Analyzer):
    """Analyzer for feature interaction analysis.
    """

    def __init__(self, model: Model, dataset: Dataset, tools: Union[List[InteractionTool], InteractionTool] = None, **kwargs):

        super().__init__(tool_type=ToolType.INTERACTION, model=model, dataset=dataset, tools=tools, **kwargs)


class CausalityAnalyzer(Analyzer):
    """Analyzer for feature causality analysis.
    """

    def __init__(self, model: Model, dataset: Dataset, tools: Union[List[CausalityTool], CausalityTool] = None, **kwargs):

        super().__init__(tool_type=ToolType.CAUSALITY, model=model, dataset=dataset, tools=tools, **kwargs)

    def evaluate_graph(self, tool: Union[Tool, str], metric: Callable = r2_score, plot_distributions: bool = False, plot_config: Dict = None) -> float:
        """Evaluates the validity of inferred graphical causality models on a test set by using it for prediction and computing a metric in comparison to ground truth target values.
        An interaction of at most two variables at a time is assumed. A dictionary of interactions and the corresponding PDP interaction results are expected to be present in the `'misc'` part of the tool's result dictionary. The graph is used for prediction by first computing all interactions by evaluating PDPs. Then a new model is fitted only on the variables going into the target node. PDP importances are computed for these variables and the predicted target is calculated by sampling these and summing them up, keeping the bias from one PDP only.

        Args:
            tool (Union[Tool, str]): The causality tool to evaluate the graph result of. Expected to be associated with this analyzer.
            metric (Callable, optional): The metric to use for evaluation. Must have the form `metric(y_true, y_pred)`. Defaults to r2_score.
            plot_distributions (bool, optional): Whether to plot the probability densities of the ground truth and predicted targets' distributions for visual comparison. Defaults to False.
            plot_config (Dict, optional): Configuration dictionary for Plotly's `show()` method ([reference](https://plotly.com/python/configuration-options/)). Defaults to None.

        Raises:
            ValueError: Raised if there exists no result for the specified tool
            KeyError: Raised if the result has no `'misc'` key.
            KeyError: Raised if the result has no `'interactions'` key in `'misc'`.
            KeyError: Raised if the result has no `'pdp'` key in `'misc'`.

        Returns:
            float: The metric evaluated on ground truth and predicted target samples.
        """        

        # check for results of specified tool
        if isinstance(tool, Tool):
            tool = tool.name  # get name as string
        if tool not in self.results.keys() or not self.results[tool]:  # if no results exist for this tool
            raise ValueError('No existing result for tool {tool} could be found.'
                             .format(tool=tool))

        # check for interaction and PDP information in result
        if 'misc' in self.results[tool].keys():
            if 'interactions' not in self.results[tool]['misc'].keys():
                raise KeyError('Key "interactions" not found in misc tool results.')
            if 'pdp' not in self.results[tool]['misc'].keys():
                raise KeyError('Key "pdp" not found in misc tool results.')
        else:
            raise KeyError('No "misc" information about interactions or PDPs in tool results.')

        # get data
        x_train, y_train = self.dataset.get_train()
        x_test, y_test = self.dataset.get_test()

        # retrieve tools results
        res = self.results[tool]
        edges = res['edges']
        interactions = res['misc']['interactions']
        pdps = res['misc']['pdp']

        # compute train and test interactions from PDP results
        x_train_ext = x_train.copy()
        x_test_ext = x_test.copy()
        for name, (f_a, f_b) in interactions.items():
            # sample PDP results for given base feature samples
            pdp_scores = pdps[(f_a, f_b)]['scores']
            pdp_grid = [pdps[(f_a, f_b)][f_a], pdps[(f_a, f_b)][f_b]]

            pdp_values_train = interpolate_2D(x_train_ext[[f_a, f_b]].to_numpy(), pdp_scores, pdp_grid)
            pdp_values_test = interpolate_2D(x_test_ext[[f_a, f_b]].to_numpy(), pdp_scores, pdp_grid)

            x_train_ext = x_train_ext.merge(pd.Series(pdp_values_train, index=x_train_ext.index, name=name),
                                            left_index=True, right_index=True)
            x_test_ext = x_test_ext.merge(pd.Series(pdp_values_test, index=x_test_ext.index, name=name),
                                          left_index=True, right_index=True)

        # find final features leading into target
        target_incoming_features = []
        for n1, n2 in edges:
            if n2 == 'target':
                target_incoming_features.append(n1)

        # fit new model on final features
        final_data = PreloadedDataset(x_train_ext[target_incoming_features], y_train,
                                      x_test_ext[target_incoming_features], y_test,
                                      self.dataset.data_type, verbose=False)
        final_model = deepcopy(self.model)
        final_model.fit(final_data._x_train, final_data._y_train)

        # obtain final PDP importance
        final_pdp = compute_tool(PDPImportance, final_model, final_data, final_data.features, None)

        # compute final model targets for test set from PDPs
        x_test_summands = final_data._x_test.copy()
        for i, f in enumerate(final_data.features):
            x_test_summands[f] = interpolate_1D(x_test_summands[f].to_numpy(), final_pdp[f]['scores'], final_pdp[f][f])
            if i > 0:  # substract bias for all but one PDP
                x_test_summands[f] -= x_test_summands[f].mean()
        y_test_pred = x_test_summands.sum(axis=1)

        # plot target and prediction distributions
        if plot_distributions:
            fig = target_distributions_plot(y_test, y_test_pred)
            fig.show(config=plot_config)

        return metric(y_test, y_test_pred)

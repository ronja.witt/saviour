from typing import Any, Callable, Dict, List, Type

import xgboost as xgb
from sklearn.exceptions import NotFittedError
from sklearn.utils.validation import check_is_fitted

from .datasets import Dataset
from .plots import model_scores_plot
from .types import DataType
import pickle

def get_model_class(quality_parameter):
    # read the pickle file
    # picklefile = open('quality_model/models/best_model', 'rb')
    picklefile = open('quality_model/models/' + str(quality_parameter), 'rb')
    # unpickle the model
    pipeline = pickle.load(picklefile)
    # close file
    picklefile.close()
    return pipeline["clf"]


class Model():
    """Wrapper class for models.
    Instantiates either a model of the provided model class or a gradient boosted tree instead.

    Attributes:
        data_type (DataType): Type of the model, which is either a classifier or a regressor.
        model ([type]): The actual underlying model.
        metrics (List[Callable]): List of callable metrics to use for evaluation.
    """

    def __init__(self, quality_parameter, data_type: DataType, model_class: Type = None, metrics: List[Callable] = None, verbose: bool = True, *args, **kwargs):
        """Initializes common model wrapper.
        If no model is explicitly provided, creates a gradient boosted tree from xgboost (for classification or regression, depending on the data type).

        Positional and keyworded arguments are passed to xgboost tree initializers or model initializer.

        Args:
            data_type (DataType): Type the data to work with, depending on what kind of output the model is meant to produce (either classification or regression).
            model_class (Type, optional): Model class to initialize and use for fitting and predicting if the gradient boosted tree does not suffice. Should be a class to be instantiated, not the instance itself. Needs to be an sklearn model or otherwise provide `fit(x, y)`, `predict(x)`, `score(x, y)` and `__sklearn_is_fitted__()` methods. Needs to adhere to data type, otherwise leads to unexpected behavior. Defaults to None.
            metrics (List[Callable], optional): List of metrics to use for evaluation. Expects callable functions taking target label and predicted label array-likes `y_true` and `y_pred` as `metric(y_true, y_pred)`. Works with `sklearn.metrics`, assuming the metric works for the specified data type (regression or classification). Defaults to None.
            verbose (bool, optional): Whether status messages should be printed or not. Defaults to True.

        Raises:
            TypeError: Raised if metrics is not a list of callable functions.
            TypeError: Raised if data_type is not a DataType.
            TypeError: Raised if model_class is not the class type but an object instead.
        """

        self.data_type = data_type

        # check passed metrics
        if isinstance(metrics, list) or metrics is None:
            self.metrics = metrics
        else:
            raise TypeError('Metrics must be a list of callable functions. Got a {actual_type} instead.'
                            .format(actual_type=type(metrics)))

        if model_class is None:
            # TODO: use model with hyperparameter optimization for each variable by model_class perheps

            # initialize gradient boosted tree
            if self.data_type == DataType.CLASSIFICATION:
                self.model = xgb.XGBClassifier(*args, **kwargs, use_label_encoder=False, verbosity=0)
                # from sklearn import svm
                # self.model = svm.SVC(C=1, gamma="scale", probability=True)
            elif self.data_type == DataType.REGRESSION:
                self.model = xgb.XGBRegressor(*args, **kwargs, verbosity=0)
                from sklearn import svm
                # self.model = svm.SVR(*args, **kwargs)
                
                import pickle
                # read the pickle file
                picklefile = open('quality_model/models/best_model', 'rb')
                # unpickle the model
                pipeline = pickle.load(picklefile)
                # close file
                picklefile.close()
                print(pipeline["clf"])
                print("-----------------")
                print(svm.SVR(*args, **kwargs))
                print("-----------------")
                self.model = svm.SVR(C=0.2, gamma="scale")

            else:
                raise TypeError('The data_type has to be a {expected_class}. Got a {actual_class} instead.'
                                .format(expected_class=DataType, actual_class=self.data_type.__class__))
        else:
            # instantiate provided model class
            # TODO: set model to selected model for each quality parameter
            # if isinstance(model_class, type):
            #     self.model = model_class(*args, **kwargs)
            # else:
            #     raise TypeError('The model_class should be the uninstantiated class. Got something else instead.')
            try:
                self.model = model_class # get_model_class(quality_parameter)
                print(self.model)
                print("we have a model")
            except:
                print("no model found")

        if verbose:
            print('Created {model}.'.format(model=self.model.__class__.__name__))

    def fit(self, x, y, *args, **kwargs):
        """Calls the `fit(x, y)` method of the underlying model and returns its return.

        Args:
            x (array-like): Training data of shape (n_samples, n_features). May be a DataFrame.
            y (array-like): Target values of shape (n_samples, n_targets). May be a Series.

        Returns:
            Any: Return of model's `fit`-method. Underlying model for sklearn-based models.
        """

        return self.model.fit(x, y, *args, **kwargs)

    def predict(self, x, *args, **kwargs):
        """Calls the `predict(x)` method of the underlying model and returns the result.

        Args:
            x (array-like): Sample data of shape (n_samples, n_features). May be a DataFrame.

        Returns:
            array-like: Predicted target values of shape (n_samples, n_targets).
        """

        return self.model.predict(x, *args, **kwargs)

    def score(self, x, y, *args, **kwargs):
        """Calls the `score(x, y)` method of the underlying model and returns the result.

        Args:
            x (array-like): Sample data of shape (n_samples, n_features). May be a DataFrame.
            y (array-like): Target values of shape (n_samples, n_targets). May be a Series.

        Returns:
            float: Evaluation metric for the prediction. Usually R² score or mean accuracy.
        """

        return self.model.score(x, y, *args, **kwargs)

    def is_fitted(self):
        """Determines whether the model has already been fitted or not, but only for sklearn-based models.
        Other models need to override this method.
        """

        fitted = True
        try:
            check_is_fitted(self.model)
        except NotFittedError:
            fitted = False

        return fitted

    def evaluate(self, dataset: Dataset) -> Dict[str, Dict[str, float]]:
        """Evaluates model for every metric specified during intitialization on both training and testing data.

        Args:
            dataset (Dataset): Dataset the evaluation is carried out on.

        Returns:
            Dict[str, Dict[str, float]]: Performance metric results for every metric and data split.
        """

        x_train, y_train = dataset.get_train()
        x_test, y_test = dataset.get_test()
        result = {}

        if self.metrics is not None:
            y_train_pred = self.predict(x_train)
            y_test_pred = self.predict(x_test)

            for metric in self.metrics:
                result[metric.__name__] = {}
                result[metric.__name__]['train'] = metric(y_train, y_train_pred)
                result[metric.__name__]['test'] = metric(y_test, y_test_pred)

        else:
            score = _scores_dict[self.data_type]
            result[score] = {}
            result[score]['train'] = self.score(x_train, y_train)
            result[score]['test'] = self.score(x_test, y_test)

        return result

    def plot(self, result: Dict[str, Dict[str, float]], plot_config: Dict = None):
        """Plots result of the model evaluation. This encompasses bar plots for every metric on both training and testing data.

        Args:
            result (Dict[str, Dict[str, float]]): Dictionary with performance metric results to plot.
            plot_config (Dict, optional): Configuration dictionary for Plotly's `show()` method ([reference](https://plotly.com/python/configuration-options/)). Defaults to None.
        """

        fig = model_scores_plot(result, title='Model Performance')
        # fig.show(config=plot_config)
        return fig


class PreloadedModel(Model):
    """Wrapper class for preloaded models.
    Manages a reference to the provided model.

    Attributes:
        fitted (bool): Whether the model has already been fitted or not.
    """

    def __init__(self, data_type: DataType, model: Any, fitted: bool, metrics: List[Callable] = None, verbose: bool = True):
        """Initializes model wrapper for preloaded models.

        Args:
            data_type (DataType): Type of the data to work with, depending on what kind of output the model is meant to produce (either classification or regression).
            model (Any): The preinstantiated model object. Needs to provide `fit(x, y)`, `predict(x)` and `score(x, y)` methods. Needs to adhere to data type, otherwise leads to unexpected behavior.
            fitted (bool): Whether the provided model has already been fitted or not.
            metrics (List[Callable], optional): List of metrics to use for evaluation. Expects callable functions taking target label and predicted label array-likes `y_true` and `y_pred` as `metric(y_true, y_pred)`. Works with `sklearn.metrics`, assuming the metric works for the specified data type (regression or classification). Defaults to None.
            verbose (bool, optional): Whether status messages should be printed or not. Defaults to True.

        Raises:
            TypeError: Raised if metrics is not a list of callable functions.
        """

        # set attributes
        self.data_type = data_type
        self.model = model
        self.fitted = fitted

        # ensure sklearn compatibility
        self.model._sklearn_fitted = fitted
        self.model.__sklearn_is_fitted__ = _sklearn_is_fitted.__get__(self.model)
        self.model._estimator_type = ('regressor' if data_type == DataType.REGRESSION else 'classifier')

        # check passed metrics
        if isinstance(metrics, list) or metrics is None:
            self.metrics = metrics
        else:
            raise TypeError('Metrics must be a list of callable functions. Got a {actual_type} instead.'
                            .format(actual_type=type(metrics)))

        if verbose:
            print('Created {model}.'.format(model=self.model.__class__.__name__))

    def fit(self, x, y, *args, **kwargs):
        """Calls the `fit(x, y)` method of the underlying model sets the `fitted` attribute and returns its return.

        Args:
            x (array-like): Training data of shape (n_samples, n_features). May be a DataFrame.
            y (array-like): Target values of shape (n_samples, n_targets). May be a Series.

        Returns:
            Any: Return of model's `fit`-method. Underlying model for sklearn-based models.
        """

        result = self.model.fit(x, y, *args, **kwargs)
        self.fitted = self.model._sklearn_fitted = True

        return result

    def is_fitted(self):
        """Determines whether the model has already been fitted or not.
        """

        return self.fitted


def _sklearn_is_fitted(self):
    """Makes information about whether a model is fitted available for sklearn functions.
    """

    return self._sklearn_fitted


_scores_dict = {
    DataType.CLASSIFICATION: 'accuracy_score',
    DataType.REGRESSION: 'r2_score'
}

# import sys
# sys.path.append('<path/to/feature-importance-toolkit>')
import os, sys
sys.path.append(os.getcwd()+"")

from quality_model.benchmark_models_classifier import get_benchmark_models_clf
from quality_model.benchmark_models_regressor import get_benchmark_models_reg
from quality_model.feature_importance_toolkit import Model, ImportanceAnalyzer
from quality_model.feature_importance_toolkit.datasets.multiplication_datasets import MultiplicationTwoDataset
from quality_model.feature_importance_toolkit.tools import PFIImportance, PDPImportance
from quality_model.feature_importance_toolkit.datasets import PreloadedDataset
from quality_model.feature_importance_toolkit.types import DataType
import h5py as h5
import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
import pickle
from sklearn.datasets import make_classification, make_regression
import pickle
from sklearn import svm
import xgboost as xgb
from sklearn.feature_selection import SelectKBest, chi2, f_classif, SelectFromModel, mutual_info_classif, f_regression
from sklearn.gaussian_process import GaussianProcessClassifier
from sklearn.gaussian_process.kernels import RBF, DotProduct
from quality_model.data_augmentation import generate_new_data




def get_model_class(quality_parameter, percentile = 100):

    """
    get the best model

    Inputs:
        - quality_parameter (string): name of the quality parameter to examine
        - X_train (pd.DataFrame): Processdata
        - y_train (numpy.ndarray): Data of the quality-parameter
    Output:
        - best model (obj)
    """

    # read the pickle file
    picklefile = open('quality_model/models/' + str(quality_parameter) + str(percentile), 'rb')
    # unpickle the model
    pipeline = pickle.load(picklefile)
    # close file
    picklefile.close()

    return pipeline


global status



# TODO: add other features to dataframe

import flask_module.mySQL_functions.db_credentials as db_credentials
import mysql.connector
from sqlalchemy import create_engine, text, exc
def connect():
    """
    connect to database and return cursor for further interactions with the database
    """
    mydb = mysql.connector.connect(
        host = db_credentials.host,
        user = db_credentials.user,
        password = db_credentials.password,
        database = db_credentials.database_name
    )
    mycursor = mydb.cursor(buffered=True)
    return mycursor, mydb

# initiate cursor
mycursor, mydb = connect()

def get_features_from_database(percentile):
    """
    gets all features from database if the data is complete and has the measurement results
    """
    X_new = pd.DataFrame()
    mycursor.execute("""SELECT id FROM process_step WHERE 1""")
    ids_process_steps = mycursor.fetchall()
    for row in ids_process_steps:
        check_preadiness = []
        for i in [10, 20, 30, 40, 50, 60, 70, 80, 90, 100]:
            mycursor.execute("""SELECT percentage FROM sensor_readings WHERE process_step_id = %s AND percentage = %s""", [row[0], i])
            percentage_all = mycursor.fetchall()
            if percentage_all == []:
                check_preadiness.append(False)
            else:
                check_preadiness.append(True)
        for sensor_id in [23, 24, 25, 26]:
            mycursor.execute("""SELECT sensor_id FROM sensor_readings WHERE process_step_id = %s AND sensor_id = %s""", [row[0], sensor_id])
            percentage_all = mycursor.fetchall()
            if percentage_all == []:
                check_preadiness.append(False)
            else:
                check_preadiness.append(True)
        if False not in check_preadiness:
            mycursor.execute("""SELECT value, sensor_id FROM sensor_readings WHERE process_step_id = %s AND (percentage = %s OR percentage IS Null)""", [row[0], percentile])
            process_step_specification_desc = mycursor.fetchall()
            for entry in process_step_specification_desc:
                # print(entry[1])
                mycursor.execute("""SELECT name FROM sensor WHERE id = %s""", [entry[1]])
                name_sensor = mycursor.fetchone()[0]
                if name_sensor not in ["Tensile_strength", "Elongation_at_break", "Impact_strength", "Geometric_accuracy"]:
                    X_new.at[row[0], name_sensor] = entry[0]
    print(X_new)            
    X_new.dropna(axis=1, inplace=True)
    return(X_new)
    

def get_Xreal_and_yreal(quality_parameter, percentile):
    features_from_database = get_features_from_database(percentile)
    X_real_quality = pd.DataFrame()

    mycursor.execute("""SELECT id FROM process_step WHERE 1""")
    ids_process_steps = mycursor.fetchall()
    result_end = pd.DataFrame()
    for row in ids_process_steps:
        for name_sensor in ["Tensile_strength", "Elongation_at_break", "Impact_strength", "Geometric_accuracy"]:
            mycursor.execute("""SELECT id FROM sensor WHERE name = %s""", [name_sensor])
            id_sensor = mycursor.fetchone()[0]
            mycursor.execute("""SELECT value FROM sensor_readings WHERE process_step_id = %s AND sensor_id = %s""", [row[0], id_sensor])
            sensor_values = mycursor.fetchall()
            for key in range(0, len(sensor_values)):
                X_real_quality.at[key, name_sensor] = sensor_values[key][0]
        result = pd.merge(features_from_database.loc[[row[0]]], X_real_quality, how="cross")
        result_end = pd.concat([result_end, result], ignore_index=True)
        
    y_real_2 = result_end[quality_parameter].T.to_numpy()
    X_real_2 = result_end.drop(columns=["Tensile_strength", "Elongation_at_break", "Impact_strength", "Geometric_accuracy"])
    
     

    return(X_real_2, y_real_2)


def get_Xreal_and_yreal_tensor(quality_parameter, percentile):
    # global status
    # status = 0

    features_from_database = get_features_from_database(percentile)
    print("_______________________features_from_database_______________________")
    print(features_from_database)




    df_of_all_features_saved = features_from_database.tail(1)
    print("----------------------------df_of_all_features_saved-----------------------------------")
    print(df_of_all_features_saved)





    X_real_quality = pd.DataFrame()

    mycursor.execute("""SELECT id FROM process_step WHERE 1""")
    ids_process_steps = mycursor.fetchall()
    result_end = pd.DataFrame()
    for row in ids_process_steps:
        for name_sensor in ["Tensile_strength", "Elongation_at_break", "Impact_strength", "Geometric_accuracy"]:
            mycursor.execute("""SELECT id FROM sensor WHERE name = %s""", [name_sensor])
            id_sensor = mycursor.fetchone()[0]
            mycursor.execute("""SELECT value FROM sensor_readings WHERE process_step_id = %s AND sensor_id = %s""", [row[0], id_sensor])
            sensor_values = mycursor.fetchall()
            for key in range(0, len(sensor_values)):
                X_real_quality.at[key, name_sensor] = sensor_values[key][0]
        try:
            result = pd.merge(features_from_database.loc[[row[0]]], X_real_quality, how="cross")
            result_end = pd.concat([result_end, result], ignore_index=True)
        except:
            continue

    y_real_2 = result_end[quality_parameter]
    X_real_2 = result_end.drop(columns=["Tensile_strength", "Elongation_at_break", "Impact_strength", "Geometric_accuracy"])
    
    
     

    return(X_real_2, y_real_2)

# X_real, y_real = get_Xreal_and_yreal(X_real, "Geometric_accuracy")


def run_analysis(X, y, quality_parameter=None, n_features=8, n_selected_features=8, percentile=100):

    """
    Based on the data (X and y) and the quality-parameter, the best model is chosen and applied.
    The performance of the model and the feature importance is then displayed.
    To monitor the training process, the global status variable
    is used for the progress bar. 

    Inputs:
        - X (pd.DataFrame): Processdata
        - y (numpy.ndarray): Data of the quality-parameter
        - n_features (int): Number of features (wird demnächst nicht mehr gebraucht, da die df bereits column-namen haben)
        - n_selected_features (int): number of features to choose
    Output:
        - fig_dict (dict): dictionary of plots for performance and feature importance
    """

    # getting the data into the right data format
    X_np = X.to_numpy()
    X = pd.DataFrame(X_np, columns=[str(X.columns[i]) for i in range(len(X.columns))])
    # X = pd.DataFrame(X, columns=[f'base_{i}' for i in range(n_features)])
    y = pd.Series(y, name='target')

    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2)

    # based on the training and testing datasets models are benchmarked an the best is chosen
    if quality_parameter == "Geometric_accuracy":
        get_benchmark_models_clf(X_train, X_test, y_train, y_test, quality_parameter, percentile, n_selected_features)
    else:
        get_benchmark_models_reg(X_train, X_test, y_train, y_test, quality_parameter, percentile, n_selected_features)

    status = 80
    pipeline = get_model_class(quality_parameter, percentile)
    model_class = pipeline.fit(X_train, y_train)
    print("___________________model_class______________________")
    print(model_class)
    status = 85


    # the chosen model is analysed on feature importance and performance
    data = PreloadedDataset(X_train, y_train, X_test, y_test, data_type=DataType.CLASSIFICATION) #CLASSIFICATION) #REGRESSION)
    features = data.features
    if quality_parameter == "Geometric_accuracy":
        model = Model(data_type=data.data_type, quality_parameter=quality_parameter, model_class=pipeline) #model_class["clf"])
    else:
        model = Model(data_type=data.data_type, quality_parameter=quality_parameter, model_class=pipeline) #model_class["reg"])
    tools = [PFIImportance(), PDPImportance()]
    status = 90
    ana = ImportanceAnalyzer(model=model, dataset=data, tools=tools)
    ana.run_analysis(features=features)
    status = 92
    fig_dict = ana.plot_results()

    status = 95
    # the dictionary of the plots is saved
    try:
        # create a file
        picklefile = open('quality_model/feature_importance_plots/' + str(quality_parameter) + str(percentile), 'wb')
        # pickle the dataframe
        pickle.dump(fig_dict, picklefile)
        # close file
        picklefile.close()
    except:
        print("The figures could not be saved in the Pickle-file")

    status = 98
    return fig_dict


    # fig_dict2 = run_analysis(X=X_real, y=y_real, quality_parameter="QParam3", n_features=n_features, n_selected_features=6, percentile=percentile)
def update_status(update_status):
    global status
    status = status + update_status

def training_the_models_all_percentiles(generate_new_data = False, n_features=8):
    global status
    status = 0
    global status_feature_importance
    status_feature_importance = 0
    for quality_param in ["Tensile_strength", "Elongation_at_break", "Impact_strength", "Geometric_accuracy"]:# ["Geometric_accuracy"]:
        for percentile in [10, 20, 30, 40, 50, 60, 70, 80, 90, 100]:
            print("______________________________________________________new_percentile______________________________________________________________")
            print(quality_param)
            print(percentile)
            update_status(round((0.5* 1/40), 3))
            # X_real = get_features_from_database(percentile)
            # print(X_real)
            X_real_2, y_real_2 = get_Xreal_and_yreal_tensor(quality_param, percentile)
            if generate_new_data == True:
                X_real_2, y_real_2 = generate_new_data(X_real_2, y_real_2)
            # X_real_2 = pd.concat([X_real_2, X_real_2, X_real_2, X_real_2, X_real_2, X_real_2], ignore_index=True)
            # y_real_2 = pd.concat([y_real_2, y_real_2, y_real_2, y_real_2, y_real_2, y_real_2], ignore_index=True)
            y_real_2 = y_real_2.T.to_numpy()
            fig_dict2 = run_analysis(X=X_real_2, y=y_real_2, quality_parameter=quality_param, n_features=n_features, n_selected_features=6, percentile=percentile)
            update_status(round((0.5 * 1/40), 3))
            print(status)

if __name__ == "__main__":
    training_the_models_all_percentiles()
    


    # Percentile = 0 nicht möglich!!! aus der Datenbank nehmen und auch zugriff nicht ermöglichen
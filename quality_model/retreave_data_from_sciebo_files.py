# """
# this scipt retreaves data from a csv-file for user input and saves it into the database
# - Quality-parameters: Zugfestig, Bruchdehnung, Schlagzähigkeit, Verzug/Geometrische Genauigkeit
# - the values of the Quality-parameters are transfered into the database without categorization into ok/nok
# - the categorization of the data in ok/nok is done at the preprocessing of the data
# - every part of a printing process is seen as a single datapoint, but bound together in a batch
# """



import pandas as pd
import os, sys
sys.path.append(os.getcwd()+"")
import mysql.connector
import flask_module.mySQL_functions.creating_database as creating_database
import flask_module.mySQL_functions.defining_database_strucure as defining_database_strucure
import flask_module.mySQL_functions.sensor_class as sensor_class
import flask_module.mySQL_functions.machine_class as machine_class
import flask_module.mySQL_functions.shopfloor_class as shopfloor_class
import flask_module.mySQL_functions.db_credentials as db_credentials
import flask_module.mySQL_functions.shopfloor_class as shopfloor_class
import quality_model.R03_ExtractParameters as extract_parameters
import quality_model.step_1_data_saving as data_saving
import numpy as np
import quality_model.step_4_feature_extraction
import pickle
import quality_model.step_0_machine_learning_pipeline
from datetime import datetime


"""
1. Schritt: Shopfloor (add)                     (id, name, description)
2. Schritt: Maschine (add)                      (id, name, description, manufacturer, shop_floor_id)
            product_specification (Null)
            batch (Null)
3. Schritt: product (add)                       (id, name, description)
            pre_product (add)                   (id, manufacturer, description, material, diameter, humidity, opening_date)
            operator (add)                      (id, first_mame, last_name)
            Process_step_specification (add)    (id, component_name, filling_level, printing_speed, layer_hight)
            tool (Null)                         
4. Schritt: Process_step (add)                  (id, name, started_time, ended_time, product_id, pre_product_id, perator_id, process_step_specification_id, machine_id)
            sensor (already there)              
5. Schritt: sensor_readings (add)               (id, value, timestamp, sensor_id, process_step_id)
"""


def connect():
    """
    connect to database and return cursor for further interactions with the database
    """
    mydb = mysql.connector.connect(
        host = db_credentials.host,
        user = db_credentials.user,
        password = db_credentials.password,
        database = db_credentials.database_name
    )
    mycursor = mydb.cursor(buffered=True)
    return mycursor, mydb


def add_shopfloor_machine_product_preproduct_operator_processstepspecification_processstep(host, user, password, database, shopfloor, machine, dict_file_path, key):
    """
    1. add a product
    2. add a pre_product
    3. add a operator
    4. add a process_step_specification

    Input:
        - host                          (z.B. "localhost")
        - user                          (z.B. "root")
        - password                      (z.B. "Patisserie3201!")
        - database                      (z.B. "database_test2")
        - product                       (dict)
        - pre_product                   (Dict)
        - operator                      (Dict)
        - process_step_specification    (Dict)
        - process_step                  (Dict)
    """
    # initiate cursor
    mycursor, mydb = connect()

    # print(dict_file_path["Bediener"])
    mycursor.execute("""SELECT id FROM process_step_specification WHERE component_name = %s""", [dict_file_path["Bediener"]])
    process_step_specification_desc = mycursor.fetchall()
    # if (process_step_specification_desc==[]):
    if True == True:
    
        df = pd.read_csv(dict_file_path["Bediener"], sep = ";")
        df2 = pd.read_csv(dict_file_path["Ergebnisse"], sep = ";")
        print(df2.columns)
        df3 = pd.read_csv(dict_file_path["Zeitreihen"], sep = ";")

        from datetime import datetime
        product = {"name": df["Bauteil-ID"]}
        pre_product = {"material": df["Material-Batch"][0], "opening_date": datetime.strptime(df["Oeffnungsdatum"][0], '%d.%m.%Y')}
        operator = {"first_name": "IPH", "last_name": "IPH"}
        process_step = {"started_time": convert_time_in_timestamp(df3["Time: "][0], df3[" Time2: "][0]), "ended_time": convert_time_in_timestamp(df3.at[df3.index[-1],'Time: '], df3.at[df3.index[-1],' Time2: ']), "product_id": "...", "pre_product_id": "...", "operator_id": "...", "process_step_specification_id": "..."}

        # a new shopfloor object is initialized and put into the database
        shopfloor_object = shopfloor_class.Shopfloor(host, user, password, database, shopfloor)
        shopfloor_object.add_shopfloor()

        # a new machine object is initialized and put into the database
        machine_object = machine_class.Machine(host, user, password, database, machine, shopfloor)
        machine_object.add_machine()

        # product
        mycursor.execute("""SELECT id FROM product WHERE name = %s""", [product["name"][0]])
        product_id = mycursor.fetchall()
        if (product_id==[]):
            try:
                mycursor.execute("""insert into product (name) values(%s)""",[product["name"][0]])
                mydb.commit()
                product_id = mycursor.lastrowid
            except mysql.connector.Error as err:
                print(err.msg)
        else:
            mycursor.execute("""SELECT id FROM product WHERE name = %s""", [product["name"][0]])
            product_id = mycursor.fetchone()[0]

        # pre_product
        mycursor.execute("""SELECT id FROM pre_product WHERE material = %s AND opening_date = %s""", [pre_product["material"], pre_product["opening_date"]])
        pre_product_id = mycursor.fetchall()
        if (pre_product_id==[]):
            try:
                mycursor.execute("""insert into pre_product (material, opening_date) values(%s, %s)""",[pre_product["material"], pre_product["opening_date"]])
                mydb.commit()
                pre_product_id = mycursor.lastrowid
            except mysql.connector.Error as err:
                print(err.msg)
        else:
            mycursor.execute("""SELECT id FROM pre_product WHERE material = %s""", [pre_product["material"]])
            pre_product_id = mycursor.fetchone()[0]

        # operator
        mycursor.execute("""SELECT id FROM operator WHERE first_name = %s AND last_name = %s""", [operator["first_name"], operator["last_name"]])
        operartor_id = mycursor.fetchall()
        if (operartor_id==[]):
            try:
                mycursor.execute("""insert into operator (first_name, last_name) values(%s, %s)""",[operator["first_name"], operator["last_name"]])
                mydb.commit()
                operartor_id = mycursor.lastrowid
            except mysql.connector.Error as err:
                print(err.msg)
        else:
            mycursor.execute("""SELECT id FROM operator WHERE first_name = %s AND last_name = %s""", [operator["first_name"], operator["last_name"]])
            operartor_id = mycursor.fetchone()[0]

        # process_step_specification
        try:
            mycursor.execute("""insert into process_step_specification (component_name) values(%s)""", [dict_file_path["Bediener"]])
            mydb.commit()
            process_step_specification_id = mycursor.lastrowid
        except mysql.connector.Error as err:
            print(err.msg)
        
        # get id's
        mycursor.execute("""SELECT id FROM machine WHERE name = %s""", [machine])
        machine_id = mycursor.fetchone()[0]

        try:
            mycursor.execute("""insert into process_step (started_time, ended_time, product_id, pre_product_id, operator_id, process_step_specification_id, machine_id) values(%s, %s, %s, %s, %s, %s, %s)""", [process_step["started_time"], process_step["ended_time"], product_id, product_id, operartor_id, process_step_specification_id, machine_id])
            mydb.commit()
            process_step_id = mycursor.lastrowid
        except mysql.connector.Error as err:
            print(err.msg)
        
        # for every sensor in "dict_of_sensor_names" a new sensor object is initialized and put into the database
        dict_of_sensor_names = {"Tensile_strength":"[N]", "Elongation_at_break":"[%]", "Impact_strength":"[J]", "Geometric_accuracy":"Boolean"}
        for sensor in dict_of_sensor_names:
            sensor_object = sensor_class.Sensor(host, user, password, database, sensor, None, dict_of_sensor_names[sensor], "first_machine")
            sensor_object.add_sensor()

        # add quality parameters to sensor readings
        add_quality_data_to_database(df2, process_step_id)

        # add relevant feature sensors and their sensor-readings
        extract_parameters.add_relevant_features_as_sensors_and_sensorreadings(process_step_id, dict_file_path["html_file_path"])

        # add sensor-reading 
        loop_int = 0
        total_runs = len(df3)/10000


        list_of_dataframes = data_saving.preprocessing_data(df3)

        df_of_all_features_all_percentage = quality_model.step_0_machine_learning_pipeline.get_df_of_features_all_percentage_DB(key, list_of_dataframes)
        # df_of_all_features_all_percentage = df_of_all_features_all_percentage[["id", "percent", "  Acc_X_Ext [mm/s] __mean", "  Acc_X_Ext [mm/s] __maximum", "  Acc_X_Ext [mm/s] __minimum"]]
        for column in df_of_all_features_all_percentage.columns:
            if column != "id" and column != "percent":
                sensor_object = sensor_class.Sensor(host, user, password, database, column, None, None, "first_machine")
                sensor_object.add_sensor()
                for row in range(0, len(df_of_all_features_all_percentage)):
                    insert_data_to_mySQL(column, process_step_id, df_of_all_features_all_percentage.iloc[row][column], None, df_of_all_features_all_percentage["percent"].iloc[row])
                    mydb.commit()
      
def get_df_of_features(list_of_dataframes):
    list_of_all_features = quality_model.step_4_feature_extraction.step_4_feature_extraction(list_of_dataframes)
    print(list_of_all_features)




# initiate cursor
mycursor, mydb = connect()

# insert data into mySQL-database
def insert_data_to_mySQL(sensor_name, id_process_step, value, timestamp, percentage):
    # print(sensor_name)
    insert_query = "INSERT INTO sensor_readings (value, sensor_id, process_step_id, timestamp, percentage) VALUES (%s, %s, %s, %s, %s)"
    mycursor.execute("SELECT id FROM sensor WHERE name = %s", (sensor_name,))
    sensor_id = mycursor.fetchone()[0]
    if isinstance(percentage, np.int64):
        percentage = percentage.item()
    if value == "ok":
        value = 1
    elif value == "nok":
        value = 0
    data = (value, sensor_id, id_process_step, timestamp, percentage)
    try:
        mycursor.execute(insert_query,data)
        mydb.commit()
        # print("success")
    except (mysql.connector.Error,mysql.connector.Warning) as e:
        print(e)


def add_quality_data_to_database(df_quality, process_step_id):
# for file_path in list_of_file_paths:
    for column_name in df_quality.columns:
        print(column_name)
        if column_name in ["Fmax1 [N]", "Fmax2 [N]", "Fmax3 [N]"]:
            insert_data_to_mySQL("Tensile_strength", process_step_id, df_quality[column_name][0], df_quality["Datum der Tests"][0], None)
        elif column_name in ["ƐtB1 [%]", "ƐtB2 [%]", "ƐtB3 [%]"]:
            insert_data_to_mySQL("Elongation_at_break", process_step_id, df_quality[column_name][0], df_quality["Datum der Tests"][0], None)
        elif column_name in ["Charpy1 [J]", "Charpy2 [J]", "Charpy3 [J]"]:
            insert_data_to_mySQL("Impact_strength", process_step_id, df_quality[column_name][0], df_quality["Datum der Tests"][0], None)
        elif column_name in ["Gemetrische Genauigkeit1", "Gemetrische Genauigkeit2", "Gemetrische Genauigkeit3"]:
            insert_data_to_mySQL("Geometric_accuracy", process_step_id, df_quality[column_name][0], df_quality["Datum der Tests"][0], None)


from datetime import datetime
def convert_time_in_timestamp(time, date):
    datetime_str = str(time) + str(date)
    datetime_object = datetime.strptime(datetime_str, '%H:%M:%S:%f  %d.%m.%Y')
    return datetime_object

list_of_files = {
    # 9:{
    #     "Bediener":"./data/All_Testdatasets/Versuch09/Bedienereingabe_2022 09 09_Mini Testplatte_240_80_0,2_120.csv",
    #     "html_file_path": "./data/All_Testdatasets/Versuch09/2022 09 09_Mini Testplatte_240_80_0,2_120.html",
    #     "Ergebnisse":"./data/All_Testdatasets/Versuch09/Ergebnisse_2022 09 09_ABS_Mini_240_80_0,2_120.csv", 
    #     "Zeitreihen":"./data/All_Testdatasets/Versuch09/2022-09-09 _Testplatte9_Mini_240_80_120_2(1)_altered.txt",},
    # # 10:{
    # #     "Bediener":"./data/All_Testdatasets/Versuch10/Bedienereingabe_10_2022 09 09_Testplate_230_85_0,2_120.csv",
    # #     "html_file_path": "./data/All_Testdatasets/Versuch10/2022 09 09_Mini Testplatte_230_80_0,2_120.html",
    # #     "Ergebnisse":"./data/All_Testdatasets/Versuch10/Ergenisse_10_2022 11 17__Mini_230_80_0,2_120.csv", 
    # #     "Zeitreihen":"./data/All_Testdatasets/Versuch10/2022-09-09 _Testplatte10_Mini_230_80_120_2_altered.txt",},
    # 12:{
    #     "Bediener":"./data/All_Testdatasets/Versuch12/Bedienereingabe_2022 09 12_Mini Testplatte_220_85_0,2_120.csv",
    #     "html_file_path": "./data/All_Testdatasets/Versuch12/2022 09 12_Mini Testplatte_220_85_0,2_120(1).html",
    #     "Ergebnisse":"./data/All_Testdatasets/Versuch12/Ergenisse_2022 09 12_ABS_Mini_220_85_0,2_120.csv", 
    #     "Zeitreihen":"./data/All_Testdatasets/Versuch12/2022-09-12 _Testplatte12_Mini_220_80_120_altered.txt",},
    # 13:{
    #     "Bediener":"./data/All_Testdatasets/Versuch13/Bedienereingabe_13_2022 09 12_Mini Testplatte_220_85_0,2_120.csv",
    #     "html_file_path": "./data/All_Testdatasets/Versuch13/2022 09 12_Mini Testplatte_220_85_0,2_120.html",
    #     "Ergebnisse":"./data/All_Testdatasets/Versuch13/Ergenisse_2022 09 12_ABS_Mini_220_85_0,2_120_offene Tür.csv", 
    #     "Zeitreihen":"./data/All_Testdatasets/Versuch13/2022-09-09 _Testplatte13_Mini_220_80_120_Tür offen_altered.txt",},
    # 14:{
    #     "Bediener":"./data/All_Testdatasets/Versuch14/Bedienereingabe_14_2022 09 12_Mini_210_85_0,2_120.csv",
    #     "html_file_path": "./data/All_Testdatasets/Versuch14/2022 09 12_Mini Testplatte_210_85_0,2_120.html",
    #     "Ergebnisse":"./data/All_Testdatasets/Versuch14/Ergenisse_14_2022 11 17_Mini_210_85_0,2_120.csv", 
    #     "Zeitreihen":"./data/All_Testdatasets/Versuch14/2022-09-09 _Testplatte14_Mini_210_80_120.txt",},
    # 15:{
    #     "Bediener":"./data/All_Testdatasets/Versuch15/Bedienereingabe_15_2022 09 13_Testplate_240_85_0,2_150.csv",
    #     "html_file_path": "./data/All_Testdatasets/Versuch15/2022 09 12_Mini Testplatte_240_85_0,2_150(3).html",
    #     "Ergebnisse":"./data/All_Testdatasets/Versuch15/Ergenisse_15_2022 10 14__Mini_240_85_0,2_150.csv", 
    #     "Zeitreihen":"./data/All_Testdatasets/Versuch15/2022-09-13_Testplatte15_Mini_240_85_150_altered.txt",},
    # 16:{
    #     "Bediener":"./data/All_Testdatasets/Versuch16/Bedienereingabe_16_2022 09 13_Testplate_240_85_0,2_150(2).csv",
    #     "html_file_path": "./data/All_Testdatasets/Versuch16/2022 09 12_Mini Testplatte_240_85_0,2_150(2).html",
    #     "Ergebnisse":"./data/All_Testdatasets/Versuch16/Ergenisse_16_2022 10 14__Mini_240_85_0,2_150(2).csv", 
    #     "Zeitreihen":"./data/All_Testdatasets/Versuch16/2022-09-13_Testplatte16_Mini_240_85_150_altered.txt",},
    # 17:{
    #     "Bediener":"./data/All_Testdatasets/Versuch17/Bedienereingabe_17_2022 09 13_Testplate_240_85_0,2_60.csv",
    #     "html_file_path": "./data/All_Testdatasets/Versuch17/2022 09 30_17_Mini Testplatte_240_85_0,2_60.html",
    #     "Ergebnisse":"./data/All_Testdatasets/Versuch17/Ergenisse_17_2022 09 30__Mini_240_85_0,2_60.csv", 
    #     "Zeitreihen":"./data/All_Testdatasets/Versuch17/2022-09-13_Testplatte17_Mini_240_85_60_altered.txt",},
    # 18:{
    #     "Bediener":"./data/All_Testdatasets/Versuch18/Bedienereingabe_18_2022 09 13_Testplate_240_85_0,2_60.csv",
    #     "html_file_path": "./data/All_Testdatasets/Versuch18/2022 09 30_18_Mini Testplatte_240_85_0,2_60.html",
    #     "Ergebnisse":"./data/All_Testdatasets/Versuch18/Ergenisse_18_2022 09 30__Mini_240_85_0,2_60.csv", 
    #     "Zeitreihen":"./data/All_Testdatasets/Versuch18/2022-09-13_Testplatte18_Mini_240_85_60_altered.txt",},
    # 19:{
    #     "Bediener":"./data/All_Testdatasets/Versuch19/Bedienereingabe_19_2022 10 14_Mini_240_85_0,2_60_offene_Tür_(2).csv",
    #     "html_file_path": "./data/All_Testdatasets/Versuch19/2022 10 14_19_Mini Testplatte_240_85_0,2_60.html",
    #     "Ergebnisse":"./data/All_Testdatasets/Versuch19/Ergenisse_19_2022 10 14_Mini_240_85_0,2_60_offene_Tür_(1).csv", 
    #     "Zeitreihen":"./data/All_Testdatasets/Versuch19/2022-09-13_Testplatte19_Mini_240_85_60_Offene_Tür_ab_15_min_altered.txt",},
    # 20:{
    #     "Bediener":"./data/All_Testdatasets/Versuch20/Bedienereingabe_20_2022 09 13_Testplate_250_85_0,2_60.csv",
    #     "html_file_path": "./data/All_Testdatasets/Versuch20/2022 10 18_20_Mini Testplatte_250_85_0,2_60.html",
    #     "Ergebnisse":"./data/All_Testdatasets/Versuch20/Ergenisse_20_2022 09 30__Mini_250_85_0,2_60.csv", 
    #     "Zeitreihen":"./data/All_Testdatasets/Versuch20/2022-09-13_Testplatte20_Mini_250_85_60.txt",},
    # 21:{
    #     "Bediener":"./data/All_Testdatasets/Versuch21/Bedienereingabe_21_2022 09 13_Testplate_260_85_0,2_60.csv",
    #     "html_file_path": "./data/All_Testdatasets/Versuch21/2022 10 18_21_Mini Testplatte_260_85_0,2_60.html",
    #     "Ergebnisse":"./data/All_Testdatasets/Versuch21/Ergenisse_21_2022 10 18__Mini_260_85_0,2_60.csv", 
    #     "Zeitreihen":"./data/All_Testdatasets/Versuch21/2022-09-13_Testplatte21_Mini_260_85_60_altered.txt",},
    # 22:{
    #     "Bediener":"./data/All_Testdatasets/Versuch22/Bedienereingabe_22_2022_10_25_Testplate_240_70_0,2_60.csv",
    #     "html_file_path": "./data/All_Testdatasets/Versuch22/2022_10_25_22_Mini_Testplatte_240_70_0,2_60.html",
    #     "Ergebnisse":"./data/All_Testdatasets/Versuch22/Ergenisse_22_2022_10_25__Mini_240_70_0,2_60.csv", 
    #     "Zeitreihen":"./data/All_Testdatasets/Versuch22/2022-09-13_Testplatte22_Mini_240_70_60(3)_altered.txt",},
    # 23:{
    #     "Bediener":"./data/All_Testdatasets/Versuch23/Bedienereingabe_23_2022_10_28_Testplate_240_85_0,2_100.csv",
    #     "html_file_path": "./data/All_Testdatasets/Versuch23/2022 10 28_23_Mini Testplatte_240_85_0,2_100.html",
    #     "Ergebnisse":"./data/All_Testdatasets/Versuch23/Ergenisse_23_2022_10_28__Mini_240_85_0,2_100.csv", 
    #     "Zeitreihen":"./data/All_Testdatasets/Versuch23/2022-10-28_Testplatte23_Mini_240_70_100_altered.txt",},
    # 24:{
    #     "Bediener":"./data/All_Testdatasets/Versuch24/Bedienereingabe_24_2022 11 01_Testplate_240_85_0,2_150.csv",
    #     "html_file_path": "./data/All_Testdatasets/Versuch24/2022 11 01_24_Mini Testplatte_240_85_0,2_150.html",
    #     "Ergebnisse":"./data/All_Testdatasets/Versuch24/Ergenisse_24_2022 11 01__Mini_240_85_0,2_150.csv", 
    #     "Zeitreihen":"./data/All_Testdatasets/Versuch24/2022-11-01_Testplatte24_Mini_240_70_150(1).txt",},
    # # 25:{
    # #     "Bediener":"./data/All_Testdatasets/Versuch25/Bedienereingabe_25_2022 11 03_Testplate_240_85_0,2_200.csv",
    # #     "html_file_path": "./data/All_Testdatasets/Versuch25/2022 11 03_25_Mini Testplatte_240_85_0,2_200.html",
    # #     "Ergebnisse":"./data/All_Testdatasets/Versuch25/Ergenisse_25_2022 11 17__Mini_240_85_0,2_60.csv", 
    # #     "Zeitreihen":"./data/All_Testdatasets/Versuch25/2022-11-03_Testplatte25_Mini_240_70_200.txt",},
    # # 26:{
    # #     "Bediener":"./data/All_Testdatasets/Versuch26/Bedienereingabe_26_2022 11 11_Testplate_240_85_0,2_150.csv",
    # #     "html_file_path": "./data/All_Testdatasets/Versuch26/2022 11 11_26_Mini Testplatte_240_85_0,2_150.html",
    # #     "Ergebnisse":"./data/All_Testdatasets/Versuch26/Ergenisse_26_2022 09 30__Mini_240_85_0,2_150_Filament geklemmt.csv", 
    # #     "Zeitreihen":"./data/All_Testdatasets/Versuch26/2022-11-11_Testplatte26_Mini_240_85_150_Filament geklemmt.txt",},
    # 27:{
    #     "Bediener":"./data/All_Testdatasets/Versuch27/Bedienereingabe_27_2022 11 11_Testplate_240_85_0,2_60.csv",
    #     "html_file_path": "./data/All_Testdatasets/Versuch27/2022 11 11_27_Mini Testplatte_240_85_0,2_60.html",
    #     "Ergebnisse":"./data/All_Testdatasets/Versuch27/Ergenisse_27_2022 11 17__Mini_240_85_0,2_60.csv", 
    #     "Zeitreihen":"./data/All_Testdatasets/Versuch27/2022-11-11_Testplatte27_Mini_240_85_60_Filament geklemmt.txt",},
    # 28:{
    #     "Bediener":"./data/All_Testdatasets/Versuch28/Bedienereingabe_28_2022 _Testplate_260_85_0,2_60_geklemmtes Fialment.csv",
    #     "html_file_path": "./data/All_Testdatasets/Versuch28/2022 11 17_21_Mini Testplatte_260_85_0,2_60.html",
    #     "Ergebnisse":"./data/All_Testdatasets/Versuch28/Ergenisse_28_2022 __Mini_260_85_0,2_60-geklemmtes Filament.csv", 
    #     "Zeitreihen":"./data/All_Testdatasets/Versuch28/2022-11-21_Testplatte28_Mini_260_85_60_Filament geklemmt.txt",},
    # 29:{
    #     "Bediener":"./data/All_Testdatasets/Versuch29/Bedienereingabe_29_2022 11 17_Testplate_260_85_0,2_60_geklemmtes Fialment.csv",
    #     "html_file_path": "./data/All_Testdatasets/Versuch29/2022 11 17_29_Mini Testplatte_250_85_0,2_60.html",
    #     "Ergebnisse":"./data/All_Testdatasets/Versuch29/Ergenisse_29_2022 __Mini_250_85_0,2_60.csv", 
    #     "Zeitreihen":"./data/All_Testdatasets/Versuch29/2022-11-11_Testplatte29_Mini_250_85_60.txt",},
    # 30:{
    #     "Bediener":"./data/All_Testdatasets/Versuch30/Bedienereingabe_30_2022 11 18_Testplate_250_85_0,2_100_geklemmtes Fialment.csv",
    #     "html_file_path": "./data/All_Testdatasets/Versuch30/2022 11 18_30_Mini Testplatte_250_85_0,2_100.html",
    #     "Ergebnisse":"./data/All_Testdatasets/Versuch30/Ergenisse_30_2022 __Mini_250_85_0,2_100.csv", 
    #     "Zeitreihen":"./data/All_Testdatasets/Versuch30/2022-11-18_Testplatte30_Mini_250_85_100.txt",},
    40:{
        "Bediener":"./data/All_Testdatasets/Versuch40/Bedienereingabe_40_2023 01 09_Testplate_240_85_0,2_100.csv",
        "html_file_path": "./data/All_Testdatasets/Versuch40/2023 01 09_40_Mini Testplatte_240_85_0,2_100.html",
        "Ergebnisse":"./data/All_Testdatasets/Versuch40/Ergenisse_40_2023 01 09__Mini_240_85_0,2_100.csv", 
        "Zeitreihen":"./data/All_Testdatasets/Versuch40/2023-01-09_Testplatte40_Mini_240_85_100.txt",},
    }


for key in list_of_files:
    add_shopfloor_machine_product_preproduct_operator_processstepspecification_processstep(db_credentials.host, db_credentials.user, db_credentials.password, db_credentials.database_name, "shopfloor_IPH", "printer_IPH", list_of_files[key], key)



def add_Quality_readings():
    pass

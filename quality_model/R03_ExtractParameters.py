
import os, sys
sys.path.append(os.getcwd()+"")
import re
import xlsxwriter
import openpyxl
import pandas as pd
import flask_module.mySQL_functions.sensor_class as sensor_class
import flask_module.mySQL_functions.db_credentials as db_credentials
import mysql.connector



def get_relevant_features(html_file_path):
    RelevanteDruckParameterName = []
    listData_Material = []
    listData = []
    # open the sample file used
    # parameterFile_HTML = open('./data/UMS5_zylinder.html')
    parameterFile_HTML = open(html_file_path)
    
    # read the content of the html file and save in .txt file
    content = parameterFile_HTML.readlines()
    savedFile = open('./data/UMS5_zylinder.txt', 'w')
    for line in content:
        savedFile.write(line)

    # read the content of .txt file
    parameterFile_Txt = open('./data/UMS5_zylinder.txt', 'r')
    LinesTxtFile = parameterFile_Txt.readlines()
    #######################################################################################################################################
    #Read all relevant print parameters
    workbook_input = openpyxl.load_workbook("./data/Relevante_Druckparameter.xlsx")
    worksheet = workbook_input.active
    for col in worksheet.iter_cols(min_row=3, max_row=55, min_col=5, max_col=5):
        for cell in col:
            RelevanteDruckParameterName.append(cell.value)
    # print(RelevanteDruckParameterName)

    ####################### search for lines in .txt file containing Parameter Names ####################################################
    for RelDruParName in RelevanteDruckParameterName:
        ParameterName = '>'+RelDruParName+'<'
        
        if RelDruParName == 'Material Extruder : 1':
            str_match_Material_idx = [i for i, item in enumerate(LinesTxtFile) if ParameterName in item]
            str_match_Material     = [x for x in LinesTxtFile if re.findall(ParameterName, x)]
        else:    
            str_match_idx = [i for i, item in enumerate(LinesTxtFile) if ParameterName in item]
            str_match     = [x for x in LinesTxtFile if re.findall(ParameterName, x)]

            for w in str_match_Material:
                listData_Material.append(w.replace("\'", "").\
                                        replace('<tr><td class=w-50>','Name_').replace('</td><td colspan=2>', '_Name_Value_').\
                                        replace('</td></tr>', '_Value').replace('\n', ''))

            for s in str_match:
                listData.append(s.replace("\'", "").replace('disabled','').replace('normal','').replace('local','').\
                                replace('<tr class=><td class=w-70 pl-0>','Name_').replace('<tr class=><td class=w-70 pl-1>','Name_').\
                                replace('<tr class=><td class=w-70 pl-2>','Name_').replace('<tr class=><td class=w-70 pl-3>','Name_').\
                                replace('</td><td class=val>', '_Name_Value_').replace('</td><td class=w-10>', '_Value_Unit_').\
                                replace('</td></tr>', '_Unit').replace('\n', '').\
                                replace('<tr><td class=w-50>', 'Name_').replace('</td><td colspan=2>', '_Value_'))

    ################ extract name, value and unit in excel ##########################
    list_name_Material=[]
    list_value_Material=[]
    list_name=[]
    list_value=[]
    list_unit=[]

    for i_Material in listData_Material:
        new_list_Material_name =re.findall('Name_' +'(.*)' + '_Name', i_Material)   #search for Eigenschaft:Name for list item [1]
        new_list_Material_value=re.findall('Value_' +'(.*)' + '_Value', i_Material)#search for Eigenschaft:Value for list item [1]
        
        list_name_Material.append(new_list_Material_name[0])  #Extract the single list items and put them in a new list all together
        list_value_Material.append(new_list_Material_value[0])#Extract the single list items and put them in a new list all together

    for i in listData:
        new_list_name =re.findall('Name_' +'(.*)' + '_Name', i)   #search for Eigenschaft:Name for list item [1]
        new_list_value=re.findall('Value_' +'(.*)' + '_Value', i)#search for Eigenschaft:Value for list item [1]
        new_list_unit =re.findall('Unit_' +'(.*)' + '_Unit', i)   #search for Eigenschaft:Unit for list item [1]

        list_name.append(new_list_name[0])  #Extract the single list items and put them in a new list all together
        list_value.append(new_list_value[0])#Extract the single list items and put them in a new list all together
        list_unit.append(new_list_unit[0])  #Extract the single list items and put them in a new list all together

    ################ prints the values in excel ##########################
    workbook_output = xlsxwriter.Workbook('./data/UMS5_zylinder_Parameter_Saviour.xlsx')
    worksheet_output = workbook_output.add_worksheet()

    worksheet_output.write(0, 0, 'Name')
    worksheet_output.write(0, 1, 'Value')
    worksheet_output.write(0, 2, 'Unit')


    ## List Name (Extruder Material)
    for item in (list_name_Material):
        worksheet_output.write(1, 3, item)
    for item in (list_value_Material):
        worksheet_output.write(1, 4, item)

    ## List Name (All other parameters)
    row = 1
    col = 0

    for item in (list_name):
        worksheet_output.write(row, col, item)
        row += 1

    ## List Value
    row = 1
    col = 1
    for item in (list_value):
        worksheet_output.write(row, col, item)
        row += 1
    ## List Unit
    row = 1
    col = 2
    for item in (list_unit):
        worksheet_output.write(row, col, item)
        row += 1
    workbook_output.close()

    df_relevant_features = pd.read_excel('./data/UMS5_zylinder_Parameter_Saviour.xlsx')

    return df_relevant_features


def connect():
    """
    connect to database and return cursor for further interactions with the database
    """
    mydb = mysql.connector.connect(
        host = db_credentials.host,
        user = db_credentials.user,
        password = db_credentials.password,
        database = db_credentials.database_name
    )
    mycursor = mydb.cursor(buffered=True)
    return mycursor, mydb


# add relevant features as sensors
def add_relevant_features_as_sensors_and_sensorreadings(process_step_id, html_file_path):
    df_relevant_features = get_relevant_features(html_file_path)
    # print(df_relevant_features)
    for i in range(0, len(df_relevant_features)):
        # print(i)
        sensor_name = df_relevant_features["Name"][i]
        value = df_relevant_features["Value"][i]
        unit = df_relevant_features["Unit"][i]
        if unit == "mm/sÂ²":
            unit = "mm/s²"
        if unit == "Â°C":
            unit = "°C"
        if df_relevant_features["Unit"].isnull()[i]:
            unit = None
        if unit == "Â°":
            unit = "°"
        sensor_object = sensor_class.Sensor(db_credentials.host, db_credentials.user, db_credentials.password, db_credentials.database_name, sensor_name, None, unit, "first_machine")
        sensor_object.add_sensor()
        mycursor, mydb = connect()
        mycursor.execute("""SELECT id FROM sensor WHERE name = %s""", [sensor_name])
        sensor_id = mycursor.fetchone()[0]

        # add sensor-reading to each sensor
        try:
            mycursor.execute("""insert into sensor_readings (value, sensor_id, process_step_id) values(%s,%s,%s)""",[value,sensor_id, process_step_id])
            mydb.commit()
        
        except mysql.connector.Error as err:
            print(err.msg)


# dict_of_sensor_names = {"Tensile_strength":"[N]", "Elongation_at_break":"[%]", "Impact_strength":"[J]", "Geometric_accuracy":"Boolean"}
# for sensor in dict_of_sensor_names:
#     sensor_object = sensor_class.Sensor(db_credentials.host, db_credentials.user, db_credentials.password, db_credentials.database_name, sensor, dict_of_sensor_names[sensor], "first_machine")
#     sensor_object.add_sensor()

# add relevant features to sensor readings



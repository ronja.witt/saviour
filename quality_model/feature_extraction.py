import pandas as pd
import os, sys
sys.path.append(os.getcwd()+"")
import quality_model.step_1_data_saving as data_saving
import quality_model.step_4_feature_extraction



def extract_features(df):
    """
    Extracts the features from the sensordata
    Input:
        - df: Dataframe of sensordata
    Output:
        - df_of_all_features: features in one Dataframe
    """
    try:
        list_of_dataframes = data_saving.preprocessing_data(df)
    except:
        list_of_dataframes = data_saving.preprocessing_data_new(df)
    df_of_all_features = get_df_of_features(list_of_dataframes)
    print(df_of_all_features)
    return df_of_all_features


def get_df_of_features(list_of_dataframes):
    """
    Extracts the features for each sensor and save it in one Dataframe
    Input:
        - list_of_dataframes: Dataframes of sensordata for all sensors combined in a list
    Output:
        - df_of_all_features: features in one Dataframe
    """
    df_of_all_features = quality_model.step_4_feature_extraction.step_4_feature_extraction(list_of_dataframes)
    return df_of_all_features

from sklearn.model_selection import train_test_split
from sklearn.feature_selection import SelectKBest, chi2, f_classif


# -----------------------------------------------------------------------------------------------------
# funktions for feature-selection
# -----------------------------------------------------------------------------------------------------

def select_features(feature_df, label_y_data, n_features):
    """
    select k most important features

    input: 
            extracted feature-dataframe
    output: 
            selected feature-dataframe
    """

    print(feature_df)
    label_y_data = label_y_data.values.ravel()
    select = SelectKBest(score_func=f_classif, k=n_features)
    select.fit_transform(feature_df,label_y_data)
    filter = select.get_support()
    features = feature_df.columns
    selected_features_df = feature_df[features[filter]]
    return selected_features_df
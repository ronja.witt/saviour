import time
from unicodedata import name
import mysql.connector # necessary to connect to mySQL database
import json # necessary for converting JSON data into python-readable data and vice versa
import csv
import pandas as pd
import numpy as np
import os, sys
import datetime
sys.path.append(os.getcwd()+"")
import flask_module.mySQL_functions.db_credentials as db_credentials
start_time = time.time()


mydb = mysql.connector.connect(
  host=db_credentials.host,
  user=db_credentials.user,
  password=db_credentials.password,
  database=db_credentials.database_name
)

## mycourser allows you to use mySQL commands
mycursor = mydb.cursor(buffered=True)


# determine which sensors are in the database and what id they have
# put the id_sensor in a dictionary for each sensor
def make_dictionary_id_sensors(df_input):
    dict_id_sensor = {}
    for k in df_input.columns:
        sql = "SELECT id FROM sensor WHERE name = %s"
        val = (k.split()[0],)
        mycursor.execute(sql, val)
        id_list = mycursor.fetchone()
        if id_list != None:
            id_sensor = id_list[0]
            dict_id_sensor[k.split()[0]] = id_sensor

    return dict_id_sensor


def one_hot_encoding_for_objecttype_variables(filtered_df_2):
    """
    makes a new dataframe for the object-type variables
            - if the datatype of a variable is "object" the variable is treated as a categorical one
            - in that case one-hot-encoding is applied
            - else if the variable is not categorical, df_categorical_return is empty

    input: 
            dataframe of a single sensor without NaN-values
    output: 
            new dataframe for categorical data
    """

    df_categorical_return = pd.DataFrame()
    df_categorical = filtered_df_2.select_dtypes(include=['object']).copy()

    if df_categorical.empty == False:
        df_categorical_neu = df_categorical._convert(numeric=True).dropna()
        df_categorical_neu = df_categorical_neu.select_dtypes(include=['object']).copy()
        if df_categorical_neu.empty == False:
            # implement one_hot_encoding for every categorical dataframe
            df_categorical_onehot =  pd.get_dummies(filtered_df_2, columns=[df_categorical.columns[0]], prefix = [df_categorical.columns[0]])
            df_categorical_return = df_categorical_onehot
    return df_categorical_return

# preprocessing_data
def preprocessing_data(df_input):
    insert_query = "INSERT INTO sensor_readings (sensor_id, process_step_id, value, timestamp) VALUES (%s, %s, %s, %s)"
    id_dict = {}
    id_dict = make_dictionary_id_sensors(df_input)
    list_of_dataframes = list()
    for j in df_input.columns:
        if j.split()[0] in id_dict:
            # print(df_input[["Time: ", j]])
            df_neu = df_input[["Time: ", j]].replace(" ", np.nan, inplace=False)
            # print(df_neu)
            if j.split()[0] != "DoorState_closed":
                try:
                    df_neu[j] = df_neu[j].replace(" Geschlossen", np.nan, inplace=False)
                    # df_neu[j] = df_neu[j].replace("0.140.3903 ", np.nan, inplace=False)
                    df_neu[j] = df_neu[j].astype(float)
                except:
                    pass
            filtered_df=df_neu.dropna(axis=0)
            id_sensor = id_dict[j.split()[0]]

            filtered_df["Time: "] = filtered_df["Time: "].str.replace(" ", "")
            filtered_df["Time: "] = pd.to_datetime(filtered_df.loc[:, "Time: "], format="%H:%M:%S:%f", errors="coerce")
            filtered_df["Time: "] = pd.to_datetime(filtered_df["Time: "], format='%Y-%m-%d %H:%M:%S:%f')

            df_categorical_return = one_hot_encoding_for_objecttype_variables(filtered_df)

            # for categorical data: append categorical dataframe
            if df_categorical_return.empty == False:
                list_of_dataframes.append(df_categorical_return)

            # else: append the filtered dataframe
            else:
                list_of_dataframes.append(filtered_df)
    return list_of_dataframes

# preprocessing_data
def preprocessing_data_current_data(df_input):
    insert_query = "INSERT INTO sensor_readings (sensor_id, process_step_id, value, timestamp) VALUES (%s, %s, %s, %s)"
    id_dict = {}
    id_dict = make_dictionary_id_sensors(df_input)
    list_of_dataframes = list()
    for j in df_input.columns:
        if j.split()[0] in id_dict:
            # print(df_input[["Time: ", j]])
            df_neu = df_input[["time", j]].replace(" ", np.nan, inplace=False)
            if j.split()[0] != "DoorState_closed":
                try:
                    df_neu[j] = df_neu[j].replace(" Geschlossen", np.nan, inplace=False)
                    df_neu[j] = df_neu[j].astype(float)
                except:
                    pass
            filtered_df=df_neu.dropna(axis=0)
            id_sensor = id_dict[j.split()[0]]

            filtered_df["time"] = filtered_df["time"].str.replace(" ", "")
            filtered_df["time"] = pd.to_datetime(filtered_df.loc[:, "time"], format="%H:%M:%S:%f", errors="coerce")
            filtered_df["time"] = pd.to_datetime(filtered_df["time"], format='%Y-%m-%d %H:%M:%S:%f')

            df_categorical_return = one_hot_encoding_for_objecttype_variables(filtered_df)

            # for categorical data: append categorical dataframe
            if df_categorical_return.empty == False:
                list_of_dataframes.append(df_categorical_return)

            # else: append the filtered dataframe
            else:
                list_of_dataframes.append(filtered_df)
    return list_of_dataframes

def preprocessing_data_new(df_input):
    insert_query = "INSERT INTO sensor_readings (sensor_id, process_step_id, value, timestamp) VALUES (%s, %s, %s, %s)"
    id_dict = {}
    id_dict = make_dictionary_id_sensors(df_input)
    list_of_dataframes = list()
    for j in df_input.columns:
        if j.split()[0] in id_dict:
            df_neu = df_input[["time", j]].replace(" ", np.nan, inplace=False)
            if j.split()[0] != "DoorState_closed":
                try:
                    df_neu[j] = df_neu[j].replace(" Geschlossen", np.nan, inplace=False)
                    df_neu[j] = df_neu[j].astype(float)
                except:
                    pass
            filtered_df=df_neu.dropna(axis=0)
            id_sensor = id_dict[j.split()[0]]
            filtered_df["time"] = filtered_df["time"].str.replace(" ", "")
            filtered_df["time"] = pd.to_datetime(filtered_df.loc[:, "time"], format="%H:%M:%S:%f", errors="coerce")
            print(filtered_df["time"])
            filtered_df["time"] = pd.to_datetime(filtered_df["time"], format='%Y-%m-%d %H:%M:%S:%f')
            df_categorical_return = one_hot_encoding_for_objecttype_variables(filtered_df)

            # for categorical data: append categorical dataframe
            if df_categorical_return.empty == False:
                list_of_dataframes.append(df_categorical_return)

            # else: append the filtered dataframe
            else:
                list_of_dataframes.append(filtered_df)
    return list_of_dataframes

# insert data into mySQL-database
def insert_data_to_mySQL(df_input, id_process_step):
    insert_query = "INSERT INTO sensor_readings (sensor_id, process_step_id, value, timestamp) VALUES (%s, %s, %s, %s)"
    id_dict = {}
    id_dict = make_dictionary_id_sensors(df_input)
    for j in df_input.columns:
        if j.split()[0] in id_dict:
            df_neu = df_input[["Time: ", j]].replace(" ", np.nan, inplace=False)
            if j.split()[0] != "DoorState_closed":
                try:
                    df_neu[j] = df_neu[j].replace(" Geschlossen", np.nan, inplace=False)
                    df_neu[j] = df_neu[j].astype(float)
                except:
                    pass
            filtered_df=df_neu.dropna(axis=0)
            id_sensor = id_dict[j.split()[0]]
            filtered_df["sensor_id"] = id_sensor
            filtered_df["process_step_id"] = id_process_step
            filtered_df["Time: "] = pd.to_datetime(filtered_df["Time: "], format="%H:%M:%S:%f  ")
            filtered_df["Time: "] = pd.to_datetime(filtered_df["Time: "], format='%Y-%m-%d %H:%M:%S:%f')

            data = list(zip(filtered_df["sensor_id"], filtered_df["process_step_id"], filtered_df[j], filtered_df["Time: "]))
            try:
                mycursor.executemany(insert_query,data)
                mydb.commit()
            except (mysql.connector.Error,mysql.connector.Warning) as e:
                print(e)

def calculate_past_time(seconds):
    days = (seconds // (60 * 60 * 24))
    days_leftover = (seconds % (60 * 60 * 24))
    hours = (days_leftover) // (60 * 60)
    hours_leftover = (days_leftover) % ( 60 * 60)
    minutes = (hours_leftover) // (60)
    minutes_leftover = (hours_leftover) % (60)
    seconds = (minutes_leftover)//1
    return int(seconds), int(minutes), int(hours), int(days)

print("--- %ss  %sm  %sh  %sd---" % (calculate_past_time(time.time() - start_time)))

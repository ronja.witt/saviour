
# Anleitung zum Setup der Saviour App

*Hinweis: Die App und insbesondere die Skripte setup_saviour.cmd und start_saviour.cmd sind für Windows konzipiert!*

Bevor Sie die Saviour App nutzen können, müssen zunächst einige Programme installiert werden. Folgende Programme werden für die App benötigt:<br>
> - Xampp (Apache Server und MariaDB)<br>
> *Hinweis: Installation unter C:\xampp*<br>
> Siehe: https://www.apachefriends.org/index.html<br>
> - Mosquitto Broker<br>
> Siehe: https://mosquitto.org/download/<br>
> - Python 3<br>
> Siehe: https://www.python.org/<br>

Bitte installieren Sie zunächst alle oben genannten Programme, bevor Sie weitermachen.

Wenn alle oben genannten Programme installiert sind, können Sie die Dateien von dem Git Repository herunterladen. Die benötigten Dateien finden Sie hier: https://git-ce.rwth-aachen.de/ronja.witt/Saviour2/-/tree/master 

Wenn alle Dateien heruntergeladen und entpackt wurden, kann der Setup Prozess gestartet werden. Gehen Sie hierfür in den Ordner: „Saviour2\start_flask“ und starten Sie die Datei „setup_saviour.cmd“. *Hinweis: Der Setupprozess kann je nach Internetverbindung relativ lange dauern. (In meinem Testdurchlauf hat dies etwa 25min gedauert.)*

Während des Setup Prozesses wird eine Verknüpfung zum Starten der App auf dem Desktop erstellt. *Aktuell wird die Verknüpfung in dem Order „C:\Users\\\<username\>\Desktop“ erstellt, das der Standardordner für den Desktop ist. Sollten Sie einen anderen Pfad für ihren Desktop haben, so müssen Sie ggf. die Verknüpfung manuell von dem Ordner „C:\Users\\\<username\>\Desktop“ auf Ihren Desktop ziehen.*

Nach dem Beenden des Setup Prozesses sollte sich auf ihrem Desktop eine Verlinkung zu dem Skript start_saviour.cmd befinden. Klicken Sie auf diese, um die Saviour App zu starten. 

# Starten der Saviour App

Bei dem Starten des Programms wird sich ein Editorfenster mit Code öffnen. Bitte geben Sie in diesem ihre Zugangsdaten für die MySQL Datenbank ein.

Die Standardwerte der Datenbank sind hierbei:<br>
> - host = "localhost"
> - user = "root"
> - password = ""
> - database_name = "db_saviour" *(Kann frei gewählt werden)*

Bitte speichern Sie diese Angaben. Ggf. müssen Sie die App anschließend erneut starten.

Des Weiteren öffnet sich ein Dialogfenster, in dem von der Datei "Saviour2_start_mosquitto.lnk" Adminrechte angefordert werden. Bitte erlauben Sie dieses. Alternativ können Sie den Mosquitto broker auch vor dem Starten der App manuell starten.

Am Ende des Startvorgangs wird ein Browserfenster mit der App geöffnet. Da die App einiges an Zeit benötigt, um zu starten, wird ihnen am Anfang ein Seiten-Ladefehler angezeigt. Dies ist nicht weiter schlimm. Sie können in der Konsole mit dem Namen "Saviour" nachsehen, ob die App bereits gestartet wurde. Laden Sie dann die Browserseite neu.

*Hinweis: Sie finden die Saviour App unter der URL: http://127.0.0.1:5000*

# Schließen der Saviour App

Um die Saviour App zu schließen, gehen Sie zunächst zu dem Konsolenfenster "Saviour". Drücken Sie hier die Tasten "Strg + c". Bestätigen Sie das Schließen mit "J". Anschließend sollten die Schließvorgänge für Apache, MySQL und Mosquitto gestartet werden. Um den Mosquitto Broker zu schließen, werden wieder Adminrechte benötigt. Natürlich ist es auch hier wieder möglich, die Programme manuell zu schließen. Am Ende des Schließvorgangs bleiben einige Konsolenfenster geöffnet. Diese müssen im Anschluss manuell geschlossen werden.


# FAQ und Troubleshooting

## Setup

### Ich habe den Setupprozess ausgeführt, kann aber das Desktop Icon nicht finden...

Aktuell wird die Verknüpfung in dem Order „C:\Users\\\<username\>\Desktop“ erstellt, das der Standardordner für den Desktop ist. Sollten Sie einen anderen Pfad für ihren Desktop haben, so müssen Sie ggf. die Verknüpfung manuell von dem Ordner „C:\Users\\\<username\>\Desktop“ auf Ihren Desktop ziehen.

### Der Setupprozess funktioniert nicht. Was kann ich tun?

Für den Setupprozess wird die Datei `setup_saviour.cmd` verwendet. Sie können die erste Codezeile zu `@ECHO: OFF` umschreiben und die Datei speichern und neuausführen. Nun sollten Sie in der Konsole sehen können, welche Befehle gerade durchgeführt werden. Fügen Sie ggf. den Befehl `PAUSE` zwischen Codezeilen hinzu, um an diesen Stellen zu stoppen. Überprüfen Sie die Dateipfade Ihrer installierten Programme mit den im Code / in der Konsole abgefragten Dateipfaden. Falls das nicht weiterhilft, können Sie den Setupprozess auch manuell durchführen.

### Muss ich für den Setupprozess die Datei setup_saviour.cmd verwenden?

Nein, das Skript soll lediglich dazu dienen, den Setupprozess zu vereinfachen. Natürlich können Sie das Setup auch auf eine andere Art durchführen. Hierfür müssen die folgenden Schritte durchgeführt werden:

- Überprüfen Sie, dass XAMPP, Mosquitto und Python3 installiert sind
- Erstellen Sie bei Bedarf einen Desktop Shortcut zu der Datei `start_saviour.cmd`
  - Hierfür können Sie das Icon `logo-app2.ico` verwenden
- Öffnen Sie eine Konsole und wechseln Sie zu Ihrem Saviour Ordnerpfad
- Erstellen Sie hier eine virtuelle Umgebung mit dem Befehl `py -m venv venv`
- Installieren Sie alle benötigten Python Pakete in die virtuelle Umgebung. Nutzen Sie hierfür den Befehl `pip install -r requirements.txt` *Hinweis: Dieser Vorgang kann eine Weile dauern.*
- Deaktivieren Sie die virtuelle Umgebung
- Sie sollten nun die App starten können

## Starten und Schließen der App

### Muss ich das Desktop Icon zum Starten der App benutzen?

Nein, das Start Skript soll lediglich dazu dienen, den Startprozess zu vereinfachen. Natürlich kann die App auch auf andere Art geöffnet werden. Hierfür müssen die folgenden Schritte durchgeführt werden:

- Starten Sie den Mosquitto Broker
- Starten Sie den Apache Server und die MySQL Datenbank (z.B durch das Starten der xampp-console und betätigen der entsprechenden Buttons)
- Öffnen Sie eine Konsole und wechseln Sie zu Ihrem Saviour Ordnerpfad
- Aktivieren Sie die virtuelle Umgebung durch eingeben von: `venv\Scripts\activate`
- Führen Sie in dem gleichen Konsolenfenster die Datei `flask_module\app3.py` aus

### Die App schließt sich nach dem Starten direkt wieder...
Das könnte daran liegen, dass ein Fehler in der App aufgetreten ist. Bitte sehen Sie in der Konsole "Saviour" nach, welcher Fehler entstanden ist. 

Ein häufiger Fehler ist, dass die Zugangsdaten zu der Datenbank nicht korrekt sind. Bitte überprüfen Sie diese.

## Sonstiges

import os, sys
from colorama import Cursor
sys.path.append(os.getcwd()+"")
import mysql.connector 
from mysql.connector import Error
import pickle
import numpy as np
import pandas as pd
from sqlalchemy import create_engine




def add_sensors(name_sensor, name_machine):
    """
    function which adds sensors to the data base
    function checks, whether sensor already exists in the 
    data base and adds the sensor if it doesn't
    
    Input:
        - name_sensor       (z.B. "sensor1")
        - name_machine      (z.B. "first_machine")
    """

    sensor_insert_query =  """
    insert into sensor (name,machine_id) 
    values(%s,(select machine.id from machine
    where name = %s))
    """

    mycursor, mydb = connect()
    mycursor.execute("""SELECT id FROM sensor WHERE name = %s""", [name_sensor])
    result = mycursor.fetchall()
    print(result)
    if (result==[]):
        try:
            # mycursor.execute("""insert into sensor (name,machine_id) values(%s,(select machine.id from machine where name = %s))""",[name_sensor,name_machine])
            mycursor.execute(sensor_insert_query,[name_sensor,name_machine])
            mydb.commit()
            print('success')
        
        except mysql.connector.Error as err:
            print(err.msg)


def connect():
    """ Connect to MySQL database """
    mydb = mysql.connector.connect(
        host="localhost",
        user="root",
        password="Patisserie3201!",
        database="database_test2"
        )
    mycursor = mydb.cursor()
    return mycursor, mydb

# add_data_point()





measurement_insert_query = """
insert into sensor_readings (value,timestamp,sensor_id,process_step_id) 
values(%s,%s,(select sensor.id from sensor
where name = %s),%s)
"""

# read the pickle file
picklefile = open('list_of_sensordataframes', 'rb')

# unpickle the dataframe
list_of_sensordataframes = pickle.load(picklefile)

# close file
picklefile.close()

# define X and y
y = pd.DataFrame({"result" : [0, 0, 0]})
data =  list_of_sensordataframes

mycursor, mydb = connect()

# read data in chunks

def add_data_points(list_of_input_dataframes):
    for df in list_of_input_dataframes:
        print(df.columns[1])
        add_sensors(df.columns[1],"first_machine")
        df_chunks = pd.DataFrame()
        df_chunks["value"] = df[df.columns[1]]
        df_chunks["timestamp"] = df[df.columns[0]]
        df_chunks["sensor_id"] = df.columns[1]
        df_chunks["process_step_id"] = 1

        for chunk in np.array_split(df_chunks, 10):
            # zip the data
            data = list(zip(chunk['value'],chunk['timestamp'],chunk['sensor_id'],
                            chunk['process_step_id']))
            
            # insert data into db
            try:
                mycursor.executemany(measurement_insert_query,data)
                mydb.commit()
                print('success')
            
            except mysql.connector.Error as err:
                print(err.msg)

#     mycursor.execute("""SELECT id FROM sensor WHERE id = 12345""")
#     result = mycursor.fetchall()
#     print(result)
# # print(data)

add_sensors("sensor1", "first_machine")
# basic structure of the database:
# - database:
#       - batch: (id)
#       - machine: (id, name, description, machinetype) + FOREIGN KEY (shop_floor)
#       - ml_training: (id, last_training_date, last_training_time)
#       - operator: (id, first_name, last_name)
#       - pre_product: (id, name, description, material, diameter, humidity, date_of_production) + FOREIGN KEY (product_specification)
#       - process_step: (id, started_time, ended_time) + FOREIGN KEY (product, pre_product, operator, process_step_specification, machine, tool)
#       - process_step_specification: (id, component_name, filling_level, printing_speed, layer_height)
#       - product: (id, name, description) + FOREIGN KEY (batch, product_specification)
#       - product_specification: (id)
#       - sensor: (id, name, description, unit, virtual, lower_limit, upper_limit, measurement_uncertainty) + FOREIGN KEY (machine)
#       - sensor_reading: (id, value, timestamp) + FOREIGN KEY (sensor, process_step)
#       - shop_floor: (id, name, description)
#       - tool: (id, name, description) + FOREIGN KEY (machine)



foreign_key_list_batch = []
foreign_key_list_machine = ["shop_floor"]
foreign_key_list_operator = []
foreign_key_list_pre_product = ["product_specification"]
foreign_key_list_process_step = ["product", "pre_product", "operator", "process_step_specification", "machine", "tool"]
foreign_key_list_process_step_specification = []
foreign_key_list_product = ["batch", "product_specification"]
foreign_key_list_product_specification = []
foreign_key_list_sensor = ["machine"]
foreign_key_list_sensor_reading = ["sensor", "process_step"]
foreign_key_list_shop_floor = []
foreign_key_list_tool = ["machine"]


dict_batch = {
}

dict_machine = {
    "name": "VARCHAR(127)",
    "description": "VARCHAR(127)",
    "manufacturer": "VARCHAR(127)",
}

dict_ml_training = {
    "last_training": "DATETIME",
}

dict_operator = {
    "first_name": "VARCHAR(127)",
    "last_name": "VARCHAR(127)",
}

dict_pre_product = {
    "manufacturer": "VARCHAR(127)",
    "description": "VARCHAR(127)",
    "material": "VARCHAR(127)",
    "diameter": "float",
    "humidity": "float",
    "opening_date": "DATE",
}

dict_process_step = {
    "name": "VARCHAR(127)",
    "started_time": "DATETIME",
    "ended_time": "DATETIME",
}

dict_process_step_specification = {
    "component_name": "VARCHAR(127)",
    "expected_duration": "BIGINT",
}

dict_product ={
    "name": "VARCHAR(127)",
    "description": "VARCHAR(127)",
}

dict_product_specification = {
}

dict_sensor = {
    "name": "VARCHAR(127)",
    "description": "VARCHAR(127)",
    "unit": "VARCHAR(127)",
    "virtual_sensor": "tinyint(1)",
    "lower_limit": "float",
    "upper_limit": "float",
    "measurement_uncertainty": "float",
}

dict_sensor_readings = {
    "value": "float",
    "timestamp": "DATETIME",
    "percentage": "tinyint(1)"
}

dict_shop_floor = {
    "name": "VARCHAR(127)",
    "description": "VARCHAR(127)",
}

dict_tool = {
    "name": "VARCHAR(127)",
    "description": "VARCHAR(127)",
}

dict_entity_foreign_keys = {
    "batch" : foreign_key_list_batch,
    "machine" : foreign_key_list_machine,
    "operator" : foreign_key_list_operator,
    "pre_product" : foreign_key_list_pre_product,
    "process_step" : foreign_key_list_process_step,
    "process_step_specification" : foreign_key_list_process_step_specification,
    "product" : foreign_key_list_product,
    "product_specification" : foreign_key_list_product_specification,
    "sensor" : foreign_key_list_sensor,
    "sensor_readings" : foreign_key_list_sensor_reading,
    "shop_floor" : foreign_key_list_shop_floor,
    "tool" : foreign_key_list_tool,
}

dict_entity_columns = {
    "batch" : dict_batch,
    "machine" : dict_machine,
    "ml_training" : dict_ml_training,
    "operator" : dict_operator,
    "pre_product" : dict_pre_product,
    "process_step" : dict_process_step,
    "process_step_specification" : dict_process_step_specification,
    "product" : dict_product,
    "product_specification" : dict_product_specification,
    "sensor" : dict_sensor,
    "sensor_readings" : dict_sensor_readings,
    "shop_floor" : dict_shop_floor,
    "tool" : dict_tool,
}

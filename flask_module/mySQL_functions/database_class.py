import os, sys
sys.path.append(os.getcwd()+"")
import mysql.connector 
from flask_module.mySQL_functions.table_class import Table

class Database:
    """
    class object of a database in mySQL
    class provides basic functions to interact with databases in mySQL:
        - init
        - connect
        - create_database
        - delete_database
    Input:
        - host          (z.B. "localhost")
        - user          (z.B. "root")
        - password      (z.B. "Patisserie3201!")
        - database      (z.B. "database_test1")
    """
  

    def __init__(self, host, user, password, database_name) -> None:
        """
        assign basic information to the properties of the database-object
        """
        self.host = host
        self.user = user
        self.password = password
        self.database_name = database_name
      
    def connect(self):
        """
        connect to mySQL and return cursor for further interactions with mySQL
        """
        mydb = mysql.connector.connect(
          host = self.host,
          user = self.user,
          password = self.password
        )
        mycursor = mydb.cursor()
        return mycursor, mydb

    def create_database(self):
        """
        creates a database in mySQL
        """
        mycursor, mydb = self.connect()
        
        # mySQL has a way of checking, whether a database already exists (CREATE DATABASE IF NOT EXISTS)
        # However, then it is not possible to catch errors when creating a database that already exists and you would overwrite it. 
        # Therefore, the error message is provoked and caught here when creating a database with an already existing name.
        mycursor.execute("CREATE DATABASE " + str(self.database_name))
        if mydb is not None and mydb.is_connected():
            mydb.close()
    
    def delete_database(self):
        """
        deletes a database in mySQL
        """
        mycursor, mydb = self.connect()
        mycursor.execute("DROP DATABASE IF EXISTS "+ str(self.database_name))
        if mydb is not None and mydb.is_connected():
            mydb.close()
            
    def create_table(self, table_name):
        table1 = Table(self.host, self.user, self.password, self.database_name, table_name)
        # table1.connect()
        table1.create_table()
        return table1


# # some examples on how to interact with the database-class
# database1 = Database("localhost", "root", "Patisserie3201!", "database_test5")
# database1.create_database()
# table_test1 = database1.create_table("table_test1")
# table_test1.add_columns
# database1.delete_database()
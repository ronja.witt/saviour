"""
creates database with a database-structure and predefined shopfloor, machine and sensors
"""

import os, sys
sys.path.append(os.getcwd()+"")
# import mysql.connector
import flask_module.mySQL_functions.creating_database as creating_database
import flask_module.mySQL_functions.defining_database_strucure as defining_database_strucure
import flask_module.mySQL_functions.sensor_class as sensor_class
import flask_module.mySQL_functions.machine_class as machine_class
import flask_module.mySQL_functions.shopfloor_class as shopfloor_class
import flask_module.mySQL_functions.db_credentials as db_credentials


def create_database_with_predifined_entries(host, user, password, database):
    """
    1. create database
    2. add a shopfloor
    3. add a machine
    4. add the sensors

    Input:
        - host          (z.B. "localhost")
        - user          (z.B. "root")
        - password      (z.B. "Patisserie3201!")
        - database      (z.B. "database_test2")
    """

    # create database with database-structure
    creating_database.create_database_and_structure(defining_database_strucure.dict_entity_columns, defining_database_strucure.dict_entity_foreign_keys, host, user, password, database)

    # machine names of the machines on the shopfloors
    shopfloors = ["shopfloor"]

    # for every machine in "machines_on_shopfloors" a new machine object is initialized and put into the database
    for shopfloor in shopfloors:
        machine_object = shopfloor_class.Shopfloor(host, user, password, database, shopfloor)
        machine_object.add_shopfloor()

    # machine names of the machines on the shopfloors
    machines_on_shopfloors = ["first_machine"]

    # for every machine in "machines_on_shopfloors" a new machine object is initialized and put into the database
    for machine in machines_on_shopfloors:
        machine_object = machine_class.Machine(host, user, password, database, machine, "shopfloor")
        machine_object.add_machine()

    # # sensor names of the sensors installed at the printer
    # sensors_installed = ["tempExt1", "tempExt2", "tempBed", "tempCham", "fanVoltage1", "fanVoltage2",
    # "multiplier", "Acc_X_Ext", "Acc_Y_Ext", "Acc_Z_Ext", "Acc_X_Bed", "Acc_Y_Bed",
    # "Acc_Z_Bed", "temperature_cham", "humidity", "pressure", "FilPositio", "FilFeed",
    # "FilDiaA", "FilDiaB", "CrossSec", "DoorState_closed"]

    # sensor names of the sensors installed at the printer
    sensors_installed = {
        "tempExt1" : {
            "description" : "Temperatur des 1. Extruders",
            "unit": "°C"
        },
        "tempExt2" : {
            "description" : "Temperatur des 2. Extruders",
            "unit" : "°C"
        },
        "tempBed" : {
            "description" : "Temperatur der Bauplatte",
            "unit" : "°C"
        },
        "tempCham" : {
            "description" : "Temperatur in der Kammer gemessen durch Drucker",
            "unit" : "°C"
        },
        "fanVoltage1" : {
            "description" : "Spannung des 1. Ventilators",
            "unit" : "V"
        },
        "fanVoltage2" : {
            "description" : "Spannung des 2. Ventilators",
            "unit" : "V"
        },
        "multiplier" : {
            "description" : "Multiplikator-Faktor",
            "unit" : "-"
        },
        "Acc_X_Ext" : {
            "description" : "Beschleunigung des Extruders in x-Richtung",
            "unit" : "mm/s²"
        },
        "Acc_Y_Ext" : {
            "description" : "Beschleunigung des Extruders in y-Richtung",
            "unit" : "mm/s²"
        },
        "Acc_Z_Ext" : {
            "description" : "Beschleunigung des Extruders in z-Richtung",
            "unit" : "mm/s²"
        },
        "Acc_X_Bed" : {
            "description" : "Beschleunigung der Bauplatte in x-Richtung",
            "unit" : "mm/s²"
        },
        "Acc_Y_Bed" : {
            "description" : "Beschleunigung der Bauplatte in y-Richtung",
            "unit" : "mm/s²"
        },
        "Acc_Z_Bed" : {
            "description" : "Beschleunigung der Bauplatte in z-Richtung",
            "unit" : "mm/s²"
        },
        "temperature_cham" : {
            "description" : "Temperatur in der Kammer (BME-280)",
            "unit" : "°C"
        },
        "humidity" : {
            "description" : "Luftfeuchtigkeit in der Kammer (BME-280)",
            "unit" : "%"
        },
        "pressure" : {
            "description" : "Luftdruck in der Kammer (BME-280)",
            "unit" : "hPa"
        },
        "FilPositio" : {
            "description" : "Position des Filaments",
            "unit" : "???"
        },
        "FilFeed" : {
            "description" : "Filamentvorschub???",
            "unit" : "mm/s???"
        },
        "FilDiaA" : {
            "description" : "Filamentdurchmesser A",
            "unit" : "mm"
        },
        "FilDiaB" : {
            "description" : "Filamentdurchmesser B",
            "unit" : "mm"
        },
        "CrossSec" : {
            "description" : "Querschnittsfläche des Filaments",
            "unit" : "mm²"
        },
        "DoorState_closed" : {
            "description" : "Türstatus",
            "unit" : "-"
        }
    }

    # for every sensor in "sensor_installed" a new sensor object is initialized and put into the database
    for sensor in sensors_installed:
        sensor_object = sensor_class.Sensor(host, user, password, database,
            sensor, sensors_installed[sensor]["description"],
            sensors_installed[sensor]["unit"], "first_machine")
        sensor_object.add_sensor()


create_database_with_predifined_entries(db_credentials.host, db_credentials.user, db_credentials.password, db_credentials.database_name)

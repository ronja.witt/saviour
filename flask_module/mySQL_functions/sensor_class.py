import mysql.connector
import pickle

class Sensor:
    """
    class object of a sensor in mySQL
    class provides basic functions to interact with sensors in a mySQL database:
        - init
        - connect
        - add_sensor
        - delete_sensor
    Input:
        - host          (z.B. "localhost")
        - user          (z.B. "root")
        - password      (z.B. "Patisserie3201!")
        - database      (z.B. "database_test2")
        - sensor_name   (z.B. "sensor1")
        - machine_name  (z.B. "first_machine")
    """


    def __init__(self, host, user, password, database, sensor_name, sensor_description, sensor_unit, machine_name) -> None:
        """
        assign basic information to the properties of the table-object
        """
        self.host = host
        self.user = user
        self.password = password
        self.database = database
        self.sensor_name = sensor_name
        self.sensor_description = sensor_description
        self.sensor_unit = sensor_unit
        self.machine_name = machine_name

    def connect(self):
        """
        connect to database and return cursor for further interactions with the database
        """
        mydb = mysql.connector.connect(
            host = self.host,
            user = self.user,
            password = self.password,
            database = self.database
        )
        mycursor = mydb.cursor(buffered=True)
        return mycursor, mydb

    def add_sensor(self):
        """
        adds a sensor in the mySQL database if it doesn't already exist
        """
        mycursor, mydb = self.connect()
        mycursor.execute("""SELECT id FROM sensor WHERE name = %s""", [self.sensor_name])
        result = mycursor.fetchall()
        if (result==[]):
            try:
                mycursor.execute("""insert into sensor (name,description,unit,machine_id)
                values(%s,%s,%s,(select machine.id from machine where name = %s))""",
                [self.sensor_name,self.sensor_description,self.sensor_unit,self.machine_name])
                mydb.commit()

            except mysql.connector.Error as err:
                print(err.msg)
        if mydb is not None and mydb.is_connected():
            mydb.close()

    def delete_sensor(self):
        """
        deletes a sensor from the mySQL database
        """
        mycursor, mydb = self.connect()
        mycursor.execute("""SELECT id FROM sensor WHERE name = %s""", [self.sensor_name])
        result = mycursor.fetchall()
        if (result!=[]):
            try:
                mycursor.execute("""delete from sensor where name = %s""",[self.sensor_name])
                mydb.commit()

            except mysql.connector.Error as err:
                print(err.msg)
        if mydb is not None and mydb.is_connected():
            mydb.close()


# sensor1 = Sensor("localhost", "root", "Patisserie3201!", "database_test2", "sensor1", "first_machine")
# sensor1.add_sensor()
# sensor1.delete_sensor()

# sensors_installed = ["tempExt1", "tempExt2", "tempBed", "tempCham", "fanVoltage1", "fanVoltage2",
# "multiplier", "Acc_X_Ext", "Acc_Y_Ext", "Acc_Z_Ext", "Acc_X_Bed", "Acc_Y_Bed",
# "Acc_Z_Bed", "temperature_cham", "humidity", "pressure", "FilPositio", "FilFeed",
# "FilDiaA", "FilDiaB", "CrossSec", "DoorState_closed"]

# for sensor in sensors_installed:
#     print(sensor)
#     sensor_object = Sensor("localhost", "root", "Patisserie3201!", "database_test3", sensor,"first_machine")
#     sensor_object.add_sensor()

import mysql.connector
import pickle

class Machine:
    """
    class object of a machine in mySQL
    class provides basic functions to interact with machines in a mySQL database:
        - init
        - connect
        - add_machine
        - delete_machine
    Input:
        - host          (z.B. "localhost")
        - user          (z.B. "root")
        - password      (z.B. "Patisserie3201!")
        - database      (z.B. "database_test2")
        - machine_name   (z.B. "first_machine")
        - shopfloor_name  (z.B. "shopfloor")
    """


    def __init__(self, host, user, password, database, machine_name, shopfloor_name) -> None:
        """
        assign basic information to the properties of the table-object
        """
        self.host = host
        self.user = user
        self.password = password
        self.database = database
        self.machine_name = machine_name
        self.shopfloor_name = shopfloor_name

    def connect(self):
        """
        connect to database and return cursor for further interactions with the database
        """
        mydb = mysql.connector.connect(
            host = self.host,
            user = self.user,
            password = self.password,
            database = self.database
        )
        mycursor = mydb.cursor(buffered=True)
        return mycursor, mydb

    def add_machine(self):
        """
        adds a machine in the mySQL database if it doesn't already exist
        """
        mycursor, mydb = self.connect()
        mycursor.execute("""SELECT id FROM machine WHERE name = %s""", [self.machine_name])
        result = mycursor.fetchall()
        if (result==[]):
            try:
                mycursor.execute("""insert into machine (name,shop_floor_id) values(%s,(select shop_floor.id from shop_floor where name = %s))""",[self.machine_name,self.shopfloor_name])
                mydb.commit()
            
            except mysql.connector.Error as err:
                print(err.msg)
        if mydb is not None and mydb.is_connected():
            mydb.close()

    def delete_machine(self):
        """
        deletes a machine from the mySQL database 
        """
        mycursor, mydb = self.connect()
        mycursor.execute("""SELECT id FROM machine WHERE name = %s""", [self.machine_name])
        result = mycursor.fetchall()
        if (result!=[]):
            try:
                mycursor.execute("""delete from machine where name = %s""",[self.machine_name])
                mydb.commit()
            
            except mysql.connector.Error as err:
                print(err.msg)
        if mydb is not None and mydb.is_connected():
            mydb.close()


# machine1 = Machine("localhost", "root", "Patisserie3201!", "database_test4", "first_machine", "shopfloor")
# machine1.add_machine()
# machine1.delete_machine()
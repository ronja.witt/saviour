import os, sys
sys.path.append(os.getcwd()+"")
import mysql.connector
import pickle
import db_credentials

class View:
    def __init__(self) -> None:
        """
        assign basic information to the properties of the table-object
        """
        self.host = db_credentials.host
        self.user = db_credentials.user
        self.password = db_credentials.password
        self.database = db_credentials.database_name


    def connect(self):
        """
        connect to database and return cursor for further interactions with the database
        """
        mydb = mysql.connector.connect(
            host = self.host,
            user = self.user,
            password = self.password,
            database = self.database
        )
        mycursor = mydb.cursor(buffered=True)
        return mycursor, mydb
    
    def create_view(self):
        mycursor, mydb = self.connect()
        mycursor.execute("""SELECT name FROM sensor WHERE id = 23""")
        result = mycursor.fetchone()[0]
        print(result)
        mycursor.execute("""CREATE OR REPLACE VIEW features AS SELECT sensor_readings.value AS %s From sensor_readings WHERE sensor_readings.sensor_id = %s""", [result, 23])
        mycursor.execute("""SELECT name FROM sensor WHERE id = 24""")
        result2 = mycursor.fetchone()[0]
        mycursor.execute("""ALTER VIEW features AS SELECT sensor_readings.value AS %s From sensor_readings WHERE sensor_readings.sensor_id = %s""", [result2, 24])
        data = [("bumber1", 23), ("number2", 24)]
        query1 = """CREATE PROCEDURE doiterate(p1 INT)
            BEGIN
            label1: LOOP
                SET p1 = p1 + 1;
                IF p1 < 10 THEN
                ITERATE label1;
                END IF;
                LEAVE label1;
            END LOOP label1;
            SET @x = p1;
            END;
        """
        mycursor.executemany("""ALTER VIEW features AS label1: LOOP SET p1 = p1 + 1; IF p1 > 3 THEN LEAVE label1; SELECT sensor_readings.value AS %s From sensor_readings WHERE sensor_readings.sensor_id = %s END LOOP label1""", data)

view = View()
view.create_view()

import os, sys
sys.path.append(os.getcwd()+"")
from ctypes import Structure
import mysql.connector
import flask_module.mySQL_functions.database_class as database_class
import flask_module.mySQL_functions.defining_database_strucure as defining_database_strucure
import flask_module.mySQL_functions.db_credentials as db_credentials

# create database
def create_database_and_structure(dict_entity_columns_input, dict_entity_foreign_keys_input, host, user, password, database_name):
    """
    this function creates the database and the structure of the database

    input:
        - dict_entity_columns_input: dictionary of the entities with their repective columns
        - dict_entity_foreign_keys_input: dictionary of the entities together with the entities to which the foreign keys point
        - database_name: name of the database
    """

    try:
        # create database
        database1 = database_class.Database(host, user, password, database_name)
        # database1.delete_database()
        database1.create_database()
        # create the structure of the database
        create_structure_of_database(dict_entity_columns_input, dict_entity_foreign_keys_input, database1)
    except mysql.connector.Error as err:
        print(err.msg)


def create_structure_of_database(dict_entity_columns_input, dict_entity_foreign_keys_input, database_object):
    """
    this function creates the structure of the database

    input:
        - dict_entity_columns_input: dictionary of the entities with their repective columns
        - dict_entity_foreign_keys_input: dictionary of the entities together with the entities to which the foreign keys point
    """

    # create tables with columns
    for entity in dict_entity_columns_input:
        entity_object = database_object.create_table(entity)
        entity_object.add_columns(dict_entity_columns_input[entity])

    # add foreign keys to the tables
    # this needs to be done after all the tables are created
    for entity in dict_entity_foreign_keys_input:
        entity_object = database_object.create_table(entity)
        entity_object.add_foreign_keys(dict_entity_foreign_keys_input[entity])

# create_database_and_structure(defining_database_strucure.dict_entity_columns, defining_database_strucure.dict_entity_foreign_keys,db_credentials.host, db_credentials.user, db_credentials.password, db_credentials.database_name)

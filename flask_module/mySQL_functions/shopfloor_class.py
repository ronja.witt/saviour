import mysql.connector
import pickle

class Shopfloor:
    """
    class object of a shopfloor in mySQL
    class provides basic functions to interact with shopfloor in a mySQL database:
        - init
        - connect
        - add_shopfloor
        - delete_shopfloor
    Input:
        - host          (z.B. "localhost")
        - user          (z.B. "root")
        - password      (z.B. "Patisserie3201!")
        - database      (z.B. "database_test2")
        - shopfloor_name  (z.B. "shopfloor")
    """


    def __init__(self, host, user, password, database, shopfloor_name) -> None:
        """
        assign basic information to the properties of the table-object
        """
        self.host = host
        self.user = user
        self.password = password
        self.database = database
        self.shopfloor_name = shopfloor_name

    def connect(self):
        """
        connect to database and return cursor for further interactions with the database
        """
        mydb = mysql.connector.connect(
            host = self.host,
            user = self.user,
            password = self.password,
            database = self.database
        )
        mycursor = mydb.cursor(buffered=True)
        return mycursor, mydb

    def add_shopfloor(self):
        """
        adds a shopfloor in the mySQL database if it doesn't already exist
        """
        mycursor, mydb = self.connect()
        mycursor.execute("""SELECT id FROM shop_floor WHERE name = %s""", [self.shopfloor_name])
        result = mycursor.fetchall()
        if (result==[]):
            try:
                mycursor.execute("""insert into shop_floor (name) values(%s)""",[self.shopfloor_name])
                mydb.commit()
            
            except mysql.connector.Error as err:
                print(err.msg)
        if mydb is not None and mydb.is_connected():
            mydb.close()

    def delete_shopfloor(self):
        """
        deletes a shopfloor from the mySQL database 
        """
        mycursor, mydb = self.connect()
        mycursor.execute("""SELECT id FROM shop_floor WHERE name = %s""", [self.shopfloor_name])
        result = mycursor.fetchall()
        if (result!=[]):
            try:
                mycursor.execute("""delete from shop_floor where name = %s""",[self.shopfloor_name])
                mydb.commit()
            
            except mysql.connector.Error as err:
                print(err.msg)
        if mydb is not None and mydb.is_connected():
            mydb.close()


# shopfloor = Shopfloor("localhost", "root", "Patisserie3201!", "database_test4", "shopfloor")
# shopfloor.add_shopfloor()
# shopfloor.delete_shopfloor()
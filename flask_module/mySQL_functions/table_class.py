import mysql.connector

class Table:
    """
    class object of a table in mySQL
    class provides basic functions to interact with tables in a mySQL database:
        - init
        - connect
        - create_table
        - delete_table
        - add_columns
        - delete_columns
        - add_foreign_key
        - delete_foreign_key
    Input:
        - host          (z.B. "localhost")
        - user          (z.B. "root")
        - password      (z.B. "Patisserie3201!")
        - database      (z.B. "test1")
        - table_name    (z.B. "test2")
    """


    def __init__(self, host, user, password, database, table_name) -> None:
        """
        assign basic information to the properties of the table-object
        """
        self.host = host
        self.user = user
        self.password = password
        self.database = database
        self.table_name = table_name

    def connect(self):
        """
        connect to database and return cursor for further interactions with the database
        """
        mydb = mysql.connector.connect(
            host = self.host,
            user = self.user,
            password = self.password,
            database = self.database
        )
        mycursor = mydb.cursor(buffered=True)
        return mycursor, mydb
    
    def create_table(self):
        """
        creates a table in the mySQL database
        all tables are created with an "id" attribute as primary-key already integrated
        """
        mycursor, mydb = self.connect()
        mycursor.execute("CREATE TABLE IF NOT EXISTS {} (id BIGINT AUTO_INCREMENT PRIMARY KEY)".format(self.table_name))
        if mydb is not None and mydb.is_connected():
            mydb.close()

    def delete_table(self):
        """
        deletes a table in the mySQL database 
        """
        mycursor, mydb = self.connect()
        mycursor.execute("DROP TABLE IF EXISTS {}".format(self.table_name))
        if mydb is not None and mydb.is_connected():
            mydb.close()

    def add_columns(self, dict_columnname_datatype):
        """
        adds columns to a table
        Input: dictionary of all column-names as key und their data-types as value
        """
        mycursor, mydb = self.connect()
        for key in dict_columnname_datatype:
            try:
                mycursor.execute("ALTER TABLE {} ADD COLUMN {} {}".format(self.table_name, key, dict_columnname_datatype[key]))
            except:
                self.delete_columns({key: dict_columnname_datatype[key]})
                mycursor.execute("ALTER TABLE {} ADD COLUMN {} {}".format(self.table_name, key, dict_columnname_datatype[key]))
                print("A column with the column name {} already exists. Therefore the previous column was deleted and the new column was added".format(key))
        if mydb is not None and mydb.is_connected():
            mydb.close()

    def delete_columns(self, dict_columnname_datatype):
        """
        deletes columns in a table
        Input: dictionary of all column-names as key und their data-types as value
        """
        mycursor, mydb = self.connect()
        for key in dict_columnname_datatype:
            try:
                mycursor.execute("ALTER TABLE {} DROP COLUMN {}".format(self.table_name, key))
            except:
                print("A column with the column name {} does not exist.".format(key))
        if mydb is not None and mydb.is_connected():
            mydb.close()

    def add_foreign_keys(self, list_table_names):
        """
        adds a foreign-key to a table
        Input:
            - list_table_names: list of table names for which to create the Foreign Keys
        """
        mycursor, mydb = self.connect()
        for table in list_table_names:
            try:
                self.add_columns({str(table) + "_id": "BIGINT"})
                mycursor.execute("ALTER TABLE {} ADD CONSTRAINT {} FOREIGN KEY ({}) REFERENCES {}(id);".format(self.table_name, str(self.table_name) + "_"+ str(table), str(table) + "_id", table))
            except:
                print("Foreign-key could not be assigned for {} {}".format(table, self.table_name))
        if mydb is not None and mydb.is_connected():
            mydb.close()

    def delete_foreign_keys(self, list_table_names):
        """
        deletes a foreign-key of a table
        to do that, we also have to drop the index, which is automatically created when a foreign-key is added
        Input:
            - list_table_names: list of table names for which to delete the Foreign Keys
        """
        mycursor, mydb = self.connect()
        for table in list_table_names:
            try:
                mycursor.execute("ALTER TABLE {} DROP FOREIGN KEY {};".format(self.table_name, str(self.table_name) + "_"+ str(table)))
                mycursor.execute("ALTER TABLE {} DROP INDEX {};".format(self.table_name, str(self.table_name) + "_"+ str(table)))
            except:
                print("Foreign-key could not be deleted")
        if mydb is not None and mydb.is_connected():
            mydb.close()

# # some examples on how to interact with the table-class
# table1 = Table("localhost", "root", "Patisserie3201!", "test1", "test1")
# # table1.create_table()
# # # table1.delete_table()
# # table1.add_columns({"class_room": VARCHAR(127)})
# # table1.delete_columns({"class_room1": VARCHAR(127)})
# table1.add_foreign_keys(["test2"])
# table1.delete_foreign_keys(["table2"])




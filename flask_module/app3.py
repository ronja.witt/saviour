import os, sys
sys.path.append(os.getcwd()+"")
import sys
import time
from datetime import datetime
import json
from datetime import timedelta
import pandas as pd
import pickle
from pickle import TRUE
from flask import (Flask, render_template, session, request, redirect, url_for, flash)
from threading import Thread
from flask_socketio import SocketIO
from flask_mqtt import Mqtt
from flask_mysqldb import MySQL
import MySQLdb.cursors
import mysql.connector as connector
from sqlalchemy import create_engine, text, exc
from mySQL_functions.creating_database_with_entries import *
import flask_module.mySQL_functions.db_credentials as db_credentials
import plotly
import plotly.express as px
import quality_model.feature_importance as feature_importance
import quality_model.R03_ExtractParameters as extract_parameters
import atexit
from apscheduler.schedulers.background import BackgroundScheduler
import paho.mqtt.client as mqtt_paho
from werkzeug.utils import secure_filename
from flask_module import mqtt_functions
import quality_model.feature_extraction



# for easy and fast access the following variables are initiated
# this makes the app much faster and enables better usability
dict_pipeline_percentile, graphJSON_dict_gesamt = mqtt_functions.preload_data()

# set up the web application
app = Flask(__name__)
app.config['SECRET_KEY'] = 'secret!'
app.config['MQTT_BROKER_URL'] = "127.0.0.1"
app.config['MQTT_BROKER_PORT'] = 1883
app.config['MQTT_KEEPALIVE'] = 10
app.config['MQTT_TLS_ENABLED'] = False
mqtt = Mqtt(app)
socket = SocketIO(app)
TOPIC = "Sensoren1"

# Change this to your secret key (can be anything, it's for extra protection)
app.secret_key = 'your secret key'

# configuration of database connection
app.config['MYSQL_HOST'] = db_credentials.host
app.config['MYSQL_USER'] = db_credentials.user
app.config['MYSQL_PASSWORD'] = db_credentials.password
app.config['MYSQL_DB'] = db_credentials.database_name
mysql = MySQL(app)

# configuration for uploading a file
app.config['UPLOAD_FOLDER'] = 'quality_model/html_files'





def connect():
    """
    connect to database and return cursor for further interactions with the database
    """
    db_connection_str = f'mysql+pymysql://{db_credentials.user}:{db_credentials.password}@{db_credentials.host}/{db_credentials.database_name}'
    db_connection = create_engine(db_connection_str, echo=False)
    return db_connection

columns = ['time', 'Acc_X_Ext','Acc_Y_Ext','Acc_Z_Ext','Acc_X_Bed','Acc_Y_Bed','Acc_Z_Bed','FilFeed', 'FilPositio', 'tempExt1', 'pressure',  'humidity', 'FilDiaA', 'FilDiaB', 'CrossSec', 'DoorState_closed']
df_all_features_current_process = pd.DataFrame(columns=columns)
def on_message(client, userdata, message):
    """
    adds MQTT-message to a pandas DataFrame
    Every 1000th message is saved into the database in "sensor_readings_process" to access it later in "historical data"

    """
    global df_all_features_current_process
    global process_step_id_global
    msg = str(message.payload.decode("utf-8"))

    data = dict(
        topic=message.topic,
        payload=message.payload.decode()
    )
    convertedDict = json.loads(data["payload"])
    df_all_features_current_process_last_row = pd.DataFrame({'time': [None], 'Acc_X_Ext': [None],'Acc_Y_Ext': [None],'Acc_Z_Ext': [None],'Acc_X_Bed': [None],'Acc_Y_Bed': [None],'Acc_Z_Bed':[None],'FilFeed': [None], 'FilPositio': [None] , 'tempExt1': [None], 'pressure':[None],  'humidity': [None], 'FilDiaA': [None], 'FilDiaB':[None], 'CrossSec': [None], 'DoorState_closed':[None]}) # columns=['time', 'Acc_X_Ext','Acc_Y_Ext','Acc_Z_Ext','humidity'], data = [None, None, None, None, None])
    convertedDict["time"] = str(convertedDict["Date"] + " " + convertedDict["Time"])
    convertedDict.pop("Time")
    convertedDict.pop("Date")
    for key in convertedDict:
        df_all_features_current_process_last_row[key][0] = convertedDict[key]
    df_all_features_current_process = pd.concat([df_all_features_current_process, df_all_features_current_process_last_row])
    # query to select the sensor_id
    get_sensor_id_of_sensor_query = """
    SELECT id FROM sensor WHERE name = %s
    """

    mydb = connector.connect(
            host=db_credentials.host,
            user=db_credentials.user,
            password=db_credentials.password,
            database=db_credentials.database_name
        )

    cursor = mydb.cursor()
    # every nth row will be saved in the DB for historical data
    if process_step_id_global != None:
        if len(df_all_features_current_process)%100 == 0:
            for sensor_name in df_all_features_current_process_last_row.columns:
                if sensor_name != 'time':
                    # get sensor_id
                    get_sensor_id_of_sensor_query = """
                    SELECT id FROM sensor WHERE name = %s
                    """
                    cursor.execute(get_sensor_id_of_sensor_query, (sensor_name,))
                    sensor_id = cursor.fetchall()[0][0]
                    # get value
                    value = df_all_features_current_process_last_row[sensor_name][0]
                    # save data in DB
                    # save features from current process_step
                    set_feature_data_from_current_process_query = """
                    insert into sensor_readings_process (value,sensor_id, process_step_id, timestamp)
                    values(%s,%s,%s,%s)
                    """
                    time_stamp = df_all_features_current_process_last_row["time"][0]
                    time_stamp = datetime.strptime(time_stamp,  '%Y-%m-%d %H:%M:%S')
                    cursor.execute(set_feature_data_from_current_process_query, (value, sensor_id, process_step_id_global, time_stamp))
                    mydb.commit()


def listen_to_mqtt_message():
    """
    here a connection to the MQTT Broker is made and messages are taken
    """
    client = mqtt_paho.Client()
    client.on_connect = mqtt_functions.on_connect
    client.on_message = on_message

    client.connect(app.config['MQTT_BROKER_URL'], app.config['MQTT_BROKER_PORT'])
    client.loop_forever()

mqtt_thr = Thread(target=listen_to_mqtt_message)
mqtt_thr.start()


process_step_id_global = None
percentile_step_list = []
my_threads = []
df_of_all_features_saved = pd.DataFrame()
# mqtt_functions.start_thread_get_data(process_step_id_global, percentile_step_list)
def start_thread_get_data(percentile = None):
    """
    starts a thread, that preprocesses the data of the current process
    and extracts features for prediction
    """
    global process_step_id_global
    # global my_threads
    global percentile_step_list

    print("//////////////////percentile_step_list//////////////////")
    print(percentile_step_list)

    if process_step_id_global != None:
        mydb = connector.connect(
            host=db_credentials.host,
            user=db_credentials.user,
            password=db_credentials.password,
            database=db_credentials.database_name
        )
        cursor = mydb.cursor()

        get_process_duration = """
        SELECT expected_duration FROM process_step_specification WHERE id = (SELECT process_step_specification_id FROM process_step WHERE id = %s)
        """
        cursor.execute(get_process_duration, (process_step_id_global,))
        expected_duration = cursor.fetchone()[0]

        get_process_start = """
        SELECT started_time FROM process_step WHERE id = %s
        """
        cursor.execute(get_process_start, (process_step_id_global,))
        started_time = cursor.fetchone()[0]
        expected_end_time = started_time + timedelta(seconds=expected_duration)
        current_time = datetime.now()
        percentile_difference = (current_time - started_time)/(expected_end_time - started_time) * 100

        for step in range(0,110,10):
            if percentile_difference >= step and step not in percentile_step_list:
                percentile_step_list.append(step)
                thr2 = Thread(target=get_and_preprocess_data_from_current_process(percentile = step))
                thr2.start()
    
def get_and_preprocess_data_from_current_process(percentile = None):
    global df_of_all_features_saved
    global df_all_features_current_process
    global percentile_step_list
    """
    preprocesses data
    extracts features
    saves data if procentile not None
    """
    print("this is a thread")
    print(time.strftime("%A, %d. %B %Y %I:%M:%S %p"))

    # X, y = feature_importance.get_Xreal_and_yreal_tensor("Geometric_accuracy", 100)
    # df_of_all_features_saved_1 = X

    df_of_all_features = quality_model.feature_extraction.extract_features(df_all_features_current_process.copy())
    print(df_of_all_features)
    print("features extracted")

    # save features in db if percentile not None
    mydb = connector.connect(
        host=db_credentials.host,
        user=db_credentials.user,
        password=db_credentials.password,
        database=db_credentials.database_name
    )
    cursor = mydb.cursor()

    cursor.execute("SELECT percentage FROM sensor_readings WHERE percentage IS Null and process_step_id = %s", (process_step_id_global,))
    percentage_null = cursor.fetchall()
    print(percentage_null)

    cursor.execute("SELECT value, sensor_id FROM sensor_readings WHERE percentage IS Null and process_step_id = 1") # TODO: ersätzen durch die html-files
    values_html = cursor.fetchall()
    print(values_html)
    df_of_all_features_2 = pd.DataFrame()
    if percentage_null == []:
        for value in values_html:
            param_value = value[0]
            sensor_id = value[1]
            # if sensor_id is a Quality Parameter then leave it
            if sensor_id in [23, 24, 25, 26]:
                pass
            else:
                cursor.execute("SELECT name FROM sensor WHERE id = %s", (sensor_id,))
                sensor_name = cursor.fetchall()[0][0]
                df_of_all_features_2[sensor_name]= [param_value,]

    df_of_all_features_3 = pd.concat([df_of_all_features.iloc[:, 0:110], df_of_all_features.iloc[:, 120:]], axis=1)
    df_of_all_features_final = pd.concat([df_of_all_features_2, df_of_all_features_3], axis=1)
    df_of_all_features_saved = df_of_all_features_final

    print("dataframe saved")





scheduler = BackgroundScheduler()
scheduler.add_job(func=start_thread_get_data, trigger="interval", seconds=60)
scheduler.start()

# Shut down the scheduler when exiting the app
atexit.register(lambda: scheduler.shutdown())


@app.route('/', methods=['GET', 'POST'])
def index():
    """Show start page."""
    return render_template('start_page.html')


@app.route('/current-process', methods=['GET', 'POST'])
def current_process():
    """
    Provides the logic for "current printing process".
        - if the button "new_process_button_two" is pressed and it had the value "on"
            --> the user is redirected to "form_static_data"
        - if the button "new_process_button" is pressed and it had the value "stopp_process"
            --> the endtime of the process is saved in the database
            --> the current session['process_step_id'] is ended
        - Further, get list of all sensors to display as diagramms.
    """
    global process_step_id_global
    if request.method == 'POST' and 'new_process_button_two' in request.form:
        if request.form['new_process_button_two'] == 'on':
            return redirect('/form_static_data')
    if request.method == 'POST' and 'new_process_button' in request.form:
        if request.form['new_process_button'] == 'stopp_process':
            cursor = mysql.connection.cursor(MySQLdb.cursors.DictCursor)
            insert_query = """
            Update process_step set ended_time = %s where id = %s
            """
            try:
                cursor.execute(insert_query, (datetime.now(),
                               session.get("process_step_id")))
                mysql.connection.commit()
                cursor.close()
                session.pop('process_step_id', None)
                process_step_id = "not_yet_given"
                process_step_id_global = None
            except:
                print("this did not work")

    if session.get("process_step_id") == None:
        process_step_id = "not_yet_given"
        process_step_time = "not_yet_given"
        process_step_id_global = None
    else:
        process_step_id = session['process_step_id']
        process_step_time = session['process_step_time']
        process_step_id_global = session['process_step_id']

    # limit the list of sensors to 10, can be changed, if this works
    stmt_list_of_sensors = "SELECT `id`, `name`, `description`, `unit` FROM `sensor` LIMIT 10"
    with connect().connect() as conn:
        list_of_sensors = conn.execute(text(stmt_list_of_sensors)).all()
    list_of_sensors_json = pd.DataFrame(list_of_sensors).to_json()

    return render_template('index20.html',
                           process_step_id=process_step_id,
                           process_step_time=process_step_time,
                           list_of_sensors=list_of_sensors,
                           list_of_sensors_json=json.dumps(
                               list_of_sensors_json)
                           )


@app.route('/updatecharts', methods=['POST', 'GET'])
def update_charts():
    """
    Provide the last datapoint for the current process for each sensor.
    This function is used in order to update the charts in "current printing process"
    """
    global df_of_all_features_saved
    global df_all_features_current_process
    if request.method == 'GET':
        try:
            # limit the list of sensors to 10, can be changed, if this works
            stmt_list_of_sensors = "SELECT `id`, `name`, `description`, `unit` FROM `sensor` LIMIT 10"
            with connect().connect() as conn:
                list_of_sensors = conn.execute(
                    text(stmt_list_of_sensors)).all()

            get_sensor_id_of_sensor_query = """
            SELECT id FROM sensor WHERE name = %s
            """

            cursor = mysql.connection.cursor()

            get_data_from_current_process_query = df_all_features_current_process.tail(1)
            dataframe_sensor_readings_all = pd.DataFrame(columns=["value", "timestamp", "sensor_id"])
            for sensor in get_data_from_current_process_query.columns:
                if sensor != "time":
                    cursor.execute(get_sensor_id_of_sensor_query, (sensor,))
                    sensor_id = cursor.fetchall()[0]
                    dataframe_sensor_readings_single = pd.DataFrame()
                    dataframe_sensor_readings_single["value"] = get_data_from_current_process_query[sensor]
                    dataframe_sensor_readings_single["timestamp"] = get_data_from_current_process_query["time"][0]
                    dataframe_sensor_readings_single["timestamp"][0]= datetime.strptime(dataframe_sensor_readings_single["timestamp"][0],  '%Y-%m-%d %H:%M:%S')
                    dataframe_sensor_readings_single["sensor_id"] = sensor_id
                    dataframe_sensor_readings_all = pd.concat([dataframe_sensor_readings_all, dataframe_sensor_readings_single], ignore_index=True)
                    
            json_sensor_readings = dataframe_sensor_readings_all.to_json(
                orient='columns')

            return json_sensor_readings
        except Exception as e:
            return str(e), 400

percentile_step = 0
import numpy as np
@app.route('/quality-prediction', methods=['GET', 'POST'])
def influential_factors():
    """
    Shows the predicted quality-parameters of the currently printed part.
    For the prediction models are chosen, which fit the current printing status (percentile)
    """
    global df_of_all_features_saved
    global process_step_id_global
    global percentile_step
    prediction_ok_Geometrische_Genauigkeit = 0
    prediction_nok_Geometrische_Genauigkeit = 0
    prediction_Zugfestigkeit = None
    prediction_Bruchdehnung = None
    prediction_Schlagzaehigkeit = None
    percentile_difference = 0
    if session.get("process_step_id") == None:
        process_step_id = "not_yet_given"
        process_step_time = "not_yet_given"
        process_step_id_global = None
        prediction_ready = ""
    else:
        # calculate percentile of past duration time
        cursor = mysql.connection.cursor(MySQLdb.cursors.DictCursor)
        process_step_id = session.get("process_step_id")
        process_step_id_global = session.get("process_step_id")
        percentile_difference = round(mqtt_functions.calculate_percentile_of_process_duration(cursor, process_step_id), 2)

        # select the pipeline for the current duration time
        pipeline = {}
        for step in range(10,110,10):
            if percentile_difference <= step:
                pipeline["Tensile_strength"] = dict_pipeline_percentile[step]["Tensile_strength"]
                pipeline["Elongation_at_break"] = dict_pipeline_percentile[step]["Elongation_at_break"]
                pipeline["Impact_strength"] = dict_pipeline_percentile[step]["Impact_strength"]
                pipeline["Geometric_accuracy"] = dict_pipeline_percentile[step]["Geometric_accuracy"]
                break
            elif percentile_difference > 100:
                pipeline["Tensile_strength"] = dict_pipeline_percentile[100]["Tensile_strength"]
                pipeline["Elongation_at_break"] = dict_pipeline_percentile[100]["Elongation_at_break"]
                pipeline["Impact_strength"] = dict_pipeline_percentile[100]["Impact_strength"]
                pipeline["Geometric_accuracy"] = dict_pipeline_percentile[100]["Geometric_accuracy"]
                break

        # get data from current process_step
        get_data_from_current_process_query = """
        SELECT value, sensor_id FROM sensor_readings WHERE process_step_id = %(process_step_id)s ORDER BY timestamp DESC
        """

        # save features from current process_step
        set_feature_data_from_current_process_query = """
        insert into sensor_readings (value,sensor_id, process_step_id, timestamp)
        values(%s,%s)
        """
        X = df_of_all_features_saved

        if X.empty:
            prediction_ready = "Es gibt noch nicht genügend Daten für eine Vorhersage"
            print("There is not enough data for a prediction yet")
        else:
            prediction_ready = ""
            # best estimator for the current time-period for Geometrische Genauigkeit
            prediction_Geometrische_Genauigkeit = pipeline["Geometric_accuracy"].predict_proba(X.head(1))
            prediction_ok_Geometrische_Genauigkeit = prediction_Geometrische_Genauigkeit[0][1]*100
            prediction_nok_Geometrische_Genauigkeit = prediction_Geometrische_Genauigkeit[0][0]*100

            # best estimator for the current time-period for Zugfestigkeit
            prediction_Zugfestigkeit = pipeline["Tensile_strength"].predict(X.head(1))[0]

            # best estimator for the current time-period for Bruchdehnung
            prediction_Bruchdehnung = pipeline["Elongation_at_break"].predict(X.head(1))[0]

            # best estimator for the current time-period for Schlagzähigkeit
            prediction_Schlagzaehigkeit = pipeline["Impact_strength"].predict(X.head(1))[0]


        process_step_id = session['process_step_id']
        process_step_time = session['process_step_time']
        process_step_id_global = session['process_step_id']

        print(type(process_step_id))
        print(type(prediction_ready))
        print(type(process_step_time))
        print(type(prediction_ok_Geometrische_Genauigkeit))
        print(type(prediction_nok_Geometrische_Genauigkeit))
        prediction_Zugfestigkeit = np.float64(prediction_Zugfestigkeit)
        print(type(prediction_Zugfestigkeit))
        print(type(prediction_Bruchdehnung))
        print(type(prediction_Schlagzaehigkeit))
        print(type(percentile_difference))



    return render_template("quality-prediction.html", process_step_id = process_step_id, prediction_ready = prediction_ready,\
    process_step_time = process_step_time, prediction_ok_Geometrische_Genauigkeit = prediction_ok_Geometrische_Genauigkeit, prediction_nok_Geometrische_Genauigkeit = prediction_nok_Geometrische_Genauigkeit, \
    prediction_Zugfestigkeit=prediction_Zugfestigkeit, prediction_Bruchdehnung=prediction_Bruchdehnung, prediction_Schlagzaehigkeit=prediction_Schlagzaehigkeit, percentile_difference=percentile_difference)


@app.route('/machine-learning-model', methods=['GET', 'POST'])
def machine_learning_model():
    """
    depending on which button has been pressed, a different graphic describing the models will be displaid
    """
    try:
        description = None
        try:
            QParam = session['QParam']
            ML_model_diagramm = session['ML_model_diagramm']
        except KeyError:
            QParam = "Zugfestigkeit [N]"
            ML_model_diagramm = "model"
        graphJSON_dict_gesamt["Zugfestigkeit [N]"]
        graph = graphJSON_dict_gesamt[QParam][ML_model_diagramm]
        graph_name = QParam + ": " + ML_model_diagramm
    
        if ML_model_diagramm == "model":
            description = "R² ist das Bestimmtheitsmaß, ein Maß dafür, wie gut die Daten durch das angepasste Modell erklärt werden. \
                Der bestmögliche R²-Wert ist 100%. Das bedeutet, dass 100% \
                    der Varianz durch das Modell erklärt werden können. R² vergleicht \
                    die Anpassung des gewählten Modells mit der einer horizontalen Geraden (der Nullhypothese). Wenn das gewählte Modell schlechter \
                        abschneidet als eine horizontale Linie, dann ist R² negativ."
        elif ML_model_diagramm == "PFI Importance":
            description = "PFI ist eine Technik der Modellprüfung. Sie ist besonders nützlich für nicht lineare oder undurchsichtige Modelle. \
                Die Bedeutung von Permutationsmerkmalen ist definiert als die Abnahme der Modellqualität, wenn ein einzelner Merkmalswert \
                    zufällig vertauscht wird. Diese Technik bricht die Beziehung zwischen dem Merkmal und dem Ziel auf, so dass die Abnahme der Modellbewertung \
                        einen Hinweis darauf gibt, wie sehr das Modell von dem Merkmal abhängt. Diese Technik hat den Vorteil, dass sie modellunabhängig \
                            ist und viele Male mit verschiedenen Permutationen des Merkmals berechnet werden kann."
        elif ML_model_diagramm == "PDP Importance":
            description = "ICE-Diagramme (Individual Conditional Expectation) zeigen eine Linie pro Instanz, die zeigt, wie sich die Vorhersage der Instanz \
                ändert, wenn sich ein Merkmal ändert."

    except:
        print("there are no plots in the pickle-file")
    
    try:
        if status >= 0:
            return render_template("machine_learning_model_progress.html", graph=graph, graph_name=graph_name, description=description)
    except NameError:
        # get last training time from database
        t = "SELECT MAX(`last_training`) FROM `ml_training`"
        with connect().connect() as conn:
            last_training_time = conn.execute(text(t)).all()
        return render_template("machine_learning_model.html", graph=graph, graph_name=graph_name, description=description, last_training_time=last_training_time[0][0])



@app.route('/machine-learning-model/show_diagramm/<QParam>/<ML_model_diagramm>')
def show_ml_model_diagramm(QParam, ML_model_diagramm):
    session['QParam'] = str(QParam)
    session['ML_model_diagramm'] = str(ML_model_diagramm)
    return redirect(url_for('machine_learning_model'))


@app.route("/train_model_calculate_results")
def start_model_training():
    """
    This function starts a thread for training of the machine learning
    model, so that training happens in background, because this takes
    a while.
    """
    global status
    status = 0
    thr = Thread(target=train_ml_model)
    thr.start()
    return redirect(url_for('machine_learning_model'))


def train_ml_model():
    """
    Function for training of machine learning model.
    Global status variable is used for progress bar.
    """
    print("Training started.")
    global status
    status = 0
    
    try:
        fig_dict = feature_importance.training_the_models_all_percentiles()
        print("Training finished.")
        status = 100
        time_last_full_training = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
        print(time_last_full_training)
        t = f"INSERT INTO `ml_training` (`last_training`) VALUES ('{time_last_full_training}');"
        with connect().connect() as conn:
            conn.execute(text(t))
    except Exception as e:
        print("An error occured during model training.")
        print("Please refere to the following error message:")
        print(e)
    del status


@app.route('/status', methods=['GET'])
def getStatus():
    """
    Function to get status of machine learning model training.
    Called from java script in "machine_learning_model_progress.html".
    This function is a bit messy, as there are status infos in
    several places.
    """
    global status
    global curr_status
    try:
        if status >= 0:
            try:
                if status < feature_importance.status and status < 100:
                    try:
                        if feature_importance.status > 0 and feature_importance.status < 1:
                            curr_status = round(100 * feature_importance.status)
                        else:
                            curr_status = feature_importance.status
                    except AttributeError:
                        curr_status = feature_importance.status
                else:
                    curr_status = status
            except AttributeError as e:
                print(e)
                try:
                    print(curr_status)
                except UnboundLocalError:
                    curr_status = status
            statusList = {'status':curr_status}
            print(f"Der Status ist jetzt {curr_status}")
            return json.dumps(statusList)
    except NameError:
        statusList = {'status':100}
        return json.dumps(statusList)


@app.route('/historical data')
def historical_data():
    """
    Shows the historical data page.
    Depending on which button was pressed, different data tables are
    shown. The user can click on a row in the table to get a diagramm
    of the data. The process step for which the data are shown can be
    choosen by the user over a drop down menu.
    """
    # Check if any buttons were pressed (if session cookies are
    # availiable). Else, empty page is shown.
    try:
        data_title = session['data_title']
        stmt_column_names = session['stmt_column_names']
        stmt_data = session['stmt_data']
    except KeyError:
        return render_template("historical_data_empty.html")
    # get column_names and data for table,
    # if a view is used, check if view exists or create it
    with connect().connect() as conn:
        try:
            column_names = conn.execute(text(stmt_column_names))
            data = conn.execute(text(stmt_data))
        except exc.SQLAlchemyError:
            stmt_view_creation = session['stmt_view_creation']
            conn.execute(text(stmt_view_creation))
            column_names = conn.execute(text(stmt_column_names))
            data = conn.execute(text(stmt_data))
        try:
            download_file = pd.read_sql(
                "SELECT * FROM sensor_readings WHERE process_step_id = %(process_step_id)s", 
                conn, 
                params={"process_step_id": session.get("process_step_id")}
            )
            download_file.to_csv("flask_module/static/assets/download_files/test_data.csv")
        except:
            print("The file could not be downloaded")
        try:
            diagramms_dict = session['diagramms_dict']
        except KeyError:
            return render_template("historical_data_2.html",
                                    data_title=data_title,
                                    column_names=column_names,
                                    data=data
                                    )
        if diagramms_dict != {}:
            # make a list of all process step ids present in database
            t = "SELECT DISTINCT `id` FROM `process_step`"
            process_step_ids = conn.execute(text(t)).all()
            process_step_id_list = []
            for i in range(len(process_step_ids)):
                process_step_id_list.append(process_step_ids[i][0])
            # check if process step data exists in database
            if process_step_id_list != []:
                # For every sensor in dictionairy of diagramms,
                # get the sensor id, name, description and unit
                # store collected data in diagramms_dict.
                # Further, create a dataframe with sensor ids.
                sensor_ids = pd.DataFrame()
                for sensor in diagramms_dict:                    
                    t = f"SELECT `id`, `name`, `description`, `unit` FROM `sensor` WHERE `name` IN ('{sensor}')"
                    sensor_data = conn.execute(text(t)).all()
                    diagramms_dict[sensor].update(
                        {"id": sensor_data[0][0], "description": sensor_data[0][2], "unit": sensor_data[0][3]})
                    sensor_ids = pd.concat(
                        [sensor_ids, pd.read_sql(t, conn)])
                sensor_ids_json = pd.DataFrame(
                    sensor_ids).to_json(orient='records')
                # collecting data for chart in pandas dataframe and
                # converting to json
                chart_data = pd.DataFrame()
                for item in diagramms_dict:
                    item_id = diagramms_dict[item]["id"]
                    item_process_step_id = diagramms_dict[item]["process_step_id"]
                    if item_process_step_id == None:
                        item_process_step_id = max(process_step_id_list)
                        diagramms_dict[item].update({"process_step_id": item_process_step_id})
                    t = f"SELECT `sensor_id`, `timestamp`, `value` FROM `sensor_readings_process` WHERE (`sensor_id` IN ({item_id}) AND `process_step_id` IN ({item_process_step_id}))"
                    try:
                        chart_data = pd.concat(
                            [chart_data, pd.read_sql(t, conn)])
                    except exc.SQLAlchemyError as e:
                        print(f"This didn't go so well... (SQLAlchemyError):\n{e}")
                    chart_data_json = chart_data.to_json(orient='records')
                return render_template("historical_data_with_charts.html",
                                        data_title=data_title,
                                        column_names=column_names,
                                        data=data,
                                        process_step_id_list=process_step_id_list,
                                        diagramms_dict=diagramms_dict,
                                        sensor_ids=json.dumps(
                                            sensor_ids_json),
                                        chart_data=json.dumps(
                                            chart_data_json)
                                        )
            else:
                flash("Es gibt aktuell keine Daten in der Datenbank.", 'info')
                return render_template("historical_data_2.html",
                                    data_title=data_title,
                                    column_names=column_names,
                                    data=data
                                    )
        else:
            return render_template("historical_data_2.html",
                                    data_title=data_title,
                                    column_names=column_names,
                                    data=data
                                    )

@app.route('/historical_data/sensors')
def historical_data_sensors():
    """Set session variables needed when first button is pressed."""
    session['data_title'] = "Daten von Sensoren"
    session['stmt_column_names'] = "DESCRIBE sensor"
    session['stmt_data'] = "SELECT * FROM sensor"
    session.pop('stmt_view_creation', None)
    return redirect(url_for('historical_data'))


@app.route('/historical_data/quality')
def historical_data_quality():
    """
    Set session variables needed when third button is pressed.

    Currently this button acts as a placeholder and
    returns the empty starting page for historical data.
    """
    session.pop('data_title', None)
    session.pop('stmt_column_names', None)
    session.pop('stmt_data', None)
    session.pop('stmt_view_creation', None)
    session.pop('diagramms_list', None)
    return redirect(url_for('historical_data'))


@app.route('/historical data/clean')
def historical_data_clean():
    """
    Clean session cookies created by pressing a button on historical
    data page.

    Used for developement purpose to clean all session variables when
    page crashed.
    """
    session.pop('data_title', None)
    session.pop('stmt_column_names', None)
    session.pop('stmt_data', None)
    session.pop('stmt_view_creation', None)
    session.pop('diagramms_list', None)
    session.pop('diagramms_dict', None)
    return redirect(url_for('historical_data'))


@app.route('/add_diagramm/<sensor_name>/')
def add_data_chart(sensor_name):
    """
    Add data chart to list of data charts shown at historical data
    page.
    
    When a line in the table at historical data is clicked on, a
    diagramm appears on the right side.
    """
    # get maximal process_step_id
    t = "SELECT MAX(process_step_id) FROM `sensor_readings`"
    with connect().connect() as conn:
        max_id = conn.execute(text(t)).all()[0][0]
    # check if diagramm already exists and add diagramm to diagramms_dict
    if 'diagramms_dict' in session:
        diagramms_dict = session['diagramms_dict']
        if sensor_name not in diagramms_dict:
            diagramms_dict.update({sensor_name: {"process_step_id": max_id}})
        else:
            diagramms_dict.pop(sensor_name)
            diagramms_dict.update({sensor_name: {"process_step_id": max_id}})
        session['diagramms_dict'] = diagramms_dict
    else:
        diagramms_dict = {sensor_name: {"process_step_id": max_id}}
        session['diagramms_dict'] = diagramms_dict
    return redirect(url_for('historical_data'))


@app.route('/change_process_step/<sensor_name>/id=<id>/')
def change_process_step_id(sensor_name, id):
    """Change process step id for one specified data chart at historical data page."""
    diagramms_dict = session['diagramms_dict']
    diagramms_dict.pop(sensor_name)
    diagramms_dict.update({sensor_name: {"process_step_id": id}})
    session['diagramms_dict'] = diagramms_dict
    return redirect(url_for('historical_data'))


@app.route('/remove_diagramm/<sensor_name>/')
def remove_data_chart(sensor_name):
    """Remove one specified diagramm from historical data page."""
    if 'diagramms_dict' in session:
        diagramms_dict = session['diagramms_dict']
        diagramms_dict.pop(sensor_name)
        session['diagramms_dict'] = diagramms_dict
    return redirect(url_for('historical_data'))


@app.route('/info/<info_name>/')
def show_information_pages(info_name):
    """
    Show information pages. All URLs that have '/info/' in front will use this
    function.
    """
    if os.path.exists(os.getcwd()+f"\\flask_module\\templates\\info_{info_name}.html"):
        return render_template(f"info_{info_name}.html")
    else:
        return redirect(url_for("index"))

@app.route('/form_static_data', methods=['GET', 'POST'])
def form_static_data():
    """
    Adds static data entered by the user to the database.
    Form is called when starting a new process in /current-process
    """
    cursor = mysql.connection.cursor()
    for f in os.listdir(os.path.join(app.config['UPLOAD_FOLDER'])):
        os.remove(os.path.join(app.config['UPLOAD_FOLDER'], f))

    if request.method == 'POST' and 'vorname-form' in request.form and 'nachname-form' in request.form:
        # Create variables for easy access
        vorname = request.form['vorname-form']
        nachname = request.form['nachname-form']

        # Check if account exists using MySQL
        cursor = mysql.connection.cursor(MySQLdb.cursors.DictCursor)

        search_query = """
        SELECT id FROM operator WHERE first_name = %s AND last_name = %s
        """

        insert_query = """
        insert into operator (first_name,last_name)
        values(%s,%s)
        """

        cursor.execute(search_query, (vorname, nachname))
        search = cursor.fetchall()
        if search == ():
            cursor.execute(insert_query, (vorname, nachname))
            mysql.connection.commit()

    if request.method == 'POST' and 'bauteilname-form' in request.form and 'duration-form' in request.form:
        # Create variables for easy access
        bauteilname = request.form['bauteilname-form']
        duration = request.form['duration-form']
        hours = int(duration.split(":")[0])
        minutes = int(duration.split(":")[1])
        time_in_seconds = hours * 60 * 60 + minutes * 60

        cursor = mysql.connection.cursor(MySQLdb.cursors.DictCursor)

        search_query = """
        SELECT id FROM process_step_specification WHERE component_name = %s AND expected_duration = %s
        """

        insert_query = """
        insert into process_step_specification (component_name,expected_duration)
        values(%s,%s)
        """

        cursor.execute(search_query, [bauteilname, int(time_in_seconds)])
        search = cursor.fetchall()
        if search == ():
            cursor.execute(insert_query, [bauteilname, int(time_in_seconds)])
            mysql.connection.commit()

    if request.method == 'POST' and 'filamenttyp-form' in request.form and 'hersteller-form' in request.form and 'diameter-form' in request.form and 'offnungsdatum-form' in request.form:
        # Create variables for easy access
        filamenttyp = request.form['filamenttyp-form']
        hersteller = request.form['hersteller-form']
        diameter = request.form['diameter-form']
        offnungsdatum = request.form['offnungsdatum-form']
        
        cursor = mysql.connection.cursor(MySQLdb.cursors.DictCursor)

        search_query = """
        SELECT id FROM pre_product WHERE material = %s AND manufacturer = %s AND opening_date = %s AND CAST(diameter AS DECIMAL) = CAST( %s AS DECIMAL)
        """

        insert_query = """
        insert into pre_product (material,manufacturer,opening_date, diameter)
        values(%s,%s,%s,%s)
        """

        cursor.execute(search_query, (filamenttyp,
                       hersteller, offnungsdatum, diameter))
        search = cursor.fetchall()
        if search == ():
            cursor.execute(insert_query, (filamenttyp,
                           hersteller, offnungsdatum, diameter))
            mysql.connection.commit()

    if request.method == 'POST' and 'druckername-form' in request.form and 'druckerhersteller-form' in request.form and 'shopfloor-form' in request.form:
        # Create variables for easy access
        druckername = request.form['druckername-form']
        hersteller = request.form['druckerhersteller-form']
        shopfloor = request.form['shopfloor-form']

        cursor = mysql.connection.cursor(MySQLdb.cursors.DictCursor)

        search_query = """
        SELECT id FROM machine WHERE name = %s AND manufacturer = %s AND shop_floor_id = %s
        """

        insert_query = """
        insert into machine (name,manufacturer,shop_floor_id)
        values(%s,%s,%s)
        """

        cursor.execute(search_query, (druckername, hersteller, shopfloor))
        search = cursor.fetchall()
        if search == ():
            cursor.execute(insert_query, (druckername, hersteller, shopfloor))
            mysql.connection.commit()


    if request.method == 'POST' and 'file' in request.files:
        if 'file' not in request.files:
            flash("No file part", 'info')

        file = request.files['file']
        if file:
            for f in os.listdir(os.path.join(app.config['UPLOAD_FOLDER'])):
                os.remove(os.path.join(app.config['UPLOAD_FOLDER'], f))
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
        if file.filename == '':
            flash("No selected file", 'info')


        if file.filename != '':
            if 'operator_selection-form' in request.form and 'setting_selection-form' in request.form and 'machine_selection-form' in request.form and 'filament_selection-form' in request.form:
                operator_selection = request.form['operator_selection-form']
                setting_selection = request.form['setting_selection-form']
                machine_selection = request.form['machine_selection-form']
                filament_selection = request.form['filament_selection-form']

                cursor = mysql.connection.cursor(MySQLdb.cursors.DictCursor)

                insert_query = """
                insert into process_step (started_time, pre_product_id,operator_id,process_step_specification_id, machine_id)
                values(%s,%s,%s,%s,%s)
                """

                search_query = """
                SELECT id, ended_time FROM process_step ORDER BY id DESC LIMIT 1
                """

                cursor.execute(search_query)  # setting_selection, machine_selection))
                search = cursor.fetchone()
                if search == None or search["ended_time"] != None:
                    try:
                        cursor.execute(insert_query, (datetime.now(
                        ), filament_selection, operator_selection, setting_selection, machine_selection))
                        cursor.execute(search_query)
                        search = cursor.fetchone()
                        mysql.connection.commit()
                    except:
                        print("this did not work")
                session['process_step_id'] = search["id"]
                session['process_step_time'] = datetime.utcnow()

                # extract information from html-file
                file_path = os.path.join(app.config['UPLOAD_FOLDER'], os.listdir(app.config['UPLOAD_FOLDER'])[0])
                extract_parameters.add_relevant_features_as_sensors_and_sensorreadings(session['process_step_id'], file_path)

                return redirect('/current-process')

    dataframe_operator = pd.read_sql(
        """SELECT id, first_name, last_name FROM operator""", con=connect())
    dataframe_settings = pd.read_sql(
        """SELECT id, component_name, expected_duration FROM process_step_specification""", con=connect())
    dataframe_machines = pd.read_sql(
        """SELECT id, name, manufacturer, shop_floor_id FROM machine""", con=connect())
    dataframe_filament = pd.read_sql(
        """SELECT id, manufacturer, material, diameter, opening_date FROM pre_product""", con=connect())

    # Saving the Actions performed on the DB
    mysql.connection.commit()
    cursor.close()

    return render_template("form_static_data.html", droplist_operator=dataframe_operator, droplist_operator_js=dataframe_operator.values.tolist(), droplist_machines=dataframe_machines, droplist_machines_js=dataframe_machines.values.tolist(), droplist_filament=dataframe_filament, droplist_filament_js=dataframe_filament.values.tolist(), droplist_settings=dataframe_settings, droplist_settings_js=dataframe_settings.values.tolist())


# http://127.0.0.1:5000/
if __name__ == '__main__':
    print(f"App started at {datetime.now().time()}")
    socket.run(app, host='127.0.0.1', port=5000, use_reloader=False)
    print("App stopped")

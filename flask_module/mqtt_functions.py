import paho.mqtt.client as mqtt_paho
import mysql.connector as connector
import json
import pandas as pd
import flask_module.mySQL_functions.db_credentials as db_credentials
from datetime import datetime
import plotly
import quality_model.feature_importance as feature_importance
import pickle
from threading import Thread
from datetime import timedelta
import time


TOPIC = "Sensoren"
MQTT_BROKER_URL = "127.0.0.1"
MQTT_BROKER_PORT = 1883

columns = ['time', 'Acc_X_Ext','Acc_Y_Ext','Acc_Z_Ext','Acc_X_Bed','Acc_Y_Bed','Acc_Z_Bed','FilFeed', 'FilPositio', 'tempExt1', 'pressure',  'humidity', 'FilDiaA', 'FilDiaB', 'CrossSec', 'DoorState_closed']
df_all_features_current_process = pd.DataFrame(columns=columns)

def on_connect(client, userdata, flags, rc):
    """
    When connected a topic is subscribed
    """
    print("Connected to MQTT Broker: " + MQTT_BROKER_URL)
    client.subscribe(TOPIC) # topic Sensoren

def start_thread_get_data(process_step_id_global, percentile_step_list, percentile = None,):
    """
    starts a thread, that preprocesses the data of the current process
    and extracts features for prediction
    """

    if process_step_id_global != None:
        mydb = connector.connect(
            host=db_credentials.host,
            user=db_credentials.user,
            password=db_credentials.password,
            database=db_credentials.database_name
        )
        cursor = mydb.cursor()

        get_process_duration = """
        SELECT expected_duration FROM process_step_specification WHERE id = (SELECT process_step_specification_id FROM process_step WHERE id = %s)
        """
        cursor.execute(get_process_duration, (process_step_id_global,))
        expected_duration = cursor.fetchone()[0]

        get_process_start = """
        SELECT started_time FROM process_step WHERE id = %s
        """
        cursor.execute(get_process_start, (process_step_id_global,))
        started_time = cursor.fetchone()[0]
        expected_end_time = started_time + timedelta(seconds=expected_duration)
        current_time = datetime.now()
        percentile_difference = (current_time - started_time)/(expected_end_time - started_time) * 100

        for step in range(0,110,10):
            if percentile_difference >= step and step not in percentile_step_list:
                percentile_step_list.append(step)
                thr2 = Thread(target=get_and_preprocess_data_from_current_process(percentile = step))
                thr2.start()

def get_and_preprocess_data_from_current_process(percentile = None):
    global df_of_all_features_saved
    global df_all_features_current_process
    global percentile_step_list
    """
    preprocesses data
    extracts features
    saves data if procentile not None
    """
    print("this is a thread")
    print(time.strftime("%A, %d. %B %Y %I:%M:%S %p"))

    X, y = feature_importance.get_Xreal_and_yreal_tensor("Geometric_accuracy", 100)
    df_of_all_features_saved = X
    print("-------------------df_of_all_features_saved_1-------------------")
    print(df_of_all_features_saved)

    # get data from current process df if it exists
    # print("data is beeing loaded")
    # df = pd.read_csv("./data/All_Testdatasets/Versuch23/2022-10-28_Testplatte23_Mini_240_70_100_altered.txt", sep = ";")
    # print("data loaded")
    
    # print("_____________________________df______________________________")
    # print(df)
    print("_______________df_all_features_current_process_______________")
    print(df_all_features_current_process)
    
    # # preprocess data

    # # get features out of dataframe
    # # df_of_all_features = quality_model.feature_extraction.extract_features(df.head(10000)) #TODO: uncomment

    # df_of_all_features = quality_model.feature_extraction.extract_features_current_data(df_all_features_current_process.copy())
    # print("features extracted")

    # # save features in df
    # # df_of_all_features.to_pickle("./data/preocess_data/df_of_all_features.pkl")
    # # df.to_pickle("./data/process_data/df_of_all_features.pkl")
    # # TODO: append features from database
    # df_of_all_features_saved = df_of_all_features
    # print("-------------------df_of_all_features_saved_2-------------------")
    # print(df_of_all_features_saved)

    

    # # save features in db if percentile not None
    # mydb = connector.connect(
    #     host=db_credentials.host,
    #     user=db_credentials.user,
    #     password=db_credentials.password,
    #     database=db_credentials.database_name
    # )
    # cursor = mydb.cursor()

    # cursor.execute("SELECT percentage FROM sensor_readings WHERE percentage = %s and process_step_id = %s", (percentile_step_list[-1], process_step_id_global))
    # percentage = cursor.fetchall()
    # if percentage == []:
    #     for column in df_of_all_features.columns:
    #         if column != "time":
    #             print("name")
    #             print((column))
    #             insert_query = """
    #             INSERT INTO sensor_readings (sensor_id, process_step_id, value, timestamp, percentage) 
    #             VALUES (%s, %s, %s, %s, %s)
    #             """
    #             cursor.execute("SELECT id FROM sensor WHERE name = %s", (column,))
    #             sensor_id = cursor.fetchone()[0]
    #             try:
    #                 cursor.execute(insert_query, (sensor_id, process_step_id_global, df_of_all_features.iloc[0][column], None, percentile_step_list[-1]))
    #                 mydb.commit()

    #             except:
    #                 print("data could not be saved")
    #             cursor.execute("SELECT percentage FROM sensor_readings WHERE percentage IS Null and process_step_id = %s", (process_step_id_global))
    #             percentage_null = cursor.fetchall()
    #             cursor.execute("SELECT value FROM sensor_readings WHERE percentage IS Null and process_step_id = 1") # TODO: ersätzen durch die html-files
    #             values_html = cursor.fetchall()
    #             print("__________________________values html______________________________")
    #             print(values_html)
    #             if percentage_null == []:
    #                 for value in values_html:
    #                     cursor.execute(insert_query, (sensor_id, process_step_id_global, value, None, None))
    #                     mydb.commit()
    # df_of_all_features_saved = feature_importance.get_features_from_database(percentile_step_list[-1]).tail(1)
    # print("-------------------df_of_all_features_saved_3-------------------")
    # print(df_of_all_features_saved)
    print(percentile_step_list[-1])
    print(df_of_all_features_saved)
    print("dataframe saved")

def preload_data():
    """
    for easy and fast access the following variables are initiated
    this makes the app much faster and enables better usability

    Output:
        dict_pipeline_percentile: Model used to predict the quality of the printed parts
        graphJSON_dict_gesamt: grphics with information about the model, which is shown in "machine_learning_model"
    """
    dict_pipeline_percentile = {}
    dict_pipeline = {}
    for percentile in range(10, 110, 10):
        for quality_parameter in ["Tensile_strength", "Elongation_at_break", "Impact_strength", "Geometric_accuracy"]:
            pipeline = feature_importance.get_model_class(quality_parameter, percentile)
            dict_pipeline[quality_parameter] = pipeline
        dict_pipeline_percentile[percentile] = dict(dict_pipeline)
    graphJSON_dict_gesamt = {}

    translation = {"Zugfestigkeit [N]": "Tensile_strength [N]", "Bruchdehnung [%]": "Elongation_at_break [%]", "Schlagzähigkeit [J]": "Impact_strength [J]", "Geometrische Genauigkeit": "Geometric_accuracy [Boolean]"}
    for quality_parameter in ["Zugfestigkeit [N]", "Bruchdehnung [%]", "Schlagzähigkeit [J]", "Geometrische Genauigkeit"]: #["Tensile_strength", "Elongation_at_break", "Impact_strength", "Geometric_accuracy"]
        graphJSON_dict = {}
        # read the pickle file
        picklefile = open('quality_model/feature_importance_plots/' + str(translation[quality_parameter]).split(" ")[0] + str(100), 'rb')
        # unpickle the dataframe
        fig_dict = pickle.load(picklefile)
        # close file
        picklefile.close()
        # read the components of fig_dict and save them in different json files
        # the json files can be read by javascript and http to display the plots
        for key in fig_dict:
            fig = fig_dict[key]
            graphJSON_dict[key] = json.dumps(fig, cls=plotly.utils.PlotlyJSONEncoder)
        graphJSON_dict_gesamt[quality_parameter] = graphJSON_dict

    return dict_pipeline_percentile, graphJSON_dict_gesamt


# calculate percentile of process duration time
def calculate_percentile_of_process_duration(cursor, process_step_id):
    """
    calculates the percentile of the process-duration

    input:
        cursor: datbase-cursor to perform an action in the database
        process_step_id: INT, needed for a search parameter in the database

    output:
        percentile_difference: percentile of the process-duration
    """
    print(type(process_step_id))
    print(process_step_id)
    get_process_duration = """
    SELECT expected_duration FROM process_step_specification WHERE id = (SELECT process_step_specification_id FROM process_step WHERE id = %s)
    """
    cursor.execute(get_process_duration, (process_step_id,))
    expected_duration = cursor.fetchone()["expected_duration"]

    get_process_start = """
    SELECT started_time FROM process_step WHERE id = %s
    """
    cursor.execute(get_process_start, (process_step_id,))
    started_time = cursor.fetchone()["started_time"]
    expected_end_time = started_time + timedelta(seconds=expected_duration)
    current_time = datetime.now()

    percentile_difference = (current_time - started_time)/(expected_end_time - started_time) * 100

    return percentile_difference
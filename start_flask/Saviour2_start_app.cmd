@ECHO OFF
if "%~1" neq "_start_" (
  cmd /c "%~f0" _start_ %*
  echo Now stopping
  CALL venv\Scripts\deactivate
  :: Stop xampp server
  start /min "" "C:\xampp\apache_stop.bat"
  start /min "" "C:\xampp\mysql_stop.bat"
  :: Stop mosquitto broker
  "%CD%\start_flask\Saviour2_stop_mosquitto.lnk"
  :: Close console windows
  echo Successfully stopped. Press any key to close.
  pause
  exit /b
)
shift /1

:: Start mosquitto broker
"%CD%\start_flask\Saviour2_start_mosquitto.lnk"

:: Start xampp server
start /min "" "C:\xampp\apache_start.bat"
start /min "" "C:\xampp\mysql_start.bat"

:: Start virtual environment and flask app
@REM start "Saviour" %CD%\start_flask\Saviour2_start_app.cmd
start "" "http://127.0.0.1:5000"

:: Activating virtual environment
CALL venv\Scripts\activate
ECHO Virtual environment started.

:: Starting Flask App
ECHO Now starting flask app.
python flask_module\app3.py

EXIT /B %ERRORLEVEL%

@ECHO OFF

ECHO Starting set up for Saviour
ECHO Please manually check whether xampp, mosquitto 
ECHO and python are installed.

:: Checking requirements for Saviour2 App
CD ..
CALL :check_requirements "C:\xampp\"
CALL :check_requirements "%ProgramFiles%\mosquitto"
CALL :check_requirements "%LOCALAPPDATA%\Programs\Python"

:: Create desktop shortcut
echo [InternetShortcut] >> "%USERPROFILE%\Desktop\Saviour.url"
echo URL="%~dp0start_saviour.cmd" >> "%USERPROFILE%\Desktop\Saviour.url"
echo IconFile="%~dp0logo-app2.ico" >> "%USERPROFILE%\Desktop\Saviour.url"
echo IconIndex=0 >> "%USERPROFILE%\Desktop\Saviour.url"

:: Create virtual environment
:: And install required Python packages
py -m venv venv
CALL venv\Scripts\activate
pip install -r requirements.txt
CALL deactivate

ECHO Set up finished.
PAUSE
GOTO :EOF
EXIT /B %ERRORLEVEL%

@REM Declaring functions
:check_requirements
IF NOT EXIST "%~1" (
    ECHO Error: "%~1" could not be found.
    ECHO Please check whether it is installed.
    PAUSE
    GOTO :eof
)
EXIT /B 0

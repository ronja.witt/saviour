@echo off

:: Checking requirements for Saviour2 App
cd %~dp0
cd ..
ECHO %CD%
CALL :check_requirements "C:\xampp\"
CALL :check_requirements "%ProgramFiles%\mosquitto"
CALL :check_requirements "venv"

:: Open Editor with 
start Notepad "flask_module\mySQL_functions\db_credentials.py"

CALL start_flask\Saviour2_start_app.cmd

:end
ECHO This is the end.
pause
GOTO :EOF
EXIT /B %ERRORLEVEL%

@REM Declaring functions
:check_requirements
IF NOT EXIST "%~1" (
    ECHO Error: "%~1" could not be found.
    ECHO Please check the requirements manually again.
    PAUSE
    GOTO end
)
EXIT /B 0
